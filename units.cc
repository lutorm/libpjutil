// $Id$

#include "units.h"
#include "boost/lexical_cast.hpp"
#include <cstdio>
#include <cstring>
#include <iostream>

using namespace std;


// in gnu_units.c
extern "C" {
int unitsmain(int, const char**, char*, int);
}

double units::convert(std::string from, std::string to)
{
  // construct arguments. because we use Msun but gnu units uses
  // sunmass, we convert any arguments (safe, since we don't use the
  // length unit Mega-sun, which is ~30km...)
  const char* sunmass="sunmass";

  const char* args[4];
  args[0]="units";
  args[1]="-t";
  int pos = from.find("Msun");
  string fromstr((pos != string::npos) ? from.replace(pos, 4, sunmass) : from);
  args[2]= fromstr.c_str();
  pos = to.find("Msun");
  string tostr((pos != string::npos) ? to.replace(pos, 4, sunmass) : to);
  args[3]= tostr.c_str();

  // call units and read output
  char buf[255];
  strcpy(buf,"failed");
  unitsmain(4,args,buf,255);

  const string result(buf);
  try {
    const double factor = boost::lexical_cast<double>(result);
    return factor;
  }
  catch(...) {
    cerr << "Invalid unit conversion: " << args[2] << " -> " << args[3]  << " returned " << result << endl;
    throw invalid_conversion();
  }
}
