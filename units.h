// $Id$

#ifndef __units__
#define __units__

#include <string>

/** This class is an interface to the "units" program, which converts
    between units. */
class units {
private:
  const static std::string units_bin_;

public:
  class invalid_conversion {};

  static double convert(std::string from, std::string to);
};

#endif
