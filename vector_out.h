// $Id$

// vector output operator

#ifndef __vector_out__

#include <vector>
#include <ostream>

namespace std {
template<typename T,typename R>
std::ostream& operator<<(std::ostream& os, const std::vector<T,R>& v);
};

template<typename T,typename R>
std::ostream& std::operator<<(std::ostream& os, const std::vector<T,R>& v) {
  os << v.size() << " [\n";
  for(int i=0;i<v.size(); ++i) 
    os << '\t' << v[i] << '\n';
  os << " ]\n";
  return os;
}


#endif
