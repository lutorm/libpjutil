#include<iostream>
#include<iterator>
#include "preferences.h"
#include<string>
#include"counter.h"
#include "units.h"
#include "interpolatort.h"
#include "blitz/array.h"
#include "partition.h"
#include "hilbert.h"
#include "bitfiddle.h"

using namespace std;

Preferences p;

template < typename T >
void test_keyword (const string& kw, const T& value)
{
  const T v = p.getValue(kw, T ());
  if (v != value )
    cerr << "Error: keyword " << kw << " not read correctly:"
	 << "\t" << v << " != " << value << endl;
}

template < typename T >
void test_keyword (const string& kw, const blitz::TinyVector<T,3>& value)
{
  typedef blitz::TinyVector<T,3> vec3;
  const vec3 v = p.getValue(kw, vec3 ());
  if ((v[0] != value[0]) || (v[1] != value[1]) || (v[2] != value[2]) )
    cerr << "Error: keyword " << kw << " not read correctly:"
	 << "\t" << v << " != " << value << endl;
}

void test_prefs()
{
  // read from test file
  p.readfile("test.prefs");

  cout << "Defined keywords:\n";
  p.print();

  test_keyword ("camera_position", blitz::TinyVector < double, 3 > (10,0,0));

  // check that values were read correctly
  test_keyword ("true", true);
  test_keyword ("false", false);
  test_keyword ("one", int (1));
  //test_keyword ("one", float (1));
  test_keyword ("one", double (1));
  //test_keyword ("two_point_two", float (2.2));
  test_keyword ("two_point_two", double (2.2));
  test_keyword ("vec3i", blitz::TinyVector < int, 3 > (1,2,3));
  //test_keyword ("vec3i", blitz::TinyVector < double, 3 > (1,2,3));
  test_keyword ("vec3d", blitz::TinyVector < double, 3 > (1.1,2.2,3.3));
  //test_keyword ("vec3d", blitz::TinyVector < int, 3 > (1,2,3));
  test_keyword ("string", string ("string"));
  test_keyword ("dir", string ("/home/patrik/test"));
  cout << "Test that unused was not used:\n";
  p.print_unused();
  cout <<"\n\n";
}

void test_counter()
{
  cout << "You should see numbers:"<<endl;
  counter c(3);
  for (int i=0;i<10000;++i) 
    c++;
  cout << endl;
}

void test_units()
{
  cout << "1 kpc is " << units::convert("kpc","m") << " m" << endl;
  cout << "1 erg/s is " << units::convert("erg/s","W") << " W" << endl;
  cout << "Trying to convert W to s... ";
  try {
    units::convert("W","s");
  }
  catch (units::invalid_conversion&) {
    cout << "...Failed, good." << endl;
  }
}

bool close(double a, double b)
{
  return abs(a-b)<1e-30;
}

void test_interpolator()
{
  cout << "Testing interpolating doubles (in 1D)" << endl;
  interpolator<double,double,1> interpol;
  vector<double> xvals;
  xvals.push_back(0.);
  xvals.push_back(1.);
  xvals.push_back(2.);
  xvals.push_back(3.);
  xvals.push_back(4.);
  interpol.setAxis(0,xvals);
  for(int i=0;i<xvals.size();++i)
    interpol.setPoint(i,xvals[i]);
  assert(close(interpol.interpolate(0.),0.));
  assert(close(interpol.interpolate(4.),4.));
  assert(close(interpol.interpolate(0.5),0.5));
  assert(close(interpol.interpolate(1.1),1.1));
  assert(close(interpol.nearest(0.51),1.));
  assert(close(interpol.nearest(1.1),1.));
  cout << "ok.\nTesting interpolating an array (in 1D)" << endl;

  interpolator<blitz::Array<double,1>,double,1> interpol2;
  interpol2.setAxis(0,xvals);
  blitz::Array<double, 1> a(3);
  a=1,2,3;
  for(int i=0;i<xvals.size();++i)
    interpol2.setPoint(i,blitz::Array<double,1>(a+i));
  assert(all(abs(interpol2.interpolate(0.5)-(a+0.5))<1e-30));
  assert(all(abs(interpol2.interpolate(2.75)-(a+2.75))<1e-30));
  assert(all(abs(interpol2.nearest(2.75)-(a+3))<1e-30));
  cout << "ok." << endl;
}

void test_partition()
{
  cout << "Testing partition\n";
  {
    vector<int> cost;
    cost.push_back(1);
    cost.push_back(2);
    cost.push_back(3);
    cost.push_back(4);
    cost.push_back(5);
    cost.push_back(6);
    cost.push_back(7);
    cost.push_back(8);
    cost.push_back(9);
    
    vector<size_t> part=partition(3,cost);
    std::copy(part.begin(), part.end(), ostream_iterator<int>(cout, ", "));
    cout << endl;
    assert(part[0]==0);
    assert(part[1]==5);
    assert(part[2]==7);
  }

  {
    vector<int> cost(9,1);
    
    vector<size_t> part=partition(3,cost);
    std::copy(part.begin(), part.end(), ostream_iterator<int>(cout, ", "));
    assert(part[0]==0);
    assert(part[1]==3);
    assert(part[2]==6);
    cout << endl;
  }
  cout << "ok." << endl;

}

void test_bitfiddle()
{
  cout << "Testing bitfiddles\n";
  assert(count_bits(0)==0);
  assert(count_bits(1)==1);
  assert(count_bits(2)==1);
  assert(count_bits(3)==2);
  assert(count_bits(4)==1);
  assert(count_bits(5)==2);
  assert(count_bits(6)==2);
  assert(count_bits(7)==3);
  assert(count_bits(0xffff)==16);
  assert(count_bits(0xffffffff)==32);

  assert(log2(1U)==0);
  assert(log2(2U)==1);
  assert(log2(3U)==1);
  assert(log2(4U)==2);
  assert(log2(7U)==2);
  assert(log2(8U)==3);
  assert(log2(13U)==3);
  assert(log2(15U)==3);
  assert(log2(255U)==7);
  assert(log2(256U)==8);
  assert(log2(65535U)==15);
  assert(log2(100000U)==16);
  cout << "ok.\n";
}

void test_hilbert()
{
  using namespace hilbert;
  typedef blitz::TinyVector<double,3> vec3d;

  cout << "Testing Hilbert\n";

  {
    // test the state operations
    hilbert_state s;
    assert(s.dim(0)==0);
    assert(s.dim(1)==1);
    assert(s.dim(2)==2);
    assert(s.axis(0)==0);
    assert(s.axis(1)==0);
    assert(s.axis(2)==0);
    s.flip_axis(0);
    assert(s.axis(0)==1);
    assert(s.axis(1)==0);
    assert(s.axis(2)==0);
    s.flip_axis(1);
    assert(s.axis(0)==1);
    assert(s.axis(1)==1);
    assert(s.axis(2)==0);
    assert(s.dim(0)==0);
    assert(s.dim(1)==1);
    assert(s.dim(2)==2);

    s.swap_dims(0,2);
    assert(s.dim(0)==2);
    assert(s.dim(1)==1);
    assert(s.dim(2)==0);
    assert(s.axis(0)==0);
    assert(s.axis(1)==1);
    assert(s.axis(2)==1);

    s.swap_dims(2,0);
    assert(s.dim(0)==0);
    assert(s.dim(1)==1);
    assert(s.dim(2)==2);
    assert(s.axis(0)==1);
    assert(s.axis(1)==1);
    assert(s.axis(2)==0);

    s.swap_dims(0,1);
    assert(s.dim(0)==1);
    assert(s.dim(1)==0);
    assert(s.dim(2)==2);
    assert(s.axis(0)==1);
    assert(s.axis(1)==1);
    assert(s.axis(2)==0);

    s.reorder_dims(1,2,0);
    assert(s.dim(0)==0);
    assert(s.dim(1)==2);
    assert(s.dim(2)==1);
    assert(s.axis(0)==1);
    assert(s.axis(1)==0);
    assert(s.axis(2)==1);
  }

  double pos[] = {0.125, 0.375, 0.625, 0.875};

  {
    std::vector<vec3d> p;
    for(int x=0; x<4; x+=2)
      for(int y=0; y<4; y+=2)
	for(int z=0; z<4; z+=2)
	  p.push_back(vec3d(pos[x], pos[y], pos[z]));
    
    // level 1 curve
    int octs[] = {0,7,3,4,1,6,2,5};
    for(int i=0; i<8; ++i) {
      cout << p[i] << '\n';
      qpoint pp(p[i], 3);
      cout << pp << endl;

      octree_cell<hilbert_state> h;
      h.descend(pp);
      cout << h.code().code() << '\t' << h << endl;
      assert(h.code().code()==octs[i]);

      assert(encode_point(p[i],1).code()==octs[i]);
    }
  }


  {
    std::vector<vec3d> p;
    for(int x=0; x<4; ++x)
      for(int y=0; y<4; ++y)
	for(int z=0; z<4; ++z)
	  p.push_back(vec3d(pos[x], pos[y], pos[z]));
    
    // level 2 curve
    int octs[] = {0,1,62,63,3,2,61,60,26,27,36,37,29,28,35,34,
		  7,6,57,56,4,5,58,59,25,24,39,38,30,31,32,33,
		  8,11,52,55,15,12,51,48,16,23,40,47,19,20,43,44,
		  9,10,53,54,14,13,50,49,17,22,41,46,18,21,42,45};
		  
    for(int i=0; i<64; ++i) {
      qpoint pp(p[i], 3);
      cout << "Point: " << p[i] << " <=> ";
      cout << pp << '\n';

      octree_cell<hilbert_state> h;
      h.descend(pp);
      h.descend(pp);
      cout << h.code().code() << '\t' << h << endl;
      assert(h.code().code()==octs[i]);

      assert(encode_point(p[i],2).code()==octs[i]);
    }
  }

  //test contains
  {
    const qpoint root(vec3d(.5,.5,.5),0);
    const qpoint p1(vec3d(.25,.25,.25),1);
    const qpoint p2(vec3d(.75,.25,.25),1);
    const qpoint p3(vec3d(.125,.24,.24),2);
    const qpoint p4(vec3d(.2359832982, .192938429362, .073928342938));
    assert(root.contains(root));
    assert(root.contains(p1));
    assert(root.contains(p2));
    assert(root.contains(p3));
    assert(root.contains(p4));
    assert(!p1.contains(root));
    assert(!p2.contains(root));
    assert(!p3.contains(root));
    assert(!p4.contains(root));
    assert(!p1.contains(p2));
    assert(p1.contains(p1));
    assert(p1.contains(p3));
    assert(p1.contains(p4));
    assert(!p3.contains(p1));
    assert(p2.contains(p2));
    assert(!p2.contains(p3));
    assert(p3.contains(p3));
    assert(!p3.contains(p2));
    assert(!p4.contains(p2));
    assert(!p4.contains(p3));
    assert(p3.contains(p4));
    assert(p4.contains(p4));
  }

  //test code contains
  {
    const cell_code root(0,0);
    const cell_code p1(0,1);
    const cell_code p2(1,1);
    const cell_code p3(0,2);
    const cell_code p4(0xfe,5);
    assert(root.contains(root));
    assert(root.contains(p1));
    assert(root.contains(p2));
    assert(root.contains(p3));
    assert(root.contains(p4));
    assert(!p1.contains(root));
    assert(!p2.contains(root));
    assert(!p3.contains(root));
    assert(!p4.contains(root));
    assert(!p1.contains(p2));
    assert(p1.contains(p1));
    assert(p1.contains(p3));
    assert(p1.contains(p4));
    assert(!p3.contains(p1));
    assert(p2.contains(p2));
    assert(!p2.contains(p3));
    assert(p3.contains(p3));
    assert(!p3.contains(p2));
    assert(!p4.contains(p2));
    assert(!p4.contains(p3));
    assert(p3.contains(p4));
    assert(p4.contains(p4));
  }

  // test qpoint conversions
  {
    const vec3d a(.5,.5,.5);
    const qpoint qa(a,1);
    assert(all(a==vec3d(qa)));

    const vec3d b(.25,.75,.25);
    const qpoint qb(b,2);
    assert(all(b==vec3d(qb)));

    const vec3d c(.982342963,.892123946,.329856239823929);
    const qpoint qc(c);
    cout << c << vec3d(qc) << vec3d(abs(c/vec3d(qc)-1)) << endl;
    assert(all(abs(c/vec3d(qc)-1)<1e-6));
  }

  // test extensions
  {
    qpoint pl(1,0,3,2);
    pl.extend_low(12);
    assert(pl.level()==12);
    assert((pl[0]&bitmask_n<uint32_t>(10))==0);
    qpoint ph(1,0,1,2);
    ph.extend_high(12);
    assert(ph.level()==12);
    assert((ph[0]&bitmask_n<uint32_t>(10))==bitmask_n<uint32_t>(10));

    cell_code cl(12,2);
    cl.extend_low(12);
    assert(cl.level()==12);
    assert((cl.code()&bitmask_n<uint64_t>(10*3))==0);

    cell_code ch(12,2);
    ch.extend_high(12);
    assert(ch.level()==12);
    assert((ch.code()&bitmask_n<uint64_t>(10*3))==bitmask_n<uint64_t>(10*3));
  }
    
};
  
main()
{
  test_bitfiddle();
  test_hilbert();
  test_partition();
  test_prefs ();
  test_counter();
  test_units();
  test_interpolator();
}
