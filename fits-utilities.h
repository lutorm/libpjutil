// $Id$

#ifndef __fits_utilities__
#define __fits_utilities__

#include<string>

namespace CCfits {
  class FITS;
  class ExtHDU;
  class HDU;
};

// Tries to open the specified HDU.  If that fails , throws (or
// rather, fails to catch the second no such HDU exception)
CCfits::ExtHDU& open_HDU (CCfits::FITS& file, const std::string& name);
CCfits::ExtHDU& open_HDU (CCfits::FITS& file, int hdunum);
const CCfits::ExtHDU& open_HDU (const CCfits::FITS& file,
				const std::string& name);
const CCfits::ExtHDU& open_HDU (const CCfits::FITS& file,
				int hdunum);

/// Returns the unit of a keyword, which is the string in [...] in the
/// comment.
std::string keyword_unit(CCfits::HDU& hdu, const std::string& keyword);

#endif
