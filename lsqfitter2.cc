// lsqfitter2.cc
//
// Implementation of numerical least squares fitter class
// Translation to c++ from Numerical Recipes C program
// $Id$

#include <cmath>
#include "lsqfitter2.h"
#include "misc.h"
#include<iostream> 

using namespace std;

// Constructor doesn't do as much as I would like...
lsqfitter2::lsqfitter2( double dystep,double dyacc )
{
  verbose=2; // Initially print info
  //dydafunc=0;

  DY_INITIALSTEP=dystep;
  INITLAMBDA=0.001;
  SUCCESSINCR=0.1;  // Shouldn't this be a factor <1??? Yes it should indeed
  FAILDECR=10;
  ABSACC = 1e-5;
  RELACC = 1+1e-5;
  FAILRETRIES =7;
  DY_ACCURACY=dyacc;
}

// This returns the 1-dof uncertainty in the parameters
matrix<double> lsqfitter2::getParameterError()
{
  matrix<double> err(npar,1);

  double stdfit;
  if(ndata-nfit<=0) {
    err=HUGE_VAL;
    return err;
  }
  else
    stdfit=sqrt(chisq/(ndata-nfit));
  
  for(int i=0;i<npar;i++)
    err[i]=stdfit*sqrt(C[i][i]);

  return err;
}


// This is the callable routine, performs the fit
double lsqfitter2::fit(const arr& xx, const arr& yy,
		       const arr& ss, arr& pp,
		       const table<char>& ff)
{
  if(verbose==2)
    cout << "Starting fit\n";

  // Copy the input parameters
  par=&pp;
  fixpar=ff;

  npar = par->n();
  int npoints = xx.n();

  nfit=0;
  for (int j=0;j<npar;j++)
    if (fixpar[j])
      nfit++;

  ndata=0;
  for(int i=0;i<npoints;i++)
    if(((arr&)ss)[i]!=HUGE_VAL)
      ndata++;

  if(ndata==0) {
    cerr << "lsqfitter2::fit: All data points invalid!\n";
    chisq=HUGE_VAL;
    C=matrix<double>(npar,npar,chisq);
    return chisq;
  }

  // Copy the data points (but only valid ones)
  x=arr(ndata);
  y=arr(ndata);
  sig=arr(ndata);
  for(int i=0,j=0;i<npoints;i++)
    if(((arr&)ss)[i]!=HUGE_VAL) {
      x[j]=((arr&)xx)[i];
      y[j]=((arr&)yy)[i];
      sig[j++]=((arr&)ss)[i];
    }


  // Initialization originally in mrqmin

  // atry needs to have all components because it's sent to the function
  atry=matr(npar,1); 
  // But all the linear eq solving stuff only needs nfit parameters
  beta=matrix<double>(nfit,1,0.0);
  da=matr(nfit,1);
  covar=matrix<double>(nfit,nfit,0.0);
  alpha=matrix<double>(nfit,nfit,0.0);

  oneda=matrix<double>(nfit,1,0.0); // oneda and covar are different dims?
  C=matrix<double>(npar,npar);
  // oneda=matrix<double>(npar,1,0.0);

  // Set initial value of alambda
  alambda=INITLAMBDA;
  
  // Copy our parameters to atry (the fixed parameters will stay unchanged
  // in there from now on)
  for(int i=0;i<npar;i++)
    atry[i]= (*par)[i];

  mrqcof(&atry,&alpha,&beta);
  ochisq=chisq;

  // I hope we can go this far without dof, but I think we'll be ok.
  if(ndata-nfit<=0) {
    // No degrees of freedom - can't fit
    // But what do we set the parameter uncertainty to? Inf?
    cerr << "lsqfitter2::fit - no degrees of freedom!\n";
    return chisq;
  }

  // This completes the initialization
  // Now we should iterate the fitting
  // we iterate until we've converged
  // should put in a "fail to converge" check

  if(verbose==2) {
    cout << "Initialization completed, starting iteration\n";
    cout << " initial chisquare is " << chisq << '\n';
  }
  
  int iter=0;
  int fail=0;
  double oxi2=chisq+1000;
  while( (((oxi2-chisq)>ABSACC) && ((oxi2/chisq)>RELACC)) || fail ){
  //while( 1) {
    oxi2=chisq;

    if(!mrqmin())
      // Step failed, increase count
      fail++;
    else
      // Reset count if a step succeeded
      fail=0;

    if(fail>FAILRETRIES)
      // Assume we're fucked
      fail=0;
     
    iter++;
    if(verbose==2) {
      cout << "Iteration " << iter << ":\n";
      cout << "  Chisquare = " << chisq << '\n';
      cout << "  Parameters are " << *par << "\n\n";
    }
    else if(verbose==1) {
      cout << iter << '\t' << chisq;
      for(int i=0;i<npar;i++)
	cout << '\t' << (*par)[i];
      cout << '\n';
    }
  }
  
  // Call with alambda=0 to get the final result
  //alambda=0;
  // mrqmin();
  covar=invert(alpha);
  covsrt();

  if(verbose==2) {
    cout << "\nFit converged with a chisquare of " << chisq;
    cout << "\nNumber of degrees of freedom are " << ndata-nfit;
    cout << "\nReduced chisquare is " << chisq/(ndata-nfit);
    cout << "\n\nBest-fit parameters are:\n\t" << *par << '\t';
    cout << "\nCovariance matrix is\n\t" << C << '\n'; 
  }
    
  // Return reduced chisquare
  return chisq/(ndata-nfit);
}


int lsqfitter2::mrqmin()
{
  // Alter linearized fitting matrix, by augmenting diagonal elements
  // Because covar, alpha, beta and oneda only contains non-fixed parameters
  // this is simple
  covar=alpha;
  oneda=beta;
  for (int j=0;j<nfit;j++)
    covar[j][j]=alpha[j][j]*( 1.0+alambda );
  
  // Matrix solution - returns inverse of covar in covar 
  // and solutions to COVAR*x=oneda in oneda
  int t1=realcc();
  //gaussj(covar,&oneda,1);
  covar.gaussj(&oneda,1);
  int t2=realcc();

  da=oneda;

  // Did the trial succeed?

  // Update atry with changing parameters
  for (int j=0, l=0;l<npar;l++)
    if (fixpar[l]) 
      atry[l]=(*par)[l]+da[j++];
  
  int t3=realcc();
  mrqcof(&atry,&covar,&da);
  int t4=realcc();
  int returnvalue;

  if (chisq < ochisq) {
    returnvalue=1; // Success
    // Success - return the new solution

    // Increase step for next time
    alambda *= SUCCESSINCR;
    if(verbose==2)
      cout << "success, new alambda is " << alambda << "\n";

    // Update with the new solution
    ochisq=chisq;
    alpha=covar;
    beta=da;
    *par=atry;
  } 
  else {
    returnvalue=0; // Failed
    // Failure - increase alambda and return
    alambda *= FAILDECR;
    if(verbose==2)
      cout << "failure, new chisquare is " << chisq << ", new alambda is " << alambda << "\n";
    chisq=ochisq;
  }
  //cout << t2-t1 << '\t' << t4-t3 << '\n';  

  return returnvalue;
}


void lsqfitter2::mrqcof(matr* p,matrix<double>* al,
			matr* be)
{
  // When we come in we have the old da in be, so we can use that info to
  // guess what a reasonable da in the derivative estimation can be (if we
  // need to use the numerical estimator
  daguess= *be;

  // set alpha and beta to zero
  (*al)=0;
  (*be)=0;

  chisq=0.0;
  int t1,t2;

  // Summation loop over data
  for (int i=0;i<ndata;i++) {
    
    double cur_y = function(x[i],(*p));

    // Call the dyda function
    arr dyda;
    // Get the parameter derivative - calls the numerical version if it
    // wasn't redefined to an analytical one
    t1=realcc();
    dyda = dydafunction( x[i], *p );
    t2=realcc();
    
    double sig2i=1.0/(sig[i]*sig[i]);
    double dy=y[i]-cur_y;
    
    // Make up the alpha and beta matrices
    for(int j=0,l=0;l<npar;l++)
      // Is this parameter being varied?
      if(fixpar[l]) {
	double wt=dyda[l]*sig2i;

	for(int k=0,m=0;m<=l;m++)
	  // What about this one?
	  if(fixpar[m])
	    // Yes, update this matrix entry
	    (*al)[j][k++] += wt*dyda[m];

	// Update the right-hand side entry
	(*be)[j] += dy*wt;
	j++;
      }
    chisq += dy*dy*sig2i;
  } // for i (data point loop)

  // Fill in the symmetric side
  for (int j=1;j<nfit;j++)
    for (int k=0;k<j;k++)
      (*al)[k][j]=(*al)[j][k];
  //  cout << t2-t1 << '\n';
}


// Unlike numerical recipes covar is "descrambled" and put in C here,
// because we have them in different sizes
void lsqfitter2::covsrt()
{
  for(int i=0;i<nfit;i++)
    for(int j=0;j<nfit;j++)
      C[i][j]=covar[i][j];

  for (int i=nfit;i<npar;i++)
    for (int j=0;j<=i;j++) 
      C[i][j]=C[j][i]=0.0;

  int k=nfit-1;

  for (int j=npar-1;j>=0;j--) {
    if (fixpar[j]) {
      for (int i=0;i<npar;i++) {
	double temp=C[i][k];
	C[i][k]=C[i][j];
	C[i][j]=temp;
      }
      for (int i=0;i<npar;i++) {
	double temp=C[k][i];
	C[k][i]=C[j][i];
	C[j][i]=temp;
      }
      k--;
    }
  }
}

matr lsqfitter2::dydafunction(double x,const matr& a)
{
  // We want to compute the derivative of function f wrt the parameters a
  // around the current point x,a
  // We do this by using the finite difference approximation
  // dy/dx = 1/(2h)*( y(x+h)-y(x-h) )

  //cout << "Evaluating dy/da:\n";
  matr da(npar,1,0.); // Small difference in a
  matr dy(npar,1); // contains derivatives

  // Use daguess*fudge factor estimate behaviour
  // But daguess has nfit length, not npar

  // Loop over the parameters a
  for(int i=0,j=0; i<npar; i++)
    // It's timeconsuming - only do it if we need the derivative
    if(fixpar[i]) {
      // We need a GOOD arbitrary starting guess for h, the parameter
      // diff da If h is too large we will have to iterate for long to
      // converge if it's too small we'll get cancellation and bad
      // precision (Note that a good h could well be very different
      // for the diff parameters)
      double h=DY_INITIALSTEP;

      double ody=1;
      dy[i]=0;
      int iter=0;
      // If we don't have an old da we do it the slow way
      if(daguess[j]==0.) {
	// Shrink step until we've converged
	da[i]=h;
	ody=( function(x,a+da) - function(x,a-da) )/(2.*h);
	h/=5.;
	iter++;

	da[i]=h;
	dy[i]=( function(x,a+da) - function(x,a-da) )/(2.*h);
	h/=5.;
	iter++;
	while( fabs(dy[i]-ody)>DY_ACCURACY ) {
	  da[i]=h;
	  ody=dy[i];
	  dy[i]=( function(x,a+da) - function(x,a-da) )/(2*h);
	  h/=5;
	  iter++;
	}
	//cout << "Parameter#" << i << " required "<< iter << " at x=" << x << '\n';
      }
      else {
	da[i]=daguess[j]*0.01;
	dy[i]=( function(x,a+da) - function(x,a-da) )/(2.*da[i]);
      }
      da[i]=0;
      j++;
    } // if fixpar

  return dy;
}
