// solver2.h
//
// Declaration of numerical equation solver routines
// Uses simple Newton-Raphson method
//
// Patrik Jonsson
//
// $Id$

/*
One solver object solves for a specific function f=0. The function may take
any number of parameters that are supplies as double*. The call to solve
must match the number of parameters and supply a first guess.
The constructor must be supplied with the function as well as its derivative.
*/

#ifndef __solver2__
#define __solver2__

class solver2 {
private:
  bool verbose;
  double accuracy,xaccuracy;                     // Required accuracy;

 protected:
  virtual double func(double)=0;      // Function to solve
  virtual double der(double)=0;       // Function derivative

public:
  solver2();
  solver2(double a, double xa=0);

  double solve(double x0 );         // Solve equation from first guess x0

  void setaccuracy(double a, double xa=0);

  void setverbose(bool v) {verbose = v;};
};

inline solver2::solver2()
{
  accuracy=1;
  xaccuracy=0;
  verbose=false;
}

inline solver2::solver2(double a, double xa)
{
  accuracy = a;
  xaccuracy = xa;
}

inline void solver2::setaccuracy(double a, double xa)
{
  accuracy=a;
  xaccuracy = xa;
}

#endif

