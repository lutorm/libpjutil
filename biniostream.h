// biniostream.h
// A class for binary file I/O

// $Id$

#ifndef __biniostream__
#define __biniostream__

#include <fstream>
#include "blitz/array.h"
#include <string>

//using namespace std;

// **********************
// *                    *
// * Binary file stream *
// *                    *
// **********************

// This is the base class for binary file streams and can be
// used for both reading and writing

class binfstream : public std::fstream {
private:
  // The byte order of the machine
  static const bool machine_big_endian;

  // Byte order of the file
  bool file_native;

protected:
  // outputs n bytes starting at p to the stream
  void output (const char* p, unsigned int n);    
  void input (char* p, unsigned int n);

public:
  binfstream() : std::fstream() {
    file_native=1; }
  binfstream(const char*,std::ios::openmode);
  binfstream(const std::string,std::ios::openmode);
  virtual ~binfstream() {};

  // Set file type - defaults to native
  void bigendian() {
    file_native = machine_big_endian;};
  void littleendian() {
    file_native = !machine_big_endian;};
  void native() {
    file_native = true;};
  void swap () {
    file_native = false;};
    

  // These output operators replace the ofstream ones
  //template < class T> binfstream& operator<< (const T);
  
  binfstream& operator<<(char c) {
    put(c); return *this;};
  // output a c-string
  binfstream& operator<<(const char* c) {
    while(*c) put(*c++); return *this;};
  // output a c++ string
  binfstream& operator<<(const std::string& s) {
    return operator<<(s.c_str());};

  // This template output operator is unsafe for anything but built-in
  // types, so the implementation is in the .cc file and is explicitly
  // instantiated only for built-in types.
  template<typename T> binfstream& operator<<(T d);

  binfstream& operator>>(char& c) {
    get(c); return *this;};
  // Inputs a null-terminated string of char from the binary ifstream
  binfstream& operator>>(char*c) {
    get(*c); while(*c++) get(*c); return *this;};
  // Inputs a blitz tinyvector from the stream
  template<typename T, int N>
  binfstream& operator>>(blitz::TinyVector<T,N>& m) {
    for(int i=0; i<N; ++i) 
      input (reinterpret_cast<char*> (&m[i]), sizeof(T));
    return *this;};



  // See comment for operator<<
  template<typename T> binfstream& operator>>(T& d);

  // Skips n bytes in stream
  binfstream& skip(int n) {
    char c; while(n--) get(c); return *this;};
};


// Binofstream is the output stream class

class binofstream : public binfstream {
public:
  binofstream() : binfstream() {};
  binofstream(const char* name): binfstream(name,std::ios::out) {};
  binofstream(const std::string name): binfstream(name.c_str(),std::ios::out) {};

  void open(const char* name) {
    binfstream::open(name,std::ios::out);};
  void open(const std::string name) {open(name.c_str());};
};


// Binifstream is the input stream class

class binifstream : public binfstream {
public:
  binifstream() : binfstream() {};
  binifstream(const char* name) : binfstream(name, std::ios::in) {};
  binifstream(const std::string name): binfstream(name.c_str(),std::ios::in) {};

  void open(const char* name) {
    binfstream::open(name,std::ios::in);};
};

#endif

