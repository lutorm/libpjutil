// lsqfitter3.h

#ifndef __lsqfitter3__
#define __lsqfitter3__

#include "matrix.h"
#include "tableutils.h"

typedef table<double> arr;
typedef matrix<double> matr;

class lsqfitter3 {
private:
  matrix<double> alpha;
  matrix<double> covar;
  matrix<double> oneda;
  double ochisq;
  matr atry;
  matrix<double> beta;
  matr da;
  matr daguess;
  matrix<double> C;
  double  chisq;
  double  alambda;

  arr x;
  arr y;
  table<char> fixpar;
  arr sig;
  arr* par;
  int npar; // Number of parameters
  int nfit; // Number of parameters to fit
  int ndata; // Number of data points

  // Threshold values that *can* be user set
  // Don't change them unless you know what you're doing
  arr DY_INITIALSTEP;
  double INITLAMBDA;
  double SUCCESSINCR;
  double FAILDECR;
  double ABSACC;
  double RELACC;
  int FAILRETRIES;

  int verbose;

  double gcf(double,double);
  double gser(double,double);

  void covsrt();
  void mrqcof(matr*, matrix<double>*, matr*);
  int mrqmin();

public: 
  double gammq(double,double);
  // Constructor - argument is starting step for numerical derivative
  // (The steps after the first is a fraction of the fit step taken.)
  lsqfitter3( const arr& dystep=arr() );

  // Function to fit - abstract virtual - must be defined in a derived class
  // Returns an entire set for all x-values
  virtual arr function(const arr&, const matr&)=0;
  
  // Parameter derivative function - the supplied is the numerical derivative
  // Can be redefined in the derived class to give an analytical (much faster)
  // Returns the derivative matrix of all points wrt all parameters.
  // dydafunction[i][n] is dy(x_n)/da_i
  virtual table<arr> dydafunction(const arr&, const matr&);
  
  // Do not print fitting progress
  void setSilent();
  void setNormal();
  void setVerbose();
  
  // Set fit routine parameters
  void setAccuracy(double,double);
  void setFailretries(int);
  void setInitialStep(double);
  void setSuccessincr(double);
  void setFaildecr(double);
  
  // Perform fit, returning chisquare
  double fit(const arr& xx, const arr& yy,
	     const arr& ss, arr& pp);

  // Perform fit with fixed parameters indicated by fixa
  double fit(const arr& xx, const arr& yy,
	     const arr& ss, arr& pp,
	     const table<char>& ff);

  // Legacy calls - don't use in new code!
  double fit(const matr* xx, const matr* yy,
	     const matr* ss, matr* pp);

  // Perform fit with fixed parameters indicated by fixa
  double fit(const matr* xx, const matr* yy,
	     const matr* ss, matr* pp,
	     const matrix<char>* ff);

  matrix<double> getParameterError();
  matrix<double> getJointError(const table<char>&);
  matrix<double> getCovar();
  double getndata();
  double getnpar();
};

inline double lsqfitter3::getndata()
{
  return ndata;
}

inline double lsqfitter3::getnpar()
{
  return nfit;
}

inline void lsqfitter3::setSilent()
{
  verbose=0;
}

inline void lsqfitter3::setNormal()
{
  verbose=1;
}

inline void lsqfitter3::setVerbose()
{
  verbose=2;
}

inline void lsqfitter3::setAccuracy(double abs,double rel)
{
  ABSACC=abs;
  RELACC=rel;
}

inline void lsqfitter3::setFailretries(int n)
{
  FAILRETRIES=n;
}

inline void lsqfitter3::setSuccessincr(double si)
{
  SUCCESSINCR=si;
}

inline void lsqfitter3::setFaildecr(double fd)
{
  FAILDECR=fd;
}

inline void lsqfitter3::setInitialStep(double is)
{
  INITLAMBDA=is;
}

// Return the covariance matrix
inline  matrix<double> lsqfitter3::getCovar()
{
  return C;
}

// This is the callable routine, performs the fit
inline double lsqfitter3::fit(const arr& xx, const arr& yy,
	   const arr& ss, arr& pp)
{
  // Just creates a "fit all" fixpar matrix and calls the next fit routine
  table<char> ff(pp.n());
  ff=1;

  return fit(xx,yy,ss,pp,ff);
}

inline double lsqfitter3::fit(const matr* xx, const matr* yy,
	   const matr* ss, matr* pp)
{
  arr x,y,s,p;
  x= *xx;
  y= *yy;
  s= *ss;
  p= *pp;
  double chisq=fit(x,y,s,p);
  *pp=p.makematrix();
  return chisq;
}

// Perform fit with fixed parameters indicated by fixa
inline double lsqfitter3::fit(const matr* xx, const matr* yy,
			      const matr* ss, matr* pp,
			      const matrix<char>* ff)
{
  arr x,y,s,p;
  table<char> f;
  x= *xx;
  y= *yy;
  s= *ss;
  p= *pp;
  f= *ff;

  double chisq=fit(x,y,s,p,f);
  *pp=p.makematrix();
  return chisq;
}


#endif

