// $Id$
//
// The very few non-inlined parts of the biniostream classes

#include "biniostream.h"

// Define the byte order of the machine
#if BINIO_BIG_ENDIAN
const bool binfstream::machine_big_endian=true;
#else
const bool binfstream::machine_big_endian=false;
#endif

// Constructor to open an ofstream to filename and send output there
binfstream::binfstream(const char* filename,
		       std::ios::openmode mode) :
  std::fstream(filename,mode)
{
  file_native=1;
}

binfstream::binfstream(const std::string filename,
		       std::ios::openmode mode) :
  std::fstream(filename.c_str(),mode)
{
  file_native=1;
}

template<typename T> binfstream& binfstream::operator<<(T d)
{
  output (reinterpret_cast<const char*> (& d), sizeof (d));
  return *this;
}

template<typename T> binfstream& binfstream::operator>>(T& d)
{
  input (reinterpret_cast<char*> (& d), sizeof (d));
  return *this;
}

// explicit instantiations for built-in types
template binfstream& binfstream::operator<<(short int);
template binfstream& binfstream::operator<<(unsigned short int);
template binfstream& binfstream::operator<<(int);
template binfstream& binfstream::operator<<(unsigned int);
template binfstream& binfstream::operator<<(long);
template binfstream& binfstream::operator<<(unsigned long);
template binfstream& binfstream::operator<<(float);
template binfstream& binfstream::operator<<(double);
template binfstream& binfstream::operator<<(long double);

template binfstream& binfstream::operator>>(short int&);
template binfstream& binfstream::operator>>(unsigned short int&);
template binfstream& binfstream::operator>>(int&);
template binfstream& binfstream::operator>>(unsigned int&);
template binfstream& binfstream::operator>>(long&);
template binfstream& binfstream::operator>>(unsigned long&);
template binfstream& binfstream::operator>>(float&);
template binfstream& binfstream::operator>>(double&);
template binfstream& binfstream::operator>>(long double&);

// outputs n bytes starting at p to the stream
void binfstream::output (char const* p, unsigned int n) {
  if(file_native) // we can use the faster write method
    write (p, n);
  else {
    p+= n; // Adjust pointer for byte order
    while (n--)
      put (*(--p));
  }}

void binfstream::input (char* p, unsigned int n) {
  if(file_native)  // we can use the faster read () method
    read (p, n);
  else {
    p+= n;
    while (n--)
      get (*(--p));
  }}
