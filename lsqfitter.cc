// lsqfitter.cc
//
// Implementation of numerical least squares fitter class
// Translation to c++ from Numerical Recipes C program
//
// Patrik Jonsson 980227
//
// 980725: The main fit loop didn't work properly if a step failed (it
//         immediately quit the fitting.) Added a "fail" counter that retries
//         a number of times with smaller steps.
// 980726: Getdyda no longer is supplied with func, it reads it directly from
//         the object data structure (it never ever evaluates another function
//         anyway). We then have the availability to call either the numerical
//         getdyda or an explicitly supplied derivative function.

#include <cmath>
#include "lsqfitter.h"
#include<iostream> 

using namespace std;

const double lsqfitter::DY_ACCURACY=1e-5;

// Constructor doesn't do as much as I would like...
lsqfitter::lsqfitter( double(*f)(double, matrix<double>), double dystep )
{
  func=f;
  verbose=2; // Initially print info
  dydafunc=0;

  DY_INITIALSTEP=dystep;
  INITLAMBDA=0.001;
  SUCCESSINCR=10;
  FAILDECR=10;
  ABSACC = 1e-5;
  RELACC = 1+1e-5;
  FAILRETRIES =7;
}

// This constructor supplies a parameter derivative function
lsqfitter::lsqfitter( double(*f)(double, matrix<double>),
		      matrix<double>(*df)(double, matrix<double>))
{
  func=f;
  verbose=2; // Initially print info

  // Just set the parameter derivative function pointer
  dydafunc=df;

  DY_INITIALSTEP=0;
  INITLAMBDA=0.001;
  SUCCESSINCR=10;
  FAILDECR=10;
  ABSACC = 1e-5;
  RELACC = 1+1e-5;
  FAILRETRIES =7;
}

// This is the callable routine, performs the fit
double lsqfitter::fit(const matrix<double>* xx, const matrix<double>* yy,
	   const matrix<double>* ss, matrix<double>* pp)
{
  // Just creates a "fit all" fixpar matrix and calls the next fit routine
  matrix<char> ff(pp->getrows(),1,(char)1);

  return fit(xx,yy,ss,pp,&ff);
}

// This is the callable routine, performs the fit
double lsqfitter::fit(const matrix<double>* xx, const matrix<double>* yy,
	   const matrix<double>* ss, matrix<double>* pp,
		      const matrix<char>* ff)
{
  if(verbose==2)
    cout << "Starting fit\n";

  // Copy the pointers
  x=xx;
  y=yy;
  sig=ss;
  par=pp;
  fixpar=ff;

  npar = pp->getrows();
  ndata = xx->getrows();

  // Initialization originally in mrqmin
  //if (alambda < 0.0) { 
  atry=matrix<double>(npar,1,0.0);
  beta=matrix<double>(npar,1,0.0);
  da=matrix<double>(npar,1,0.0);
  covar=matrix<double>(npar,npar,0.0);
  alpha=matrix<double>(npar,npar,0.0);

  nfit=0;
  for (int j=0;j<npar;j++)
    if ((*fixpar)[j])
      nfit++;

  oneda=matrix<double>(nfit,1,0.0); // oneda and covar are different dims?
  // oneda=matrix<double>(npar,1,0.0);

  // Set initial value of alambda
  alambda=INITLAMBDA;
  
  mrqcof(par,&alpha,&beta);
  ochisq=chisq;
  for (int j=0;j<npar;j++) 
    atry[j]=(*par)[j];

  // This completes the initialization
  // Now we should iterate the fitting
  // we iterate until we've converged
  // should put in a "fail to converge" check

  if(verbose==2) {
    cout << "Initialization completed, starting iteration\n";
    cout << " initial chisquare is " << chisq << '\n';
  }
  
  int iter=0;
  int fail=0;
  double oxi2=chisq+1000;
  while( (((oxi2-chisq)>ABSACC) && ((oxi2/chisq)>RELACC)) || fail ){
  //while( 1) {
    oxi2=chisq;

    if(!mrqmin())
      // Step failed, increase count
      fail++;
    else
      // Reset count if a step succeeded
      fail=0;

    if(fail>FAILRETRIES)
      // Assume we're fucked
      fail=0;
     
    iter++;
    if(verbose==2) {
      cout << "Iteration " << iter << ":\n";
      cout << "  Chisquare = " << chisq << '\n';
      cout << "  Parameters are " << *par << "\n\n";
    }
    else if(verbose==1) {
      cout << iter << '\t' << chisq;
      for(int i=0;i<par->getrows();i++)
	cout << '\t' << (*par)[i];
      cout << '\n';
    }
  }
  
  // Call with alambda=0 to get the final result
  alambda=0;
  mrqmin();

  if(verbose==2) {
    cout << "\nFit converged with a chisquare of " << chisq;
    cout << "\n\nBest-fit parameters are:\n\t" << *par << '\t';
    cout << "\nCovariance matrix is\n\t" << covar << '\n'; 
  }
    
  return chisq;
}


int lsqfitter::mrqmin()
{
  // Alter linearized fitting matrix, by augmenting diagonal elements
  for (int j=0, l=1;l<=npar;l++) {
    if ((*fixpar)[l-1]) {
      j++;
      for (int k=0, m=1;m<=npar;m++) {
	if ((*fixpar)[m-1]) {
	  k++;
	  covar[j-1][k-1]=alpha[j-1][k-1];
	}
      }
      covar[j-1][j-1]=alpha[j-1][j-1]*( 1.0+alambda );
      //      oneda[j-1][0]=beta[j-1];
      oneda[j-1]=beta[j-1];
    }
  }
  
  // Matrix solution - returns inverse of covar in covar 
  // and solutions to COVAR*x=oneda in oneda
  gaussj(covar,&oneda,1);
  
  for (int j=0;j<nfit;j++) 
    da[j]=oneda[j][0];
  
  // This should also be moved to fit, it's the endfunc
  if (alambda == 0.0) {
    covsrt();
    return 1;
  }
  
  // Did the trial succeed?
  for (int j=0, l=0;l<npar;l++)
    if ((*fixpar)[l]) 
      atry[l]=(*par)[l]+da[++j-1];
  
  mrqcof(&atry,&covar,&da);
  
  int returnvalue;

  if (chisq < ochisq) {
    returnvalue=1; // Success
    // Success - return the new solution

    // Increase step for next time
    alambda *= SUCCESSINCR;
    if(verbose==2)
      cout << "success, new alambda is " << alambda << "\n";

    ochisq=chisq;
    for (int j=0, l=0;l<npar;l++) {
      if ((*fixpar)[l]) {
	j++;
	for (int k=0, m=0;m<npar;m++) {
	  if ((*fixpar)[m]) {
	    k++;
	    alpha[j-1][k-1]=covar[j-1][k-1];
	  }
	}
	beta[j-1]=da[j-1];
	(*par)[l]=atry[l];
      }
    }
  } 
  else {
    returnvalue=0; // Failed
    // Failure - increase alambda and return
    alambda *= FAILDECR;
    if(verbose==2)
      cout << "failure, new chisquare is " << chisq << ", new alambda is " << alambda << "\n";
    chisq=ochisq;
  }
  return returnvalue;
}


void lsqfitter::mrqcof(matrix<double>* p,matrix<double>* al,
		       matrix<double>* be)
{
  // we already know nfit
  //for (j=1;j<=npar;j++)
  //if (ia[j]) mfit++;
    
  // set alpha and beta to zero
  (*al)=0;
  (*be)=0;
  //for (int j=1;j<=nfit;j++) {
  //  for (int k=1;k<=j;k++) 
  //    alpha[j][k]=0.0;
  //  beta[j]=0.0;
  /// }

  chisq=0.0;
  
  // Summation loop over data
  for (int i=0;i<ndata;i++) {
    
    double cur_y = (*func)((*x)[i],(*p));

    // Call the dyda function
    matrix<double> dyda;
    if(dydafunc) // We have an explicit parameter derivative function
      dyda = (*dydafunc)( (*x)[i], *p );
    else // Use the numerical one
      dyda = getdyda( (*x)[i], *p);
    
    double sig2i=1.0/((*sig)[i]*(*sig)[i]);
    double dy=(*y)[i]-cur_y;
    
    for (int j=0, l=0;l<npar;l++) {
     if ((*fixpar)[l]) {
	
	double wt=dyda[l]*sig2i;
	
	j++;
	for (int k=0,m=0;m<=l;m++) {
	  if ((*fixpar)[m]) 
	    (*al)[j-1][++k-1] += wt*dyda[m];
	}
	(*be)[j-1] += dy*wt;
      }
    }
    chisq += dy*dy*sig2i;
  }
  
  // Fill in the symmetric side
  for (int j=1;j<nfit;j++)
    for (int k=0;k<j-1;k++)
      (*al)[k][j]=(*al)[j][k];
}

// Linear equation solution by Gauss-Jordan elimination
// A is the input matrix of size n*n
// b is an array of m st right hand side vectors that will be solved
void lsqfitter::gaussj(matrix<double>& A, matrix<double>* b,int m)
{
  //  short int n=A.getrows(); // problem here - is n = nfit or npar?
  short int n=nfit;

  matrix<short int> indxc(n,1,(short int)0);
  matrix<short int> indxr(n,1,(short int)0);
  matrix<short int> ipiv(n,1,(short int)0);

  int icol,irow;

  for (int i=0;i<n;i++) {
    double big=0.0;
    for (int j=0;j<n;j++)
      if (ipiv[j] != (short)1)
	for (int k=0;k<n;k++) {
	  if (ipiv[k] == (short)0) {
	    if (fabs(A[j][k]) >= big) {
	      big=fabs(A[j][k]);
	      irow=j;
	      icol=k;
	    }
	  } 
	  else if (ipiv[k] > 1) 
	    //nrerror("gaussj: Singular Matrix-1");
	    cout << "Oops, singular matrix-1, wtf?\n";
	}
    ipiv[icol]+=1;

    // We now have the pivot element, so we interchange rows, if needed,
    // to put the pivot element on the diagonal. The columns are not
    // physically interchanged, only relabelled:
    // indxc[i] is the column of the ith pivot element, is the ith column
    // that is reduced, while indxr[i] is the row in which that pivot element
    // was originally located. With this form of bookkeeping, the solution b's
    // will end up in the correct order, and the inverse matrix will be
    // scrambled by columns.

    if (irow != icol) {
      for (int l=0;l<n;l++) {
	double temp=A[irow][l];
	A[irow][l]=A[icol][l];
	A[icol][l]=temp;
      }
      for (int l=0;l<m;l++) {
	double temp=(b[l])[irow];
	(b[l])[irow]=(b[l])[icol];
	(b[l])[icol]=temp;
      }
    }

    // We are now ready to divide the pivot row by the pivot element
    // located at irow,icol

    indxr[i]=irow;
    indxc[i]=icol;
    if (A[icol][icol] == 0.0)
      //nrerror("gaussj: Singular Matrix-2");
      cout << "Oops, singular matrix-2, wtf?\n";
    
    double pivinv=1.0/A[icol][icol];
    A[icol][icol]=1.0;

    for (int l=0;l<n;l++) 
      A[icol][l] *= pivinv;
    for (int l=0;l<m;l++) 
      (b[l])[icol] *= pivinv;

    // Next, reduce the rows
    for (int ll=0;ll<n;ll++)
      if (ll != icol) { // except for the pivot row, of course.
	double dum=A[ll][icol];
	A[ll][icol]=0.0;
	for (int l=0;l<n;l++) 
	  A[ll][l] -= A[icol][l]*dum;
	for (int l=0;l<m;l++) 
	  (b[l])[ll] -= (b[l])[icol]*dum;
	  //	  (b[l])[ll] -= (b[l])[icol]*dum;
      }
  }

  // This is the end of the main loop over columns of the reduction. It only
  // remains to unscramble the solution in view of the column interchanges
  // We do this by interchanging pairs of columns in reverse order that
  // the permutation was built up.
  for (int l=n-1;l>=0;l--)
    if (indxr[l] != indxc[l])
      for (int k=0;k<n;k++) {
	double temp=A[k][indxr[l]];
	A[k][indxr[l]]=A[k][indxc[l]];
	A[k][indxc[l]]=temp;
      }
  // And we are done
}

void lsqfitter::covsrt()
{
  for (int i=nfit;i<npar;i++)
    for (int j=0;j<=i;j++) 
      covar[i][j]=covar[j][i]=0.0;

  int k=nfit-1;

  for (int j=npar-1;j>=0;j--) {
    if ((*fixpar)[j]) {
      for (int i=0;i<npar;i++) {
	double temp=covar[i][k];
	covar[i][k]=covar[i][j];
	covar[i][j]=temp;
      }
      for (int i=0;i<npar;i++) {
	double temp=covar[k][i];
	covar[k][i]=covar[j][i];
	covar[j][i]=temp;
      }
      k--;
    }
  }
}

matrix<double> lsqfitter::getdyda(double x,const matrix<double>& a)
{
  // We want to compute the derivative of function f wrt the parameters a
  // around the current point x,a
  // We do this by using the finite difference approximation
  // dy/dx = 1/(2h)*( y(x+h)-y(x-h) )

  //cout << "Evaluating dy/da:\n";
  matrix<double> da(a.getrows(),1,0.); // Small difference in a
  matrix<double> dy(a); // contains derivatives

  // Loop over the parameters a
  for(int i=0; i<a.getrows(); i++)
    if((*fixpar)[i]) {
      // We need a GOOD arbitrary starting guess for h, the parameter diff da
      // If h is too large we will have to iterate for long to converge
      // if it's too small we'll get cancellation and bad precision
      // (Note that h could well be very different for the diff parameters)
      double h=DY_INITIALSTEP;
      double ody=1;
      dy[i]=0;
      int iter=0;
      // Shrink step until we've converged
      while( fabs(dy[i]-ody)>DY_ACCURACY ) {
	da[i]=h;
	ody=dy[i];
	dy[i]=( (*func)(x,a+da) - (*func)(x,a-da) )/(2*h);
	h/=5;
	iter++;
      }
      da[i]=0;
      //cout << "Parameter#" << i << " required "<< iter << " at x=" << x << '\n';
    }
  return dy;
}
