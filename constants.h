#ifndef __constants__
#define __constants__

#include <cmath>


namespace constants {

  // physical constants
  // all units are SI
  const double c0 = 2.99792458e8; // speed of light in m/s
  const double k  = 1.38066e-23;  // Boltzmann's const in J/K
  const double mp = 1.672623e-27; // proton mass in kg
  const double sigma_SB = 5.67051e-8; // Stefan-Boltzmann's constant in W/(m^2 K^4)
  const double N_A = 6.02214179e23;// Avogadro's constant in mol^-1
  const double R = N_A*k;          // Gas constant in J/(K*mol) 

  // astro constants and units
  const double Lsun = 4e26;          // Solar lum in W
  const double Msun = 1.989e30;      // Solar mass in kg
  const double kpc = 3.085678e19;    // the number of m in a kpc  
  const double yr = 3600*24*365.225; // the number of s in a year

  // math constants

  const double e = exp(1.);
  const double ln10 = log(10.);
  const double invln10 = 1./log(10.);

#ifdef KCC
  static const double 
  pi= 3.141592653589793238462643383279502884197169399375105820974944;
#else
  // KCC evaluates this to 0, for some reason... 
  static const double pi=4*atan(1.); 
#endif
  
};

#endif
