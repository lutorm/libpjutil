// rungekutta.cc
// Integrates 1st order ODE's using a 5th order Runge-Kutta method with 
// adaptive step size, from Numerical Recipes.
//
// $Id$
// $Log$
// Revision 1.3  1999/07/20 23:21:20  patrik
// Fixed return type warnings for cxx.
//
// Revision 1.2  1999/04/17 08:18:18  patrik
// I have no idea what was changed here... :(
//
// Revision 1.1  1999/03/10 10:17:30  patrik
// Implements an embedded 5th order Cash-Karp Runge-Kutta ODE solver from
// Numerical Recipes. It does adaptive step size to a specified accuracy and
// can be run step by step (RKadvance) or to a specified x (solve).
// Not exhaustively tested but at least reproduces a sine wave from 0-100...
//

#include "rungekutta_template.h"
#include "matrix.h"

double CashKarp::a2=0.2;
double CashKarp::a3=0.3;
double CashKarp::a4=0.6;
double CashKarp::a5=1.0;
double CashKarp::a6=0.875;

double CashKarp::b21=0.2;
double CashKarp::b31=0.075;
double CashKarp::b32=0.225;
double CashKarp::b41=0.3;
double CashKarp::b42=-0.9;
double CashKarp::b43=1.2;
double CashKarp::b51=-11./54;
double CashKarp::b52=2.5;
double CashKarp::b53=-70./27;
double CashKarp::b54=35./27;
double CashKarp::b61=1631./55296;
double CashKarp::b62=175./512;
double CashKarp::b63=575./13824;
double CashKarp::b64=44275./110592;
double CashKarp::b65=253./4096;

double CashKarp::c1=37./378;
double CashKarp::c3=250./621;
double CashKarp::c4=125./594;
double CashKarp::c6=512./1771;

double CashKarp::dc1=37./378-2825./27648;
double CashKarp::dc3=250./621-18575./48384;
double CashKarp::dc4=125./594-13525./55296;
double CashKarp::dc5=0;
double CashKarp::dc6=512./1771-0.25;

