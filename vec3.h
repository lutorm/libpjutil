// This lean-and-mean 3-vector class is based on the matrix class but
// is tweaked for only 3vector access which means *a lot*.
//
// $Id$

#ifndef __vec3__
#define __vec3__

#include <math.h>
#include "matrix.h"
#include "biniostream.h"

template<class R> class mat3;
template<class T> class vec3;

template<class T>
const T dot(const vec3<T>&, const vec3<T>&);

template<class T> class vec3 {
  //template<class R> friend class vec3<R>;
private:
  T c[3]; // just keeps 3 components

  // exception class for out-of-bonds
  class index_error {
  public:
    int i;
    index_error(int a) {i=a;};
  }; 
  
  void rangecheck(int) const; // Checks if given index is valid
   
 public:
  vec3();
  vec3(T,T,T);
  vec3(T);
  template<class R> operator vec3<R>();
  vec3(const vec3&);
  ~vec3();

  // conversions
  operator matrix<T>() const;

  // vec3 unary operators

  const vec3 operator-() const;
  T& operator[](int);
  const T& operator[](int) const;

  // vec3 binary operators (element-wise)

  const vec3 operator+(const vec3&) const;
  const vec3 operator-(const vec3&) const;
  const vec3 operator*(const vec3&) const;
  const vec3 operator/(const vec3&) const;

  vec3& operator=(const vec3&);
  vec3& operator+=(const vec3&);
  vec3& operator-=(const vec3&);
  vec3& operator*=(const vec3&);
  vec3& operator/=(const vec3&);

  bool operator==(const vec3&) const;
  bool operator!=(const vec3&) const;
  bool operator<(const vec3&) const;
  bool operator>(const vec3&) const;
  bool operator<=(const vec3&) const;
  bool operator>=(const vec3&) const;

  // vec3-scalar operators (element-wise operations)

  const vec3 operator+(T) const;
  const vec3 operator-(T) const;
  const vec3 operator*(T) const;
  const vec3 operator/(T) const;

  //friend const vec3 operator+(T, const vec3&);
  //friend const vec3 operator-(T, const vec3&);
  //friend const vec3 operator*(T, const vec3&);
  //friend const vec3 operator/(T, const vec3&);

  vec3& operator=(T);
  vec3& operator+=(T);
  vec3& operator-=(T);
  vec3& operator*=(T);
  vec3& operator/=(T);

  bool operator==(T) const;
  bool operator!=(T) const;
  bool operator<(T) const;
  bool operator<=(T) const;
  bool operator>(T) const;
  bool operator>=(T) const;
  //friend bool operator==(T, const vec3&);
  //friend bool operator!=(T, const vec3&);
  //friend bool operator<(T, const vec3&);
  //friend bool operator<=(T, const vec3&);
  //friend bool operator>(T, const vec3&);
  //friend bool operator>=(T, const vec3&);

  // Vector algebra operations

  //friend const vec3 cross(const vec3&, const vec3&);
  vec3& cross(const vec3&);
  mat3<T> star() const;
  friend const T dot<T> (const vec3&, const vec3&);
  const T mag() const;
  vec3& norm();
  //friend const vec3 norm(const vec3&);
  vec3& cart2sph();  
  vec3& sph2cart();

  int min() const; // Finds min element
  int max() const; // Finds max element

  // I/O operators
  //friend std::ostream& operator<< <T>(std::ostream&, const vec3&);
  //friend istream& operator>> <T>(istream&, vec3&);

  //friend binfstream& operator<< <T> (binfstream&, const vec3&);
  //friend binfstream& operator>> <T> (binfstream&, vec3&);  
};

#include "mat3.h"

// Default vec3 constructor
// Creates null vector
template<class T> inline vec3<T>::vec3()
{
  c[0]=0;
  c[1]=0;
  c[2]=0;
}

// Constructor for vec3 of constant value
template<class T> inline vec3<T>::vec3(T v)
{
  c[0]=v;
  c[1]=v;
  c[2]=v;
}

// Constructor for vec3 with specified components
template<class T> inline vec3<T>::vec3(T vx, T vy, T vz)
{
  c[0]=vx;
  c[1]=vy;
  c[2]=vz;
}

// Copy constructor (actually unnecessary...)
template<class T> inline vec3<T>::vec3(const vec3& m)
{
  c[0]=m.c[0];
  c[1]=m.c[1];
  c[2]=m.c[2];
}

// Implicit conversion operator between different vec3<>'s
template<class T> template<class R>
inline vec3<T>::operator vec3<R>()
{
  vec3<R> v(static_cast<R>(c[0]),
	    static_cast<R>(c[1]),
	    static_cast<R>(c[2]));
  return v;
}

// Destructor doesn't do anything
template<class T> inline vec3<T>::~vec3()
{
}

// Checks whether specified component is valid
// it throws an exception if it isn't
// if the define is defined it is skipped completely, to avoid overhead
template<class T> inline void vec3<T>::rangecheck(int i) const
{
#ifndef VEC3_NODEBUG
  if( (i<0) || (i>2) )
    throw index_error(i);
#endif
}


// ***
// *** vec3 unary operators ***
// ***

// Unary negation operator
template<class T> inline const vec3<T> vec3<T>::operator-() const
{
  vec3 a;

  a.c[0]=-c[0];
  a.c[1]=-c[1];
  a.c[2]=-c[2];

  return a;
}

// const version of index operator
template<class T> inline const T& vec3<T>::operator[](int i) const
{
  rangecheck(i);
    
  return c[i];
}

// non-const version of index operator can modify its contents
template<class T> inline T& vec3<T>::operator[](int i)
{
  rangecheck(i);
    
  return c[i];
}

// ***
// *** vec3-vec3 operators ***
// ***

// vec3 assignment operator
template<class T> inline vec3<T>& vec3<T>::operator=(const vec3& m)
{
  c[0]=m.c[0];
  c[1]=m.c[1];
  c[2]=m.c[2];

  return *this;
}

// Adds vec3 m to this
template<class T> inline vec3<T>& vec3<T>::operator+=(const vec3& m)
{
  c[0]+=m.c[0];
  c[1]+=m.c[1];
  c[2]+=m.c[2];

  return *this;
}

// Subtracts vec3 m from this
template<class T> inline vec3<T>& vec3<T>::operator-=(const vec3& m)
{
  c[0]-=m.c[0];
  c[1]-=m.c[1];
  c[2]-=m.c[2];

  return *this;
}

// Element-wise multiplication
template<class T> inline vec3<T>& vec3<T>::operator*=(const vec3& m)
{
  c[0]*=m.c[0];
  c[1]*=m.c[1];
  c[2]*=m.c[2];

  return *this;
}

// Element-wise division
template<class T> inline vec3<T>& vec3<T>::operator/=(const vec3& m)
{
  c[0]/=m.c[0];
  c[1]/=m.c[1];
  c[2]/=m.c[2];

  return *this;
}

// Compares this with vec3 m
// vec3s are equal if all elements equal
template<class T> inline bool vec3<T>::operator==(const vec3& m) const
{
  if((c[0]==m.c[0])&&(c[1]==m.c[1])&&(c[2]==m.c[2]))
    return true;
  else
    return false;
}

template<class T> inline bool vec3<T>::operator!=(const vec3& m) const
{
  if((c[0]==m.c[0])&&(c[1]==m.c[1])&&(c[2]==m.c[2]))
    return false;
  else
    return true;
}

// Compares this with vec3 m
// vec3s are less if all elements less
template<class T> inline bool vec3<T>::operator<(const vec3& m) const
{
  if((c[0]<m.c[0])&&(c[1]<m.c[1])&&(c[2]<m.c[2]))
    return true;
  else
    return false;
}

// Compares this with vec3 m
// vec3s are less if all elements less
template<class T> inline bool vec3<T>::operator<=(const vec3& m) const
{
  if((c[0]<=m.c[0])&&(c[1]<=m.c[1])&&(c[2]<=m.c[2]))
    return true;
  else
    return false;
}

// Compares this with vec3 m
// vec3s are less if all elements less
template<class T> inline bool vec3<T>::operator>(const vec3& m) const
{
  if((c[0]>m.c[0])&&(c[1]>m.c[1])&&(c[2]>m.c[2]))
    return true;
  else
    return false;
}

// Compares this with vec3 m
// vec3s are less if all elements less
template<class T> inline bool vec3<T>::operator>=(const vec3& m) const
{
  if((c[0]>=m.c[0])&&(c[1]>=m.c[1])&&(c[2]>=m.c[2]))
    return true;
  else
    return false;
}


// ***
// *** vec3-scalar operators (element-wise operations) ***
// ***

// Assigns constant value to vec3
template<class T> inline vec3<T>& vec3<T>::operator=(T d)
{
  c[0]=d;
  c[1]=d;
  c[2]=d;

  return *this;
}

// Adds d to all elements of vec3
template<class T> inline vec3<T>& vec3<T>::operator+=(T d)
{
  c[0]+=d;
  c[1]+=d;
  c[2]+=d;

  return *this;
}

// Subtracts d from all elements of vec3
template<class T> inline vec3<T>& vec3<T>::operator-=(T d)
{
  c[0]-=d;
  c[1]-=d;
  c[2]-=d;
  
  return *this;
}

// Multiplies all elements of vec3 by d
template<class T> inline vec3<T>& vec3<T>::operator*=(T d)
{
  c[0]*=d;
  c[1]*=d;
  c[2]*=d;

  return *this;
}

// Divides all elements of vec3 by d
template<class T> inline vec3<T>& vec3<T>::operator/=(T d)
{
  c[0]/=d;
  c[1]/=d;
  c[2]/=d;

  return *this;
}

// Compares this with scalar d
// vec3s are equal if all elements equal to d
template<class T> inline bool vec3<T>::operator==(const T d) const
{
  if((c[0]==d)&&(c[1]==d)&&(c[2]==d))
    return true;
  else
    return false;
}

template<class T> inline bool vec3<T>::operator!=(const T d) const
{
  if((c[0]==d)&&(c[1]==d)&&(c[2]==d))
    return false;
  else
    return true;
}

template<class T> inline bool vec3<T>::operator<(const T d) const
{
  if((c[0]<d)&&(c[1]<d)&&(c[2]<d))
    return true;
  else
    return false;
}

template<class T> inline bool vec3<T>::operator<=(const T d) const
{
  if((c[0]<=d)&&(c[1]<=d)&&(c[2]<=d))
    return true;
  else
    return false;
}

template<class T> inline bool vec3<T>::operator>(const T d) const
{
  if((c[0]>d)&&(c[1]>d)&&(c[2]>d))
    return true;
  else
    return false;
}

template<class T> inline bool vec3<T>::operator>=(const T d) const
{
  if((c[0]>=d)&&(c[1]>=d)&&(c[2]>=d))
    return true;
  else
    return false;
}

// Vector algebra routines

// Calculates standard Euclidean Dot Product of two vectors
template<class T> inline const T dot(const vec3<T>& a, const vec3<T>& b)
{
  T p=0;
  p+=a.c[0]*b.c[0];
  p+=a.c[1]*b.c[1];
  p+=a.c[2]*b.c[2];

  return p;
}

// Calculates cross product of this and vec3
template<class T> inline vec3<T>& vec3<T>::cross(const vec3<T>& b)
{
  T t1=c[2]*b.c[0]-c[0]*b.c[2];
  T t2=c[0]*b.c[1]-c[1]*b.c[0];
  c[0]=c[1]*b.c[2]-c[2]*b.c[1];
  c[1]=t1;
  c[2]=t2;

  return *this;
}

template<class T> 
inline const vec3<T> cross(const vec3<T>& a, const vec3<T>& b)
{
  return vec3<T>(a).cross(b);
}

// Returns the '*' matrix such that a* b = axb (Baraff)
template<class T> inline mat3<T> vec3<T>::star() const
{
  return mat3<T>(0,-c[2],c[1],
		 c[2],0,-c[0],
		 -c[1],c[0],0);
}

template<class T> inline const T vec3<T>::mag() const
{
  return sqrt(dot(*this,*this));
}

// Normalizes this to unit length
template<class T> inline vec3<T>& vec3<T>::norm()
{
  return (*this)/=mag();
}

template<class T> inline const vec3<T> norm(const vec3<T>& m)
{
  return m/m.mag();
}

// Transforms from cartesian to spherical coordinates
template<class T> inline vec3<T>& vec3<T>::cart2sph()
{
  T t0=c[0];
  T t1=c[1];
  T t2=c[2];

  c[0] = mag();
  c[1] = acos( t2/c[0] );
  c[2] = atan2( t1, t0 );

  return *this;
}

// Transforms from spherical to cartesian coordinates
template<class T> inline vec3<T>& vec3<T>::sph2cart()
{
  T mag=c[0];

  T s1=sin( c[1] );
  T c1=cos( c[1] );

  c[0] = mag*s1*cos( c[2] );
  c[1] = mag*s1*sin( c[2] );
  c[2] = mag*c1;

  return *this;
}

// If several elements are equal it will return the first of them
template<class T> inline int vec3<T>::min() const
{
  int i=c[0]<c[1]?0:1;
  i=c[2]<c[i]?2:i;
  return i;
}

template<class T> inline int vec3<T>::max() const
{
  int i=c[0]>c[1]?0:1;
  i=c[2]>c[i]?2:i;
  return i;
}


// ***
// *** I/O operators ***
// ***

// User-defined input operator for vec3 type
template<class T> inline std::istream& operator>>(std::istream& s, vec3<T>& m)
{
  // it's not absolutely straightforward to make this function behaved
  // like the built-in ones in the sense that it shouldn't consume any
  // part of the  stream if input fails.
  s >> m[0] >> m[1] >> m[2];

  return s;
}

// User-defined output operator for vec3 type
template<class T> inline std::ostream& operator<<(std::ostream& s, const vec3<T>& m)
{
  s << "( " << m[0] << ", " << m[1] << ", " << m[2] << " )";

  return s;
}

//  binary output operator for vec3
template<class T> inline binofstream& operator<<(binofstream& s, const vec3<T>& m)
{
  s << m[0] << m[1] << m[2];

  return s;
}

// User-defined binary input operator for vec3 type
template<class T> inline binifstream& operator>>(binifstream& s, vec3<T>& m)
{
  s >> m[0] >> m[1] >> m[2];

  return s;
}


//
// The binary operators are just implemented using the copy constructor
// and the unary x= operators, so use the latter if performance is an issue
//

template<class T> 
inline const vec3<T> vec3<T>::operator+(const vec3<T>& m) const
{
  return vec3(*this) += m;
}

template<class T>
inline const vec3<T> vec3<T>::operator-(const vec3<T>& m) const
{
  return vec3(*this) -= m;
}

template<class T> 
inline const vec3<T> vec3<T>::operator*(const vec3<T>& m) const
{
  return vec3(*this) *= m;
}

template<class T> 
inline const vec3<T> vec3<T>::operator/(const vec3<T>& m) const
{
  return vec3(*this) /= m;
}

template<class T> 
inline const vec3<T> vec3<T>::operator+(T d) const
{
  return vec3(*this) += d;
}

template<class T> 
inline const vec3<T> vec3<T>::operator-(T d) const
{
  return vec3(*this) -= d;
}

template<class T> 
inline const vec3<T> vec3<T>::operator*(T d) const
{
  return vec3(*this) *= d;
}

template<class T> 
inline const vec3<T> vec3<T>::operator/(T d) const
{
  return vec3(*this) /= d;
}

template<class T> 
inline const vec3<T> operator+(T d, const vec3<T>& m)
{
  return vec3<T>(m) += d;
}

template<class T> 
inline const vec3<T> operator-(T d, const vec3<T>& m)
{
  vec3<T> a(-m);
  return vec3<T>(a) += d;
}

template<class T> 
inline const vec3<T> operator*(T d, const vec3<T>& m)
{
  return vec3<T>(m) *= d;
}

template<class T> 
inline const vec3<T> operator/(T d, const vec3<T>& m)
{
  return vec3<T>(d) /= m;
}

template<class T> 
inline bool operator==(T d, const vec3<T>& m)
{
  return (m==d);
}

template<class T> 
inline bool operator!=(T d, const vec3<T>& m)
{
  return (m!=d);
}

template<class T> 
inline bool operator<(T d, const vec3<T>& m)
{
  return (m>d);
}

template<class T> 
inline bool operator<=(T d, const vec3<T>& m)
{
  return (m>=d);
}

template<class T> 
inline bool operator>(T d, const vec3<T>& m)
{
  return (m<d);
}

template<class T> 
inline bool operator>=(T d, const vec3<T>& m)
{
  return (m<=d);
}

template<class T> 
inline vec3<T>::operator matrix<T>() const
{
  return matrix<T>(c[0],c[1],c[2]);
}


#endif
