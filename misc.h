/// \file misc.h
/// Miscellaneous routines. Declarations of some functions that come in
/// handy, mostly to do with path manipulation.
// $Id$

#ifndef __misc__
#define __misc__

#include <stdlib.h>
#include <istream>
#include <vector>
#include <string>

void seed(long int);

// Return a random number, since drand48 doesn't work on the stupid ppc.
inline double rnd()
{
  // what is the proper pp define for linuxppc?
#ifdef LINUXPPC
  return double(rand())/RAND_MAX;
#else
  return drand48();
#endif
}

// Read the real-time counter in the PowerPC to time programs
static inline int realcc(void) {
#ifdef LINUXPPC
  unsigned long cc;
  /* read the 64 bit process cycle counter into variable cc: */
  asm volatile("mftb %0" : "=r"(cc) : : "memory");
  //asm volatile("rpcc %0" : "=r"(cc) : : "memory");
  return cc;                    /* return the lower 32 bits */
#else
  return 0;
#endif
}

//class istream; //kcc
std::istream& eatwhite(std::istream& is);

bool suffix_is (const std::string & s, const std::string & suffix);
std::string extract_directory (const std::string & s);
std::string strip_directory (const std::string & s);
std::string strip_suffix (const std::string & s);
std::vector<std::string> word_expand(const std::string& s);

#endif
