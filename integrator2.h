// integrator2.h
//
// Declaration of numerical integrator routines
// Based on fortran code my Mike Gross
// Uses fifth order Runge-Kutta with adaptive step size
// 
// Patrik Jonsson
//
/*
One integrator object is initialised to integrate a specific function
to a specified accuracy with a specified starting step.
The syntax of the integrating function is
double integrand(double x, double* parameters)
where the parameters are an arbitrary number of parameters that are not
integrated over.

Integration is performed with
integral = i.Integrate(lower, upper, ... )
where the ellipsis should be the same number of parameters that the integrand
is taking.
*/

// $Id$
// $Log$
// Revision 1.1  1999/03/10 22:27:44  patrik
// Implemented integrator2, where the integrand is implemented in a derived
// class instead of taken as a pointer to a global function. (Like lsqfitter2.)
// Parameters to the integrand are no longer passed in the call to integrate but
// defined directly in the derived class. All in all, code is simpler now.
//

#ifndef __integrator2__
#define __integrator2__

#include "matrix.h"

class integrator2 {
private:
  double accuracy;                     // Required accuracy;
  double startstep;                    // First-guess step size

  double x,y,dydx,yscale;              // Integration variables

  double Runge5(double h,double& yerr);
  double Makestep(double htry);
  double sign(double val, double sgn);

protected:
  virtual double integrand(double)=0;

public:
  integrator2();
  integrator2( double a, double s );

  void setaccuracy(double,double);

  double integrate(double a, double b); // Make integration

};


inline integrator2::integrator2()
{
  accuracy = 1;
  startstep = 1;
}

inline integrator2::integrator2(double a, double s)
{
  accuracy = a;
  startstep = s;
}

inline void integrator2::setaccuracy(double a, double s)
{
  accuracy = a;
  startstep = s;
}
  

#endif

