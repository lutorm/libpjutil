// ndim_interpolator.h
//
// $Id$
// $Log$
// Revision 1.8  2000/01/28 12:44:19  patrik
// There was a bug in the index() function, a postfix i-- was evaluated early.
// I wonder if it's because it was in a user-defined operator that might
// switch the order... Gotta watch that!
//
// Revision 1.7  1999/12/20 10:18:30  patrik
// Changed the implementation to use table<>'s instead of normal arrays. That
// way we get range checking and allocation taken care of, but if we disable
// checking it shouldn't be a lot slower.
// Also made index() an inline.
//
// Revision 1.6  1999/12/01 03:04:20  patrik
// Tweaked the reduce() for speedup. We used to allocate new arrays for
// every recursion but that's really not necessary because we can reduce
// in place.
// Also sped up index() by adding a multiplier array that is set up by the
// constructor, so we don't have to multiply that all the time.
//
// Revision 1.5  1999/12/01 00:51:20  patrik
// Made the main callable interpolate() use a table instead of the slow
// va_arg. That way can still be used but it just assembles a table and then
// calls the other interpolate(). This gave quite a nice speedup.
//
// Revision 1.4  1999/11/05 01:38:45  patrik
// Fixed a delete[] bug for points_indep that I think was responsible for
// segfaulting. It was stupid, I looped and freed the same memory over
// and over...  Also added a routine writedata() for writing the interpolation
// table to a file.
//
// Revision 1.3  1999/07/20 22:11:53  patrik
// Changed some variables-size array declarations to proper new's. g++ apparently
// doesn't complain about this, but cxx surely does...
//
// Revision 1.2  1999/03/09 21:12:21  patrik
// Now uses the table class to store the grid points, so it at least does
// bisection search instead of the look-through model it did before.
//
// Revision 1.1  1998/11/20 02:56:30  patrik
// ndim_interpolator is a class that does linear interpolation in n dimensions
// on a regular grid. Tested with up to 3 dimensions, but not exhaustively so.
//

/*
Interpolation routines assume that x values are rising. If extrapolation
is attempted the routines will return 0.
*/

#ifndef __ndim_interpolator__
#define __ndim_interpolator__

#include "tableutils.h"

// Can perform n-dimensional interpolation on an orthogonal grid
class ndim_interpolator {
protected:
  int ndim; // Number of dimensions (# of indep. variables)
  int* npoints; // Number of points in the dimensions;
  int* multi;   // Multiplier for the indexing routine

  //int points_n;
  table<double>* points_indep;
  //double** points_indep;
  double* points_dep;

  int index(table<int>&) const;
  double reduce(int,table<double>&,table<double>&);

  //ndim_interpolator(int);
  //ndim_interpolator(int,double*,double*);

 public:
  ndim_interpolator(int...); // N-dimensional interpolator
  ndim_interpolator(char*); // Reads interpolation data from file
  ~ndim_interpolator();

  void setPoint(int...); // set a data point
  double interpolate(table<double>&);
  double interpolate(double...);
  void writedata(char*); // Writes the interpolation table
};

// *** INLINES ***

// Destructor deletes arrays
inline ndim_interpolator::~ndim_interpolator()
{
  if(points_dep)
    delete[] points_dep;
  if(points_indep)
    delete[] points_indep;
  if(npoints)
    delete[] npoints;
  if(multi)
    delete[]multi;
}

// Calculates the index of the components in j
inline int ndim_interpolator::index(table<int>& ji) const
{
  int j=ji[0];
  int i=ndim;
  while(--i)
    j+=multi[i]*ji[i];
  return j;
}

/*
// Calculates the index of the components in j
inline int ndim_interpolator::index(int* ji)
{
  int j=ji[0];
  int i=ndim-1;
  while(i)
    j+=multi[i]*ji[i--];
  return j;
}

  int err=0;
  int j=ji[0];
  if((j<0)||(j>npoints[0]-1)) {
    cerr << "ndim_interpolator::index - array index[0] out of range: " << j
	 << '\n';
    err=1;
  }

  int m=1;

  for(int i=1;i<ndim;i++) {
    if((ji[i]<0)||(ji[i]>npoints[i]-1)) {
      cerr << "ndim_interpolator::index - array index[" << i 
	   << "] out of range: " << ji[i] << '\n';
      err=1;
    }
    m*= npoints[i-1];
    j+= m * ji[i];
  }
  if(err)
    j=0;

  return j;
}
*/

#endif


