// rungekutta.h
// Integrates 1st order ODE's using a 5th order Runge-Kutta method with 
// adaptive step size, from Numerical Recipes.
//
// The problem is posed as
//
//   dy/dx = f(x,y,a)
// 
// where y is a vector of n dependent variables, and a are parameters

// $Id$
// $Log$
// Revision 1.2  2004/01/26 19:02:28  patrik
// Tweaked the class so it can be used in cases where the template state
// class does not have a (usable) default constructor, as is the case
// with the blitz Arrays.  All state objects are now initialized using
// the copy constructor. Also changed the RKintegrator constructor to do
// initialization instead of assignment to the state variables, for the
// same reason.
//
// Finally, removed some remnants of the old matrix class.
//
// Revision 1.1  2000/03/10 21:59:33  patrik
// Turned the rungekutta integrator into a template so that the dependent
// variables can be an arbitrary class. Then only thing needed is that
// operator*(double) and operator+(T), renormalize(T) which is element-wise
// division, and max() which returns the largest number in the structure.
//
// Seems to work fine.
//
// Revision 1.3  1999/07/20 23:21:20  patrik
// Fixed return type warnings for cxx.
//
// Revision 1.2  1999/04/17 08:18:18  patrik
// I have no idea what was changed here... :(
//
// Revision 1.1  1999/03/10 10:17:30  patrik
// Implements an embedded 5th order Cash-Karp Runge-Kutta ODE solver from
// Numerical Recipes. It does adaptive step size to a specified accuracy and
// can be run step by step (RKadvance) or to a specified x (solve).
// Not exhaustively tested but at least reproduces a sine wave from 0-100...
//

#ifndef __rungekutta_template__
#define __rungekutta_template__

#include <cmath>
#include <iostream>

class CashKarp {
 protected:
  // Cash-Karp constants
  static double a2,a3,a4,a5,a6,
    b21,b31,b32,b41,b42,b43,b51,b52,b53,b54,b61,b62,b63,b64,b65,
    c1,c3,c4,c6,
    dc1,dc3,dc4,dc5,dc6;
};

template<class T>
class RKintegrator : public CashKarp {
protected:
  // Problem parameters
  T yscale;                     // Scaling of dependent variables
  double eps;                   // Tolerance
  double hmax;                  // Maximum step
  double hdid,hnext;

  // Current solution point
  T yerr;
  double curx;
  T cury;
  T curdydx;

  // Takes a RK step of length h, trunc error est in yerr
  T RKstep(double x, const T& y, const T& dydx, double h);

protected:
  // Routine supplying the right-hand side derivatives that must be redefined
  virtual T dydxfunction(double,const T&)=0;

public:
  RKintegrator(double,const T&,double,const T&,double);
  //~RKintegrator();

  T solve(double); // Advances the solution and returns result
  // Advances the solution one step, keeping track of the truncation error
  void RKadvance();

  void setpos(double,const T& ,double);
  void settolerance(double t) { eps = t;};
  void setscale(const T& s) { yscale = s;};
  void setmaxstep(double s) { hmax = s;};
  double getx() const {return curx;};
  const T& gety() const {return cury;};

};

// Constructor takes starting point and tolerance/error scale
template<class T>
inline RKintegrator<T>::RKintegrator(double x0, const T& y0, double t,
				     const T& s, double h0):
  curx (x0), cury(y0), eps (t), yscale(s), yerr(y0*0), curdydx(y0*0),
  hnext (h0), hdid (0), hmax (0)
{}

// Sets the current solution point
template<class T>
inline void RKintegrator<T>::setpos(double x0, const T& y0,double h0)
{
  curx=x0;
  cury=y0;
  hnext=h0;
}

// Advances the solution one step, keeping the truncation error under control
template<class T>
void RKintegrator<T>::RKadvance()
{
  // Constants
  const double safety=0.9;
  const double powgrow = -0.2;
  const double powshrink = -0.25;
  const double errcon = pow(5/safety,1/powgrow);

  // Initial guess step size from last time
  double h=hnext;
  double errmax;
  T ytemp(cury);

  // Set the initial derivative
  curdydx = dydxfunction(curx,cury);

  while(1) {
    // Take a step
    ytemp=RKstep(curx,cury,curdydx,h);

    // Find maximal truncation error in that step
    //errmax=0;
    errmax=yerr.renormalize(yscale).maxelement();
    /*
    for(int i=0;i<ndim;i++) {
      if(fabs(yerr[i]/yscale[i])>errmax)
	errmax=fabs(yerr[i]/yscale[i]);
    }
    */
    errmax /= eps;

    if(errmax<=1.0)
      break;  // Step succeeded

    // Step failed - reduce stepsize
    double htemp= safety*h*pow(errmax,powshrink);
    h= (h>=0?(htemp>0.1*h?htemp:0.1*h):(htemp<0.1*h?htemp:0.1*h));

    // If a maximum step is set, check that we don't exceed it
    if(hmax && (fabs(h)>hmax) )
      h=h/fabs(h)*hmax;

    // Check for underflow
    if(curx+h==curx) {
      std::cerr << "RKintegrator:: Hey, I'm in bad shape here - step size underflow!\n";
      break;
    }
  }

  // Increase stepsize
  if(errmax>errcon)
    hnext = safety*h*pow(errmax,powgrow);
  else
    hnext = 5*h;

  // If a maximum step is set, check that we don't exceed it
  if(hmax && (fabs(hnext)>hmax) )
    hnext=hnext/fabs(hnext)*hmax;

  cury = ytemp;
  hdid=h;
  curx+=h;
}

  
// Takes a RK step of length h, trunc error est in yerr
template<class T>
T RKintegrator<T>::RKstep(double x, const T& y,const T& dydx, double h)
{

  // First step
  T ytemp = y + dydx*(b21*h);

  // Second step
  T ak2 = dydxfunction(x + a2*h, ytemp);
  ytemp              = y + ( dydx*b31 + ak2*b32 )*h;

  // Third step
  T ak3 = dydxfunction(x + a3*h, ytemp);
  ytemp              = y + ( dydx*b41 + ak2*b42 + ak3*b43 )*h;

  // Fourth step
  T ak4 = dydxfunction(x + a4*h, ytemp);
  ytemp              = y + ( dydx*b51 + ak2*b52 + ak3*b53 +
 			      ak4*b54)*h;

  // Fifth step
  T ak5 = dydxfunction(x + a5*h, ytemp);
  ytemp              = y + ( dydx*b61 + ak2*b62 + ak3*b63 +
			      ak4*b64 + ak5*b65 )*h;
  // Sixth step
  T ak6 = dydxfunction(x + a6*h, ytemp);

  // Accumulate increments to get final result
  T yout = y + ( dydx*c1 + ak3*c3 + ak4*c4 + ak6*c6 )*h;

  // Estimate error as difference between 4th and 5th order methods
  yerr = ( dydx*dc1 + ak3*dc3 + ak4*dc4 + ak5*dc5 + ak6*dc6 )*h;

  return yout;
}

// Advances the solution to x=xend and returns the result. Note that it doesn't
// return on exactly xend, but rather after the step that takes it across
template<class T>
T RKintegrator<T>::solve(double xend)
{
  // Check sign of h
  if((xend-curx)*hnext<0) {
    // wrong sign - switch and hope for the best...  Actually, it's
    // probably due to the fact that the last step was larger than
    // anticipated and it has already stepped past
    std::cerr << "RKintegrator::solve - step guess has wrong sign, moron!\n";
    return cury;
  }

  while( (xend-curx)*hnext>0 )
    // Still not there, step again
    RKadvance();

  // Passed - stop
  return cury;
}

#endif
