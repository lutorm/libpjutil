// interpolator_template.h
//
// A templated version of the interpolator

// $Id$
// $Log$
// Revision 1.1  2000/03/05 06:42:53  patrik
// This is a new interpolator class where the interpolation object is a
// template. This means that it's easy to interpolate a vector etc. The
// necessary operations that need to be defined are addition, subtraction and
// multiplication with a scalar.
//
// Revision 1.3  1999/07/20 23:17:56  patrik
// Fixed return type warnings in cxx.
//
// Revision 1.2  1999/04/28 05:51:20  patrik
// The x-axis now uses a table<double> instead of an array, so looking up
// x values is now done somewhat more intelligently then just looking through
// the whole array.
//

#ifndef __interpolator_template__
#define __interpolator_template__

#include <fstream.h>
#include <map>
#include <vector>
#include <string>

using namespace std;

template<class T>
class linear_interpolator : public map<double,T> {
public:
  linear_interpolator() : map<double,T>() {};
  linear_interpolator(const linear_interpolator& i) : map<double,T>(i) {};
  linear_interpolator(const vector<double>&, const vector<T>&);
  linear_interpolator(const string&); // Reads interpolation data from an ascii file

  T interpolate(double) const;
};

// Copies data points from vectors x and y
template<class T> 
linear_interpolator<T>::linear_interpolator(const vector<double>& x, 
					 const vector<T>& y) : map<double,T>()
{
  if(x.size()!=y.size()) {
    cerr << "linear_interpolator:: There are not the same number of entries in the vectors!\n";
    return;
  }
      
  vector<double>::iterator a = x.begin();
  vector<T>::iterator b = y.begin();

  // Copy contents
  while((a!=x.end())&&(b!=y.end()))
    (*this)[*a++] = *b++;
}

// Reads interpolation data from file, on format n of points x0 y0 x1 y1 ...
template<class T>
linear_interpolator<T>::linear_interpolator(const string& fn) : map<double,T>()
{
  ifstream infile(fn.c_str());

  if(infile) {
    int n;
    infile >> n;

    for(int i=0;i<n;i++) {
      double x;
      infile >> x;
      infile >> (*this)[x];
    }
  }
}

// do a linear interpolation
template<class T> T linear_interpolator<T>::interpolate(double x) const
{
  const_iterator j = lower_bound(x);
  const_iterator i = j;
  --i;
  // Check for extrapolation and keep the nearest value
  bool ivalid = (i!=end());
  bool jvalid = (j!=end());

  if( ivalid && jvalid ) {
    const double dx      = x           - i->first;
    const double delta_x = j->first - i->first;
    const T delta_y = j->second - i->second;
    
    return delta_y*(dx/delta_x) + i->second;;
  }
  else if(ivalid) {
    // We are higher than the highest element
    return i->second;
  }
  else 
    // We are lower than the lowest
    return j->second;
}


#endif

