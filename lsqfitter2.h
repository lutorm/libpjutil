// $Id$

#ifndef __lsqfitter2__
#define __lsqfitter2__

#include "matrix.h"
#include "tableutils.h"

typedef table<double> arr;
typedef matrix<double> matr;

class lsqfitter2 {
private:
  matrix<double> alpha;
  matrix<double> covar;
  matrix<double> oneda;
  double ochisq;
  matr atry;
  matrix<double> beta;
  matr da;
  matr daguess;
  matrix<double> C;
  double  chisq;
  double  alambda;

  arr x;
  arr y;
  table<char> fixpar;
  arr sig;
  arr* par;
  int npar; // Number of parameters
  int nfit; // Number of parameters to fit
  int ndata; // Number of data points

  // Threshold values that *can* be user set
  // Don't change them unless you know what you're doing
  double DY_INITIALSTEP;
  double INITLAMBDA;
  double SUCCESSINCR;
  double FAILDECR;
  double ABSACC;
  double RELACC;
  int FAILRETRIES;

  // Needed accuracy in iterating for the dyda derivatives.
  // I think we don't need to be REALLY accurate here since we'll probably
  // converge pretty well anyway.
  double DY_ACCURACY;

  int verbose;

  void covsrt();
  void mrqcof(matr*, matrix<double>*, matr*);
  int mrqmin();

public: 
  // Constructor - argument is starting step for numerical derivative
  // and accuracy of numerical derivative
  lsqfitter2( double dystep=0.1,double dyacc=1e-5 );

  // Function to fit - abstract virtual - must be defined in a derived class
  virtual double function(double, const matr&)=0;
  
  // Parameter derivative function - the supplied is the numerical derivative
  // Can be redefined in the derived class to give an analytical (much faster)
  virtual matr dydafunction(double, const matr&);
  
  // Do not print fitting progress
  void setSilent();
  void setNormal();
  void setVerbose();
  
  // Set fit routine parameters
  void setAccuracy(double,double);
  void setFailretries(int);
  void setInitialStep(double);
  void setSuccessincr(double);
  void setFaildecr(double);
  
  // Perform fit, returning chisquare
  double fit(const arr& xx, const arr& yy,
	     const arr& ss, arr& pp);

  // Perform fit with fixed parameters indicated by fixa
  double fit(const arr& xx, const arr& yy,
	     const arr& ss, arr& pp,
	     const table<char>& ff);

  // Legacy calls - don't use in new code!
  double fit(const matr* xx, const matr* yy,
	     const matr* ss, matr* pp);

  // Perform fit with fixed parameters indicated by fixa
  double fit(const matr* xx, const matr* yy,
	     const matr* ss, matr* pp,
	     const matrix<char>* ff);

  matrix<double> getParameterError();
  matrix<double> getCovar();
  double getndata();
  double getnpar();
};

inline double lsqfitter2::getndata()
{
  return ndata;
}

inline double lsqfitter2::getnpar()
{
  return nfit;
}

inline void lsqfitter2::setSilent()
{
  verbose=0;
}

inline void lsqfitter2::setNormal()
{
  verbose=1;
}

inline void lsqfitter2::setVerbose()
{
  verbose=2;
}

inline void lsqfitter2::setAccuracy(double abs,double rel)
{
  ABSACC=abs;
  RELACC=rel;
}

inline void lsqfitter2::setFailretries(int n)
{
  FAILRETRIES=n;
}

inline void lsqfitter2::setSuccessincr(double si)
{
  SUCCESSINCR=si;
}

inline void lsqfitter2::setFaildecr(double fd)
{
  FAILDECR=fd;
}

inline void lsqfitter2::setInitialStep(double is)
{
  INITLAMBDA=is;
}

// Return the covariance matrix
inline  matrix<double> lsqfitter2::getCovar()
{
  return C;
}

// This is the callable routine, performs the fit
inline double lsqfitter2::fit(const arr& xx, const arr& yy,
	   const arr& ss, arr& pp)
{
  // Just creates a "fit all" fixpar matrix and calls the next fit routine
  table<char> ff(pp.n());
  ff=1;

  return fit(xx,yy,ss,pp,ff);
}

inline double lsqfitter2::fit(const matr* xx, const matr* yy,
	   const matr* ss, matr* pp)
{
  arr x,y,s,p;
  x= *xx;
  y= *yy;
  s= *ss;
  p= *pp;
  double chisq=fit(x,y,s,p);
  *pp=p.makematrix();
  return chisq;
}

// Perform fit with fixed parameters indicated by fixa
inline double lsqfitter2::fit(const matr* xx, const matr* yy,
			      const matr* ss, matr* pp,
			      const matrix<char>* ff)
{
  arr x,y,s,p;
  table<char> f;
  x= *xx;
  y= *yy;
  s= *ss;
  p= *pp;
  f= *ff;

  double chisq=fit(x,y,s,p,f);
  *pp=p.makematrix();
  return chisq;
}


#endif

