// $Id$

// An implementation of copy_if pulled straight out of "effective
// stl"...

template <typename input_iterator, typename output_iterator,
typename predicate>
output_iterator copy_if (input_iterator begin, input_iterator end,
			 output_iterator destination_begin,  predicate
			 p)
{
  while (begin != end) {
    if (p (*begin))
      *destination_begin++ = *begin;
    ++begin;
  }
  return destination_begin;
}

