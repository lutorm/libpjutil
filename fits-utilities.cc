#include "fits-utilities.h"
#include "CCfits/CCfits"
#include <cassert>

using namespace CCfits;
using namespace std;

// Tries to open the specified HDU.  If that fails , throws (or
// rather, fails to catch the second no such HDU exception)
ExtHDU& open_HDU (FITS& file, const std::string& name)
{
  ExtHDU* hdu;
  try {
    hdu = &file.extension(name);
  }
  catch (FITS::NoSuchHDU&) {
    file.read(name);
    hdu = &file.extension(name);
  }

  return *hdu;
}  

ExtHDU& open_HDU (FITS& file, int hdunum)
{
  ExtHDU* hdu;
  try {
    hdu = &file.extension(hdunum);
  }
  catch (FITS::NoSuchHDU&) {
    file.read(hdunum);
    hdu = &file.extension(hdunum);
  }

  return *hdu;
}  

const ExtHDU& open_HDU (const FITS& file,
				const std::string& name)
{
  const ExtHDU* hdu;
  try {
    hdu = &file.extension(name);
  }
  catch (FITS::NoSuchHDU&) {
    const_cast<FITS&> (file).read(name);
    hdu = &file.extension(name);
  }

  return *hdu;
}  


std::string keyword_unit(HDU& hdu, const std::string& keyword)
{
  hdu.readAllKeys();
  const string cmt = hdu.keyWord(keyword).comment();

  const size_t unitstart = cmt.find("[", 0);

  if (unitstart==string::npos)
    // no [
    return "";

  // the unit start should be in position 0 or 1 of the comment
  assert(unitstart<=1);

  const size_t unitstop = cmt.find("]", unitstart+1);
  
  if (unitstop==string::npos)
    // no ], so it can't be a unit
    return "";

  return cmt.substr(unitstart+1,unitstop-unitstart-1);
}
