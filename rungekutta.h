// rungekutta.h
// Integrates 1st order ODE's using a 5th order Runge-Kutta method with 
// adaptive step size, from Numerical Recipes.
//
// The problem is posed as
//
//   dy/dx = f(x,y,a)
// 
// where y is a vector of n dependent variables, and a are parameters

// $Id$
// $Log$
// Revision 1.2  1999/04/17 08:18:18  patrik
// I have no idea what was changed here... :(
//
// Revision 1.1  1999/03/10 10:17:30  patrik
// Implements an embedded 5th order Cash-Karp Runge-Kutta ODE solver from
// Numerical Recipes. It does adaptive step size to a specified accuracy and
// can be run step by step (RKadvance) or to a specified x (solve).
// Not exhaustively tested but at least reproduces a sine wave from 0-100...
//

#ifndef __rungekutta__
#define __rungekutta__


#include "matrix.h"

class RKintegrator {
private:
 public:
  // Problem parameters
  int ndim;                     // Number of dependent variables
  matrix<double> yscale;        // Scaling of dependent variables
  double eps;                   // Tolerance
  double hmax;                  // Maximum step
  double hdid,hnext;

  // Current solution point
  matrix<double> yerr;
  double curx;
  matrix<double> cury;
  matrix<double> curdydx;

  // Cash-Karp constants
  static matrix<double> a,b,c,dc;

  // Takes a RK step of length h, trunc error est in yerr
  matrix<double> RKstep(double x, const matrix<double>& y, 
			const matrix<double>& dydx, double h);

protected:
  // Routine supplying the right-hand side derivatives that must be redefined
  virtual matrix<double> dydxfunction(double,matrix<double>)=0;

public:
  RKintegrator(double,matrix<double>,double,matrix<double>,double);
  //~RKintegrator();

  matrix<double> solve(double); // Advances the solution and returns result
  // Advances the solution one step, keeping track of the truncation error
  void RKadvance();

  void setpos(double,matrix<double>,double);
  void settolerance(double);
  void setscale(matrix<double>);
  void setmaxstep(double);
  double getx() const;
  matrix<double> gety() const;

};

// Constructor takes starting point and tolerance/error scale
inline RKintegrator::RKintegrator(double x0, matrix<double> y0, double t,
				  matrix<double> s, double h0)
{
  ndim=y0.getrows();
  curx=x0;
  cury=y0;
  eps=t;
  yscale=s;
  hnext = h0;
  hdid=0;
  hmax=0;

  yerr=matrix<double>(ndim,1,0.);
}

// Sets the current solution point
inline void RKintegrator::setpos(double x0, matrix<double>y0,double h0)
{
  curx=x0;
  cury=y0;
  hnext=h0;
}

inline void RKintegrator::settolerance(double t)
{
  eps = t;
}

inline void RKintegrator::setscale(matrix<double> s)
{
  yscale = s;
}

inline void RKintegrator::setmaxstep(double s)
{
  hmax = s;
}

inline double RKintegrator::getx() const
{
  return curx;
}

inline matrix<double> RKintegrator::gety() const
{
  return cury;
}

#endif
