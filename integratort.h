// $Id$

// Third implementation of an integrator, this one using the possible
// inlining of the integrand by supplying it as a functor to the
// integrate routine 

#ifndef __integratort__
#define __integratort__

#include <iostream>
#include <cmath>

class integrator {
private:
  double accuracy;                     // Required accuracy;
  double startstep;                    // First-guess step size

  double x,y,dydx,yscale;              // Integration variables
  template< typename T_function>
  double Runge5(double h,double& yerr, T_function);
  template< typename T_function>
  double Makestep(double htry, T_function);
  double sign(double val, double sgn) const { 
    return fabs(val)*(sgn > 0?  1: (sgn < 0?  -1: 0));
  }

public:
  integrator(): accuracy (1), startstep (1) {};
  integrator( double a, double s ):
    accuracy (a), startstep (s) {};

  void setaccuracy(double a,double s){
    accuracy = a; startstep = s;}

  template< typename T_function>
  double integrate(double a, double b, T_function); // Make integration

};


template<typename T_function>
double integrator::Runge5(double h, double& yerr, T_function integrand )
{
  const double a3=0.3, a4=0.6, a5=1.0, a6=0.875;
  const double c1=37.0/378, c3=250.0/621, c4=125.0/594, c6=512.0/1771;
  const double dc1= c1-2825.0/27648, dc3= c3-18575.0/48384;
  const double dc4= c4-13525.0/55296, dc5=-277.0/14336;
  const double dc6= c6-0.25;

  const double ak3 = integrand(x+a3*h);
  const double ak4 = integrand(x+a4*h);
  const double ak5 = integrand(x+a5*h);
  const double ak6 = integrand(x+a6*h);

  // Estimate error as difference between fourth and fifth order
  yerr = h*(dc1*dydx + dc3*ak3 + dc4*ak4 + dc5*ak5 + dc6*ak6);

  // Estimate fifth-order value
  return (y + h*(c1*dydx + c3*ak3 + c4*ak4 + c6*ak6) );
}

// 5th order Runge-Kutta step
template<typename T_function>
double integrator::Makestep(double htry, T_function integrand)
{
  const double safety=0.9, pgrow=-0.2, pshrink=-0.25, errcon=1.89e-4;

  double h = htry; // Set stepsize to initial accuracy
  double errmax = 10;
  bool underflow = false;
  double ytemp;

  while( (errmax>1) && (!underflow) ) {
    double yerr;
    ytemp = Runge5(h, yerr, integrand); // Take a step

    errmax = fabs(yerr/yscale)/accuracy; // Scale relative to required accuracy

    if(errmax>1) { // Truncation error too large, reduce h
      const double htemp = safety*h*pow(errmax,pshrink);
      h = sign( (fabs(htemp)>0.1*fabs(h)) ? fabs(htemp) : 0.1*fabs(h) , h);
      double xnew = x+h;

      if( (xnew==x) && (!underflow) ) {
	underflow = true;
	std::cout << "integrator::Makestep: Warning, stepsize underflow\n";
      }
    }
  }

  x+=h;
  y=ytemp;

  // Step succeeded. Estimate and return size of next step

  if(errmax>errcon)
    return safety*h*pow(errmax,pgrow);
  else
    return 5.0*h; // Not more than a factor of 5 increase
}


template<typename T_function>
double integrator::integrate(double a, double b, 
			      T_function integrand)
{
  const int maxsteps = 100000000;

  // Initialise members
  x = a;
  y = 0;
  double dx = startstep;
  int Nstep = 0;

  while( ( ((x-b)*(b-a))<0 ) && (Nstep<maxsteps)) {
    ++Nstep;
    dydx = integrand(x);

    // yscale is used to monitor accuracy. This general purpose choice
    // can be modified if need be.

    yscale = ( (fabs(y)+fabs(dx*dydx))>1e-16 ? fabs(y)+fabs(dx*dydx) : 1e-16 );

    if( ((x+dx-b)*(x+dx-a)) > 0) // Stepsize overshoots, decrease it
      dx = b-x;

    dx = Makestep(dx, integrand);
  }

  if(Nstep==maxsteps)
    std::cout << "integrator::integrate: Warning, failed to converge\n";

  return y;
}




#endif

