/*
    Copyright 2003-2012 Patrik Jonsson, code@familjenjonsson.org

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

/** \file 

    Routines for determining the position of a point in space on a
    Hilbert curve. Based on the algorithm described in
    "Vertex-labeling algorithms for the Hilbert space filling curve"
    by John J. Bartholdi, III and Paul Goldsman, January 11, 2000.
*/

#include "hilbert.h"

/** Outputs the hilbert code in the 0.nnn notation used by BH. */
std::ostream& hilbert::operator<<(std::ostream& os, const hilbert_code_2D& c)
{
  for(int i=0;i<=c.n_;++i) {
    os << (c.c_ >> (2*(c.n_-i)) & 0x03);
    if(i==0) os << '.';
  }
  return os;
}



/** Returns the Hilbert code of the specified point in an n-th order
    Hilbert curve. The point p should be in the unit cube. */
hilbert::hilbert_code_2D 
hilbert::encode_point(const blitz::TinyVector<double, 2>& p, 
		      int n)
{

  hilbert_cell_2D c(hilbert_cell_2D::T_point(0,0), 
		    hilbert_cell_2D::T_point(1,1));
  for(int i=0;i<n;++i)
    c.descend(p);
  return c.code_;
}


/** Updates a Hilbert cell to the subcell containing the point. */
void hilbert::hilbert_cell_2D::descend (const T_point& p)
{
  // Determine which point the cell is closest to.
  const T_point dp = (p-a_);
  const T_point frac(dp[dims_[0]]/axes_[0], 
		     dp[dims_[1]]/axes_[1]);

  int nearest = frac[0]>0.5 ? 1:0;
  if(frac[1]>0.5)
    nearest = 3-nearest;

  assert(nearest<4);
  // nearest now indicates 0-3 for abcd as the nearest point.
  
  // update code
  code_.add_right(nearest);

  // determine new vertices
  
  // all axes lengths are halved. we only need to determine new dims
  // and signs
  axes_*=0.5;

  switch(nearest) {
  case 0:
    // a'=a, b'=d/2, d'=b/2
    std::swap(dims_[0], dims_[1]);
    std::swap(axes_[0], axes_[1]);
    break;

  case 1:
    // a'=a+b/2, b'=b/2, d'=d/2
    a_[dims_[0]] += axes_[0];
    break;

  case 2:
    // a'=a+c/2, b'=b/2, d'=d/2
    a_[dims_[0]] += axes_[0];
    a_[dims_[1]] += axes_[1];
    break;

  case 3:
    // a'=a+b/2+d, b'=-d/2, d'=-b/2
    a_[dims_[0]] += axes_[0];
    a_[dims_[1]] += 2*axes_[1];
    std::swap(dims_[0], dims_[1]);
    std::swap(axes_[0], axes_[1]);
    axes_ *= -1;
    break;

  default:
    assert(0);
  }

  // The cell now reflects the subcell.
}


/** Outputs a cell code in the 0.ooo notation used by BH. We also
    recognize the special code 1.0... which is the "one past end"
    code.  */
std::ostream& hilbert::operator<<(std::ostream& os, const cell_code& c)
{
  for(int i=0;i<=c.n_;++i) {
    os << (c.c_ >> (3*(c.n_-i)) & 0x07);
    if(i==0) os << '.';
  }
  return os;
}


/** Outputs a hilbert state. */
std::ostream&
hilbert::operator<<(std::ostream& os, const hilbert_state& s)
{
  os << "Dimensions: " 
     << int(s.dim(0)) << "," << int(s.dim(1)) << "," << int(s.dim(2))
     << " axes: " << int(s.axis(0)) << "," << int(s.axis(1)) << "," << int(s.axis(2));
  return os;
}


/** Outputs a C state, which is stateless so just a placeholder. */
std::ostream&
hilbert::operator<<(std::ostream& os, c_state s)
{
  os << "(C-state)";
  return os;
}


/** Outputs a qpoint in the 0.nnn notation used by BH. We also
    recognize the special value 1, which is the "one past end"
    code.  */
std::ostream&
hilbert::operator<<(std::ostream& os, const qpoint& p)
{
  os << "(";
  for(int d=0; d<3; ++d) {
    //os << ((d==0) ? 'x' : ((d==1) ? 'y' : 'z')) << ": ";
    for(int i=0;i<=p.level();++i) {
      os << (p[d] >> (p.level()-i) & 0x01);
      if(i==0) os << '.';
    }
    if(d<2) os << ", ";
  }
  os << ")";
  return os;
}

/** Returns the Hilbert code of the specified point in an n-th order
    Hilbert curve. The point p should be in the unit cube. */
hilbert::cell_code 
hilbert::encode_point(const blitz::TinyVector<double, 3>& p, 
		      int n)
{
  const qpoint qp(p,n);
  octree_cell<hilbert_state> c;
  for(int i=0;i<n;++i)
    c.descend(qp);
  return c.code();
}

/** Returns the Hilbert code of the specified qpoint. */
hilbert::cell_code 
hilbert::encode_point(const qpoint& qp)
{
  const int n=qp.level();
  octree_cell<hilbert_state> c;
  for(int i=0;i<n;++i)
    c.descend(qp);
  return c.code();
}


