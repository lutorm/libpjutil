// $Id$

#ifndef __mat3__
#define __mat3__

#include <cmath>
#include "biniostream.h"
#include "vec3.h"
#include "matrix.h"

template<class C> class mat3;
template<class T> mat3<T> invert(mat3<T>);
template<class T> std::ostream& operator<< (std::ostream&, const mat3<T>&);
template<class T> std::istream& operator>> (std::istream&, mat3<T>&);
template<class T> binfstream& operator<< (binfstream&, const mat3<T>&);
template<class T> binfstream& operator>> (binfstream&, mat3<T>&);


template<class C> class mat3 {
  //template<class R> friend class vec3<R>;
private:
  vec3<C> c[3]; // just keeps 3 components

  // exception class for out-of-bonds
  class index_error {
  public:
    int i;
    index_error(int a) {i=a;};
  }; 
  
  void rangecheck(int) const; // Checks if given index is valid

  // Inversion support routines
  void ludcmp(int[], C*);
  void lubksb(int[], vec3<C>&) const;
   
 public:
  //static mat3 I;

  mat3();
  mat3(C,C,C,C,C,C,C,C,C);
  mat3(C);
  mat3(const vec3<C>&, const vec3<C>&, const vec3<C>&);
  template<class R> operator mat3<R>();
  //mat3(const mat3&);
  ~mat3() {};

  //conversion
  operator matrix<C>() const;

  // mat3 unary operators
  const mat3 operator-() const;
  vec3<C>& operator[](int);
  const vec3<C>& operator[](int) const;
  mat3& identity();

  // mat3 binary operators

  const mat3 operator+(const mat3&) const;
  const mat3 operator-(const mat3&) const;
  const mat3 operator*(const mat3&) const;

  mat3& operator=(const mat3&);
  mat3& operator+=(const mat3&);
  mat3& operator-=(const mat3&);
  mat3& operator*=(const mat3&);

  bool operator==(const mat3&) const;
  bool operator!=(const mat3&) const;

  // mat3-vec3 operators

  vec3<C> operator*(const vec3<C>&) const;
  //friend vec3<C> operator*(const vec3<C>&, const mat3&);

  // mat3-scalar operators (element-wise operations)

  const mat3 operator+(C) const;
  const mat3 operator-(C) const;
  const mat3 operator*(C) const;
  const mat3 operator/(C) const;

  //friend const mat3 operator+(C, const mat3&);
  //friend const mat3 operator-(C, const mat3&);
  //friend const mat3 operator*(C, const mat3&);

  mat3& operator=(C);
  mat3& operator+=(C);
  mat3& operator-=(C);
  mat3& operator*=(C);
  mat3& operator/=(C);

  bool operator==(C) const;
  bool operator!=(C) const;
  //friend bool operator==(C, const mat3&);
  //friend bool operator!=(C, const mat3&);

  // Linear algebra operations
  mat3 T() const; // transpose
  mat3 xform(const mat3&) const; // product A*M*AT

  friend mat3 invert<> (mat3);

  // I/O operators
  friend std::ostream& operator<< <>(std::ostream&, const mat3&);
  friend std::istream& operator>> <>(std::istream&, mat3&);

  friend binfstream& operator<< <>(binfstream&, const mat3&);
  friend binfstream& operator>> <>(binfstream&, mat3&);
  
};

// Default mat3 constructor
// Creates null mattor
template<class C> inline mat3<C>::mat3()
{
}

// Constructor for mat3 of constant value
template<class C> inline mat3<C>::mat3(C v)
{
  c[0]=vec3<C>(v);
  c[1]=vec3<C>(v);
  c[2]=vec3<C>(v);
}

// Constructor for mat3 with specified components
template<class C> inline mat3<C>::mat3(C v1, C v2, C v3, 
				       C v4, C v5, C v6,
				       C v7, C v8, C v9)
{
  c[0]=vec3<C>(v1,v2,v3);
  c[1]=vec3<C>(v4,v5,v6);
  c[2]=vec3<C>(v7,v8,v9);
}

// Constructor for mat3 with specified components
template<class C> inline mat3<C>::mat3(const vec3<C>& v1,const vec3<C>& v2,
				       const vec3<C>& v3)
{
  c[0]=vec3<C>(v1);
  c[1]=vec3<C>(v2);
  c[2]=vec3<C>(v3);
}

/*
// Implicit conversion operator between different mat3<>'s
template<class C> template<class R>
inline mat3<C>::operator mat3<R>()
{
  mat3<R> v(static_cast<R>(c[0]),
	    static_cast<R>(c[1]),
	    static_cast<R>(c[2]));
  return v;
}
*/

// Checks whether specified component is valid
// it throws an exception if it isn't
// if the define is defined it is skipped completely, to avoid overhead
template<class C> inline void mat3<C>::rangecheck(int i) const
{
#ifndef MAT3_NODEBUG
  if( (i<0) || (i>2) )
    throw index_error(i);
#endif
}


// ***
// *** mat3 unary operators ***
// ***

// Unary negation operator 
template<class C> inline const mat3<C> mat3<C>::operator-() const
{
  return mat3(-c[0],-c[1],-c[2]);
}

// const version of index operator (returns a (row)vec3 so we can use [r][k])
template<class C> inline const vec3<C>& mat3<C>::operator[](int i) const
{
  rangecheck(i);
  
  return c[i];
}

// non-const version of index operator can modify its contents
template<class C> inline vec3<C>& mat3<C>::operator[](int i)
{
  rangecheck(i);
    
  return c[i];
}

// resets to identity matrix
template<class C> inline mat3<C>& mat3<C>::identity()
{
  (*this)=0.;
  c[0][0]=1.;
  c[1][1]=1.;
  c[2][2]=1.;

  return *this;
}

// ***
// *** mat3-mat3 operators ***
// ***

// mat3 assignment operator
template<class C> inline mat3<C>& mat3<C>::operator=(const mat3& m)
{
  c[0]=m.c[0];
  c[1]=m.c[1];
  c[2]=m.c[2];

  return *this;
}

// Adds mat3 m to this
template<class C> inline mat3<C>& mat3<C>::operator+=(const mat3& m)
{
  c[0]+=m.c[0];
  c[1]+=m.c[1];
  c[2]+=m.c[2];

  return *this;
}

// Subtracts mat3 m from this
template<class C> inline mat3<C>& mat3<C>::operator-=(const mat3& m)
{
  c[0]-=m.c[0];
  c[1]-=m.c[1];
  c[2]-=m.c[2];

  return *this;
}

// Matrix multiplication
template<class C> inline mat3<C>& mat3<C>::operator*=(const mat3& m)
{
  // We are doing it to ourselves, so we need to make a temporary copy
  // otherwise we got trouble
  mat3 a(*this);

  // Check for self-multiplication and 
  const mat3& b( (&m!=this)?m:a );

  for(int r=0;r<3;++r)
    for(int col=0;col<3;++col)
      c[r][col]=a[r][0]*b[0][col]+a[r][1]*b[1][col]+a[r][2]*b[2][col];
    
  return *this;
}

// Compares this with mat3 m
// mat3s are equal if all elements equal
template<class C> inline bool mat3<C>::operator==(const mat3& m) const
{
  if((c[0]==m.c[0])&&(c[1]==m.c[1])&&(c[2]==m.c[2]))
    return true;
  else
    return false;
}

template<class C> inline bool mat3<C>::operator!=(const mat3& m) const
{
  if((c[0]==m.c[0])&&(c[1]==m.c[1])&&(c[2]==m.c[2]))
    return false;
  else
    return true;
}

// ***
// *** mat3-vec3 operators ***
// ***

// matrix*column vector v => column vector
template<class C> vec3<C> mat3<C>::operator*(const vec3<C>& v) const
{
  return vec3<C>(dot(c[0],v),dot(c[1],v),dot(c[2],v)); 
}

// row vector v*matrix => row vector
template<class C> vec3<C> operator*(const vec3<C>& v, const mat3<C>& m)
{
  return vec3<C>(v[0]*m[0][0]+v[1]*m[1][0]+v[2]*m[2][0],
		 v[0]*m[0][1]+v[1]*m[1][1]+v[2]*m[2][1],
		 v[0]*m[0][2]+v[1]*m[1][2]+v[2]*m[2][2]);
}

// ***
// *** mat3-scalar operators (element-wise operations) ***
// ***

// Assigns constant value to mat3
template<class C> inline mat3<C>& mat3<C>::operator=(C d)
{
  c[0]=d;
  c[1]=d;
  c[2]=d;

  return *this;
}

// Adds d to all elements of mat3
template<class C> inline mat3<C>& mat3<C>::operator+=(C d)
{
  c[0]+=d;
  c[1]+=d;
  c[2]+=d;

  return *this;
}

// Subtracts d from all elements of mat3
template<class C> inline mat3<C>& mat3<C>::operator-=(C d)
{
  c[0]-=d;
  c[1]-=d;
  c[2]-=d;
  
  return *this;
}

// Multiplies all elements of mat3 by d
template<class C> inline mat3<C>& mat3<C>::operator*=(C d)
{
  c[0]*=d;
  c[1]*=d;
  c[2]*=d;

  return *this;
}

// Divides all elements of mat3 by d
template<class C> inline mat3<C>& mat3<C>::operator/=(C d)
{
  c[0]/=d;
  c[1]/=d;
  c[2]/=d;

  return *this;
}

// Compares this with scalar d
// mat3s are equal if all elements equal to d
template<class C> inline bool mat3<C>::operator==(const C d) const
{
  if((c[0]==d)&&(c[1]==d)&&(c[2]==d))
    return true;
  else
    return false;
}

template<class C> inline bool mat3<C>::operator!=(const C d) const
{
  if((c[0]==d)&&(c[1]==d)&&(c[2]==d))
    return false;
  else
    return true;
}

// *** Misc matrix operators ***

// Cranspose
template<class C> inline mat3<C> mat3<C>::T() const
{
  return mat3(c[0][0], c[1][0], c[2][0],
	      c[0][1], c[1][1], c[2][1],
	      c[0][2], c[1][2], c[2][2]);
}

// ***
// *** I/O operators ***
// ***

// User-defined input operator for mat3 type
// Cakes rows*cols plain numbers
template<class C> inline std::istream& operator>>(std::istream& s, mat3<C>& m)
{
  s >> m.c[0] >> m.c[1] >> m.c[2];

  return s;
}

// User-defined output operator for mat3 type
template<class C> inline std::ostream& operator<<(std::ostream& s, const mat3<C>& m)
{
  s << "( " << m.c[0] << ", " << m.c[1] << ", " << m.c[2] << " )";

  return s;
}

// User-defined binary output operator for mat3 type
template<class C> inline binfstream& operator<<(binfstream& s, const mat3<C>& m)
{
  s << m.c[0] << m.c[1] << m.c[2];

  return s;
}

// User-defined binary input operator for mat3 type
template<class C> inline binfstream& operator>>(binfstream& s, mat3<C>& m)
{
  s >> m.c[0] >> m.c[1] >> m.c[2];

  return s;
}


//
// Che binary operators are just implemented using the copy constructor
// and the unary x= operators, so use the latter if performance is an issue
//

template<class C> 
inline const mat3<C> mat3<C>::operator+(const mat3<C>& m) const
{
  return mat3(*this) += m;
}

template<class C>
inline const mat3<C> mat3<C>::operator-(const mat3<C>& m) const
{
  return mat3(*this) -= m;
}

template<class C> 
inline const mat3<C> mat3<C>::operator*(const mat3<C>& m) const
{
  return mat3(*this) *= m;
}

template<class C> 
inline const mat3<C> mat3<C>::operator+(C d) const
{
  return mat3(*this) += d;
}

template<class C> 
inline const mat3<C> mat3<C>::operator-(C d) const
{
  return mat3(*this) -= d;
}

template<class C> 
inline const mat3<C> mat3<C>::operator*(C d) const
{
  return mat3(*this) *= d;
}

template<class C> 
inline const mat3<C> mat3<C>::operator/(C d) const
{
  return mat3(*this) /= d;
}

template<class C> 
inline const mat3<C> operator+(C d, const mat3<C>& m)
{
  return mat3<C>(m) += d;
}

template<class C> 
inline const mat3<C> operator-(C d, const mat3<C>& m)
{
  mat3<C> a(-m);
  return mat3<C>(a) += d;
}

template<class C> 
inline const mat3<C> operator*(C d, const mat3<C>& m)
{
  return mat3<C>(m) *= d;
}

template<class C> 
inline bool operator==(C d, const mat3<C>& m)
{
  return (m==d);
}

template<class C> 
inline bool operator!=(C d, const mat3<C>& m)
{
  return (m!=d);
}

template<class C> 
inline mat3<C>::operator matrix<C>() const
{
  matrix<C> m(3,3);

  for(int rr=0;rr<3;++rr)
    for(int cc=0;cc<3;++cc)
      m[rr][cc] = c[rr][cc];

  return m;
}

template<class C>
mat3<C> invert(mat3<C> A)
{
  mat3<C> B;
  int indx[3];

  // Do the LU decomposition
  C d;
  A.ludcmp(indx,&d);

  // Back-substitute to get inverse
  for(int j=0;j<3;j++) {
    vec3<C> col(0);
    col[j]=1.0;
    A.lubksb(indx,col);
    for(int i=0,I=0;i<3;i++,I+=3)
      B[i][j]=col[i];
  }

  return B;
}

#define TINY 1.0e-20;

// What used to be a is now this
template<class C>
void mat3<C>::ludcmp(int indx[3], C *d)
{
  int imax;
  C big,dum,sum,temp;

  // Get the pointers to speed it up
  const int n=3;
  vec3<C> vv;
  //C* va = val();
  //C* vvv = vv.val();

  *d=1.0;

  // Loop over rows to get implicit scaling info
  for (int i=0,I=0;i<n;i++,I+=n) {
    big=0.0;
    for (int j=0;j<n;j++)
      if ((temp=fabs(c[i][j])) > big) 
	big=temp;
    if (big == 0.0)
      std::cerr << "Singular matrix in routine ludcmp\n";

    vv[i]=1.0/big; // Save the scaling
  }

  for (int j=0,J=0;j<n;j++,J+=n) { // Loop over columns in Crout's method
    //cout << "\nstart loop for j=" << j <<'\n';

    for (int i=0,I=0;i<j;i++,I+=n) { // Equation 2.3.12 for i<j
      sum=c[i][j];
      for (int k=0,K=0;k<i;k++,K+=n)
	sum -= c[i][k]*c[k][j];
      c[i][j]=sum;
    }

    //cout << j << 'a' << a;

    big=0.0;    // Init search for largest pivot element
    for (int i=j,I=J;i<n;i++,I+=n) {
      // i=j part of 2.3.12 and i=j+1...n of 2.3.13
      sum=c[i][j];
      for (int k=0,K=0;k<j;k++,K+=n)
	sum -= c[i][k]*c[k][j];
      c[i][j]=sum;
      dum = vv[i]*fabs(sum);
      if ( dum >= big) {
	//cout << "new imax " << i << '\t' << '\n';
	// Is the figure of merit of this pivot better than the best so far?
	big=dum;
	imax=i;
      }
    }
    //cout << j << 'b' << a;
    if (j != imax) { // Do we need to interchange rows?
      //cout << "interchanging " << imax << "<->" << j << "\n";
      int IMAX=imax*n;
      for (int k=0;k<n;k++) { // yes
	dum=c[imax][k];
	c[imax][k]=c[j][k];
	c[j][k]=dum;
      }
      *d = -(*d); // and update parity
      //cout << "Changing scale factor\n";
      vv[imax]=vv[j]; // and interchange scale factor
      //cout << j << 'c' << a;
    }
    indx[j]=imax;
    if (c[j][j] == 0.0) c[j][j]=TINY;
    if (j != (n-1)) { // Finally, divide by the pivot element
      dum=1.0/(c[j][j]);
      for (int i=j+1,I=J+n;i<n;i++,I+=n) 
	c[i][j] *= dum;
    }
    //cout << j << 'd' << a;

  } // end column loop for j

}

// what used to be a is now this
template<class C>
void mat3<C>::lubksb(int indx[3], vec3<C>& b) const
{
  int ii=-1,ip;
  C sum;

  const int n=3;
  //C* va=val();
  //C*vb=b.val();

  for (int i=0,I=0;i<n;i++,I+=n) {
    ip=indx[i];
    sum=b[ip];
    b[ip]=b[i];
    if (ii>=0)
      for (int j=ii;j<=i-1;j++) 
	sum -= c[i][j]*b[j];
    else if (sum) 
      ii=i;
    b[i]=sum;
  }
  for (int i=n-1,I=n*(n-1);i>=0;i--,I-=n) {
    sum=b[i];
    for (int j=i+1;j<n;j++) 
      sum -= c[i][j]*b[j];
    b[i]=sum/c[i][i];
  }
}



#endif
