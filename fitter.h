// $Id$

#ifndef __fitter__
#define __fitter__

#include "blitz/array.h"

template <typename T> class fitter_impl;

// Base class for fitting function functors. Functors should derive
// from this and must define the function through
// "vector operator(const T_indep_vector& x, const vector& par)" 
// as well as the parameter derivative as
// "matrix dyda(const T_indep_vector& x, const vector& par, const vector& fixed)".
template <typename T>
class fitting_function {
public:
  typedef T T_indep;
  typedef blitz::Array<double, 1> vector;
  typedef blitz::Array<T_indep, 1> T_indep_vector;
  typedef blitz::Array<double, 2> matrix;
  void setdaguess(const vector& v) {};
private:
  //friend class fitter_impl<fitting_function<T> >;
};

// Base class for fitting function functors without analytical derivative.
// Functors should derive from this and must define 
// "vector operator(const T_indep_vector& x, const vector& par)" 

// The predefined function dyda will evaluate dy/da numerically, and
// accesses f nonvirtually through the Barton-Nackman trick. 
template <typename T, typename T_leaf>
class numder_fitting_function {
public:
  typedef T T_indep;
  typedef blitz::Array<double, 1> vector;
  typedef blitz::Array<T_indep, 1> T_indep_vector;
  typedef blitz::Array<double, 2> matrix;

  // Constructor takes dyda accuracy parameters
  numder_fitting_function(double dystep=0.1, double dyacc=1e-5) :
    DY_INITIALSTEP(dystep), DY_ACCURACY(dyacc) {};

  matrix dyda(const T_indep_vector&, const vector&, 
	      const blitz::Array<bool, 1>&);
private:
  // Needed accuracy in iterating for the dyda derivatives.
  double DY_INITIALSTEP;
  double DY_ACCURACY;

  friend class fitter_impl<T_leaf>;
  vector daguess;
  void setdaguess(const vector& v) {daguess.resize(v.size());daguess=v;};
};

class fitter {
public:
  typedef blitz::Array<double, 1> vector;
  typedef blitz::Array<double, 2> matrix;

protected:
  int npar; // Number of parameters
  int nfit; // Number of parameters to fit
  int ndata; // Number of data points

  // "current" parameter set
  matrix alpha;
  vector beta, par;
  double xi2;

  // "trial" parameter set
  matrix covar;
  vector da, atry;
  double oxi2;

  matrix C;
  double alambda;

  //vector daguess; // used by the numerical derivative evaluator

  // points
  vector y, sig2i;
  blitz::Array< bool, 1> fixed;

  // Threshold values that *can* be user set
  // Don't change them unless you know what you're doing
  double INITLAMBDA;
  double SUCCESSINCR;
  double FAILDECR;
  double ABSACC;
  double RELACC;
  int FAILRETRIES;


  int verbose;

  // member functions
  fitter();

  bool mrqmin ();
  matrix covsrt(const matrix& cov) const;

  // backend fitting routine
  double fit(const vector&, const vector&,
	     vector&, const blitz::Array<bool, 1>&);

  // this deals with independent variable points, so needs to be
  // defined in the implementation class
  virtual double mrqcof(vector&, matrix&, vector&) const=0;

public:
  void setSilent() {verbose=0;};
  void setNormal() {verbose=1;};
  void setVerbose() {verbose=2;};
  
  // Set fit routine parameters
  void setAccuracy(double abs, double rel) {ABSACC=abs; RELACC=rel;};
  void setFailretries(int n) {FAILRETRIES=n;};
  void setInitialStep(double is) {INITLAMBDA=is;};
  void setSuccessincr(double si) {SUCCESSINCR=si;};
  void setFaildecr(double fd) {FAILDECR=fd;};
};


template <typename T>
class fitter_impl : public fitter {
public:
  // fitting functor type
  typedef T T_function;

  // independent variable type
  typedef typename T_function::T_indep T_indep;

  typedef blitz::Array<T_indep, 1> T_indep_vector;

private:
  // functor
  T_function& f;

  // x values
  blitz::Array<T_indep, 1> x;
  
  // calculates covariance matrix, returns xi2
  virtual double mrqcof(vector&, matrix&, vector&) const;

public: 
  // Constructor - argument is starting step for numerical derivative
  // and accuracy of numerical derivative
  fitter_impl(T_function& ff) :
    f(ff), fitter() {};

  // Perform fit, returning chisquare
  double fit(const T_indep_vector& xx, const vector& yy,
	     const vector& ss, vector& pp);

  // Perform fit with fixed parameters indicated by fixa
  double fit(const T_indep_vector& xx, const vector& yy,
	     const vector& ss, vector& pp,
	     const blitz::Array<bool, 1>& ff);

  matrix getParameterError();
  const matrix& getCovar() const {return C;};
  int getndata() const {return ndata;};
  int getnpar() const {return nfit;};
};

template<class T>
void gaussj(blitz::Array<T,2>& A, blitz::Array<T,2>& b);

// Simple wrapper for the case of a 1d rhs
template<class T>
void gaussj(blitz::Array<T,2>& A, blitz::Array<T,1>& b) {
  blitz::Array<T,2>temp(b.dataFirst(),shape(b.size(),1),
		    blitz::neverDeleteData);
  gaussj(A,temp);
};

fitter::fitter() 
{
  verbose=2; // Initially print info

  // initialize with reasonable values
  INITLAMBDA=0.001;
  SUCCESSINCR=0.1;
  FAILDECR=10;
  ABSACC = 1e-5;
  RELACC = 1+1e-5;
  FAILRETRIES =7;
}

// Just creates a "fit all" fixed vector and calls the main fit routine
template <typename T>
double fitter_impl<T>::fit(const T_indep_vector& xx, const vector& yy,
			   const vector& ss, vector& pp)
{
  blitz::Array< bool, 1> ff(pp.size());
  ff=true;

  return fit(xx,yy,ss,pp,ff);
}


// This is the callable routine, performs the fit
template <typename T>
double fitter_impl<T>::fit(const T_indep_vector& xx, const vector& yy,
			   const vector& ss, vector& pp,
			   const blitz::Array<bool, 1>& ff)
{
  // We do everything concerning x here and then throw it back to the
  // backend x-independent fitting routine
  assert(xx.size()==yy.size());
  x.resize(xx.size());
  x=xx;

  return fitter::fit(yy,ss,pp,ff);
}

// Back-end fitting routine that doesn't need to know anything about x
double fitter::fit(const vector& yy, const vector& ss, vector& pp,
		   const blitz::Array<bool, 1>& ff)
{
  assert(yy.size()==ss.size());
  assert(pp.size()==ff.size());

  if(verbose==2)
    std::cout << "Starting fit\n";

  // Copy the input parameters
  npar = pp.size();
  ndata= yy.size();

  par.resize(npar);
  fixed.resize(npar);
  par=pp;
  fixed=ff;

  nfit=0;
  for (int j=0;j<npar;j++)
    if (fixed(j))
      ++nfit;

  // Copy the data points 
  y.resize(ndata);
  sig2i.resize(ndata);
  y=yy;
  sig2i=1/(ss*ss);

  // Initialization originally in mrqmin

  // atry needs to have all components because it's sent to the function
  atry.resize(npar); 
  // But all the linear eq solving stuff only needs nfit parameters
  beta.resize(nfit);
  da.resize(nfit);
  //daguess.resize(nfit);
  covar.resize(nfit,nfit);
  alpha.resize(nfit,nfit);

  // oneda=matrix<double>(npar,1,0.0);

  // Set initial value of alambda
  alambda=INITLAMBDA;
  
  // Copy our parameters to atry (the fixed parameters will stay unchanged
  // in there from now on)
  atry = par;

  xi2 = mrqcof(atry, alpha, beta);
  oxi2= xi2;

  // I hope we can go this far without dof, but I think we'll be ok.
  if(ndata-nfit<=0) {
    // No degrees of freedom - can't fit
    // But what do we set the parameter uncertainty to? Inf?
    std::cerr << "lsqfitter2::fit - no degrees of freedom!\n";
    return xi2;
  }

  // This completes the initialization
  // Now we should iterate the fitting
  // we iterate until we've converged
  // should put in a "fail to converge" check

  if(verbose==2) {
    std::cout << "Initialization completed, starting iteration\n";
    std::cout << " initial chisquare is " << xi2 << '\n';
  }
  
  int iter=0;
  int fail=0;
  double loxi2=xi2+1000;
  while( (((loxi2-xi2)>ABSACC) && ((loxi2/xi2)>RELACC)) || fail ){
    loxi2=xi2;

    if(!mrqmin())
      // Step failed, increase count
      fail++;
    else
      // Reset count if a step succeeded
      fail=0;

    if(fail>FAILRETRIES)
      // Assume we're fucked
      fail=0;
     
    iter++;
    if(verbose==2) {
      std::cout << "Iteration " << iter << ":\n";
      std::cout << "  Chisquare = " << xi2 << '\n';
      std::cout << "  Parameters are " << par << "\n\n";
    }
    else if(verbose==1) {
      std::cout << iter << '\t' << xi2;
      for(int i=0;i<npar;i++)
	std::cout << '\t' << par(i);
      std::cout << '\n';
    }
  }
  
  //covar=invert(alpha);
  C.resize(npar,npar);
  C = matrix(covsrt(covar));
  pp=par;

  if(verbose==2) {
    std::cout << "\nFit converged with a chisquare of " << xi2;
    std::cout << "\nNumber of degrees of freedom are " << ndata-nfit;
    std::cout << "\nReduced chisquare is " << xi2/(ndata-nfit);
    std::cout << "\n\nBest-fit parameters are:\n\t" << par << '\t';
    std::cout << "\nCovariance matrix is\n\t" << C << '\n'; 
  }
    
  // Return reduced chisquare
  return xi2/(ndata-nfit);
}



// This function is called repeatedly as part of the minimization
// procedure
bool fitter::mrqmin()
{
  assert(covar.rows()==nfit);
  assert(covar.columns()==nfit);
  assert(da.size()==nfit);

  // Alter linearized fitting matrix, by augmenting diagonal elements
  // Because covar, alpha, beta and oneda only contains non-fixed parameters
  // this is simple
  covar=alpha;
  da=beta;
  for (int j=0;j<nfit;j++)
    covar (j, j)*= 1.0+alambda;
  
  // Matrix solution - returns inverse of covar in covar 
  // and solutions to COVAR*x=oneda in oneda
  gaussj(covar,da);

  // Did the trial succeed?

  // Update atry with changing parameters
  for (int j=0, l=0;l<npar;l++)
    if (fixed(l)) 
      atry (l)=par(l)+da (j++);
  
  xi2 = mrqcof(atry,covar,da);

  if (xi2 < oxi2) {
    // Success - return the new solution

    // Increase step for next time
    alambda *= SUCCESSINCR;
    if(verbose==2)
      std::cout << "success, new alambda is " << alambda << "\n";

    // Update with the new solution
    oxi2=xi2;
    alpha=covar;
    beta=da;
    par=atry;

    return true;
  } 
  else {
    // Failure - increase alambda and return
    alambda *= FAILDECR;
    if(verbose==2)
      std::cout << "failure, new chisquare is " << xi2
	   << ", new alambda is " << alambda << "\n";
    xi2=oxi2;

    return false;
  }
}

// Unlike numerical recipes covar is "descrambled" and put in C here,
// because we have them in different sizes
fitter::matrix fitter::covsrt(const matrix& cov) const
{
  // C in npar^2 but cov is nfit^2
  matrix C(npar,npar);
  for(int i=0;i<nfit;i++)
    for(int j=0;j<nfit;j++)
      C(i,j)=cov(i,j);

  for (int i=nfit;i<npar;i++)
    for (int j=0;j<=i;j++) {
      C(i,j)=0;
      C(j,i)=0.0;
    }

  for (int j=npar-1, k=nfit-1; j>=0; j--) {
    if (fixed(j)) {
      for (int i=0;i<npar;i++) {
	const double temp=C(i,k);
	C(i,k)=C(i, j);
	C(i,j)=temp;
      }
      for (int i=0;i<npar;i++) {
	const double temp=C(k, i);
	C(k, i)=C(j, i);
	C (j, i)=temp;
      }
      k--;
    }
  }
  return C;
}

template <typename T>
double fitter_impl<T>::mrqcof(vector& p, matrix& al,
			      vector& be) const
{
  // When we come in we have the old da in be, so we can use that info to
  // guess what a reasonable da in the derivative estimation can be (if we
  // need to use the numerical estimator

  // set alpha and beta to zero
  al=0;
  be=0;

  //double chisq = 0;
  vector daguess(be.size());
  daguess=be;
  f.setdaguess(da);

  // get data points
  const vector current_y = f(x,p);
  const vector dy(y- current_y);
  
  // Get the parameter derivative - calls the numerical version if it
  // wasn't redefined to an analytical one -- hmm how do we do this?
  // (ndata*npar matrix)
  const matrix current_dyda = f.dyda(x,p,fixed);

  // Make up the alpha and beta matrices
  for(int j=0,l=0;l<npar;l++)
    // l counts all parameters, j counts fitting parameters
    
    // Is this parameter being varied?
    if(fixed(l)) {
      const vector wt(current_dyda(blitz::Range::all(),l) * sig2i);
      
      for(int k=0,m=0;m<=l;m++)
	// m counts all parameters, k counts fitting parameters
	
	// What about this one?
	if(fixed(m)) {
	  // Yes, update this matrix entry
	  al (j, k) = sum(wt*current_dyda(blitz::Range::all(),m));
	  // it's a symmetric matrix
	  al (k, j) = al (j, k);
	  ++k;
	}
      
      // Update the right-hand side entry
      be(j) = sum(dy*wt);
      j++;
    }

  const double chisq= sum(dy*dy*sig2i);
  return chisq;
}


// Evaluates parameter derivatives dy/da numerically
template <typename T, typename T_leaf>
typename numder_fitting_function<T, T_leaf>::matrix
numder_fitting_function<T, T_leaf>::dyda(const T_indep_vector& x,
					 const vector& a,
					 const blitz::Array<bool, 1>& fixed)
{
  // We want to compute the derivative of function f wrt the parameters a
  // around the current point x,a
  // We do this by using the finite difference approximation
  // dy/dx = 1/(2h)*( y(x+h)-y(x-h) )

  //std::cout << "Evaluating dy/da:\n";
  const int npar=a.size();
  const int ndata=x.size();
  vector da(npar); // Small difference in a
  matrix dyoverda(ndata,npar); // contains dy/da(point, parameter)

  // Use daguess*fudge factor estimate behaviour
  // But daguess has nfit length, not npar

  // Loop over the parameters a
  for(int i=0,j=0; i<npar; i++)
    // i counts all parameters, j fitting parameters

    // It's timeconsuming - only do it if we need the derivative
    if(fixed(i)) {
      // We need a GOOD arbitrary starting guess for h, the parameter
      // diff da If h is too large we will have to iterate for long to
      // converge if it's too small we'll get cancellation and bad
      // precision (Note that a good h could well be very different
      // for the diff parameters)
      double h=DY_INITIALSTEP;

      vector ody(ndata);
      int iter=0;
      // If we don't have an old da we do it the slow way
      assert(daguess.size()>j);
      if(daguess(j)==0.) {
	// Shrink step until we've converged
	dyoverda(blitz::Range::all(),i)=0;
	da(i)=h;
	ody = (static_cast<const T_leaf&>(*this)(x,vector(a+da)) - 
	       static_cast<const T_leaf&>(*this)(x,vector(a-da)))/(2.*h);
	h/=5.;
	iter++;

	da(i)=h;
	dyoverda(blitz::Range::all(), i) = (static_cast<const T_leaf&>(*this)(x,vector(a+da)) - 
				 static_cast<const T_leaf&>(*this)(x,vector(a-da)))/(2.*h);
	h/=5.;
	iter++;
	while( max(abs(dyoverda(blitz::Range::all(), i)-ody))>DY_ACCURACY ) {
	  da(i)=h;
	  ody=dyoverda(blitz::Range::all(), i);
	  dyoverda(blitz::Range::all(), i) = (static_cast<const T_leaf&>(*this)(x,vector(a+da)) - 
				   static_cast<const T_leaf&>(*this)(x,vector(a-da)))/(2*h);
	  h/=5;
	  iter++;
	}
	//std::cout << "Parameter#" << i << " required "<< iter 
	//<< " at x=" << x << '\n';
      }
      else {
	da(i)=daguess(i)*0.01;
	dyoverda(blitz::Range::all(),i)= (static_cast<const T_leaf&>(*this)(x,vector(a+da)) - 
			       static_cast<const T_leaf&>(*this)(x,vector(a-da)))/(2.*da(i));
      }
      da(i)=0;
      j++;
    } // if fixed
  else 
    // prameter is fixed, just set derivative to 0
    dyoverda(blitz::Range::all(),i)=0;

  return dyoverda;
}

// Linear equation solution by Gauss-Jordan elimination
// A is the input matrix of size n*n
// b is an array of m right hand side vectors that will be solved
// A is returned as what?
template<class T>
void gaussj(blitz::Array<T,2>& A, blitz::Array<T,2>& b)
{
  assert(A.rows() == A.columns());
  assert(A.rows() == b.rows());

  const int n=A.rows();
  const int m=b.columns();

  blitz::Array<int,1> vic(n);
  blitz::Array<int,1> vir(n);
  blitz::Array<int,1> vip(n);
  vic=0;
  vir=0;
  vip=0;

  int icol,irow;

  for (int i=0;i<n;i++) {
    T big=0.0;
    for (int j=0,J=0;j<n;j++,J+=n)
      if (vip(j) != 1)
	for (int k=0;k<n;k++) {
	  if (vip(k) == 0) {
	    if (fabs(A(j,k)) >= big) {
	      big=fabs(A(j,k));
	      irow=j;
	      icol=k;
	    }
	  } 
	  else if (vip(k) > 1) 
	    std::cerr << "gaussj: Singular matrix, 1!\n";
	}
    ++vip(icol);

    // We now have the pivot element, so we interchange rows, if needed,
    // to put the pivot element on the diagonal. The columns are not
    // physically interchanged, only relabelled:
    // indxc[i] is the column of the ith pivot element, is the ith column
    // that is reduced, while indxr[i] is the row in which that pivot element
    // was originally located. With this form of bookkeeping, the solution b's
    // will end up in the correct order, and the inverse matrix will be
    // scrambled by columns.
    if (irow != icol) {
      for (int l=0;l<n;l++) {
	const T temp=A(irow,l);
	A(irow,l)=A(icol,l);
	A(icol,l)=temp;
      }
      for (int l=0;l<m;l++) {
	const T temp=b(irow,l);
	b(irow,l)=b(icol,l);
	b (icol,l)=temp;
      }
    }

    // We are now ready to divide the pivot row by the pivot element
    // located at irow,icol

    vir(i)=irow;
    vic(i)=icol;
    
    if (A(icol,icol) == 0.0)
      std::cerr << "gaussj: Singular matrix, 2!\n";
    
    const T pivinv=1.0/A(icol,icol);
    A(icol,icol)=1.0;

    A(icol,blitz::Range::all())*=pivinv;
    //for (int l=0;l<n;l++) 
    //A(icol,l) *= pivinv;
    b(icol,blitz::Range::all())*=pivinv;
    //for (int l=0;l<m;l++) 
    //b(icol,l) *= pivinv;

    // Next, reduce the rows
    for (int ll=0,LL=0;ll<n;ll++,LL+=n)
      if (ll != icol) { // except for the pivot row, of course.
	const T dum=A(ll,icol);
	A(ll,blitz::Range::all()) -= A(icol,blitz::Range::all())*dum;
	A(ll,icol)=0.0; // Expl. set to 0 to avoid trunc error?
	//for (int l=0;l<n;l++) 
	//A(ll,l) -= A(icol,l)*dum;
	b(ll,blitz::Range::all()) -= b(icol,blitz::Range::all())*dum;
	//for (int l=0;l<m;l++) 
	//b(ll,l) -= b(icol,l)*dum;
	  //	  (b[l])[ll] -= (b[l])[icol]*dum;
      }
  }

  // This is the end of the main loop over columns of the reduction. It only
  // remains to unscramble the solution in view of the column interchanges
  // We do this by interchanging pairs of columns in reverse order that
  // the permutation was built up.
  for (int l=n-1;l>=0;l--)
    if (vir(l) != vic(l))
      for (int k=0,K=0;k<n;k++,K+=n) {
	const T temp=A(k,vir(l));
	A(k,vir(l))=A(k,vic(l));
	A(k,vic(l))=temp;
      }
  // And we are done
}




#endif

