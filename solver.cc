// Solver.cc
//
// Implementation of numerical equation solver routines
// Uses Newton-Raphson
//
// Patrik Jonsson
//

#include "solver.h"
#include <math.h>
#include <iostream>
#include <stdarg.h>

using namespace std;

// Constructor for a solver
// A solver has a specified function, accuracy and number of parameters
solver::solver(double(*f)(double,double*), double(*d)(double,double*),
	       int n, double a)
{
  func = f;
  der = d;
  parameters = 0;
  accuracy = a;
  npar = n;

  parameters = new double[npar]; // Allocate array of parameters
}

// Call to Solve returns solution of f(x)=0. For an equation with multiple
// roots, which root is returned depends on the inital guess
// The ellipsis are n additional parameters propagated to
// the function and had better conform to its syntax.
double solver::Solve(double x0 ...)
{
  if(npar>0) {
    // Find parameters given to integrand and put them in the array
    va_list ap;
    va_start(ap,x0);
    for(int i=0;i<npar;i++)
      parameters[i] = va_arg(ap,double);
    va_end(ap);
  }

  const int maxsteps = 1000;

  double f;
  double x = x0;
  int Nstep = 0;

  while( (fabs(f=func(x,parameters))>accuracy) && (Nstep<maxsteps) ) {
    Nstep++;

    double fp = der(x,parameters);
    if(fp==0) {
      cout << "solver::Solve: Warning, extremum encountered\n";
      break;
    }
     else
      x-= f/fp;
  }

  if(Nstep==maxsteps)
    cout << "solver::Solve: Warning, failed to converge\n";

  //cout << Nstep << " steps\n";
  return x;
}
