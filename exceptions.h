#include <exception>

class file_not_found : public std::exception {
public:
  file_not_found(const char*) : std::exception() {};
  const char* what() const throw() {return "File not found";};
};

