// $Id$
//
// Declaration of counter class

#ifndef __counter__
#define __counter__

#include <iostream>

// Counter class counts something and outputs with regular
// intervals
class counter {
private:
  int value;
  int interval;


public:
  static int enable; // If this is zero, counter object don't print

  explicit counter(int i) {
    value = 0;
    interval = i;
  };
  
  void reset() {
    value = 0;
  };
  int operator= (int v) {return value = v;};
  int operator++ (int) {return incr ();};
  int operator+= (int a) {
    if( !((value +=a)%interval) && enable) {
    std::cout << ' ' << value << '\r';
      std::cout.flush();
    }
    return value;
  };
  int incr() {
    if( !((++value)%interval) && enable) {
    std::cout << ' ' << value << '\r';
      std::cout.flush();
    }
    return value;
  };
};


#endif

