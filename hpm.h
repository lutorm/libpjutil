/// \file hpm.h
/// Wrapper for libhpm that can be turned on and off.

#ifndef __hpm__
#define __hpm__

#include<cstring>
#include<iostream>

/** Contains routines for sampling performance counters. (Name comes
    from the original HPM toolkit on Seaborg.) */
namespace hpm {
  class scoped_stage;

  extern bool enable_;
  inline void disable () {enable_= false;};

#if !defined(HAVE_LIBPAPI) && !defined(HAVE_LIBHPM)

  // To avoid overhead, inline empty calls if no lib is defined
  inline void enable () {std::cerr << "Warning: no performance monitoring support found" << std::endl;};
  inline void init() {};
  inline void thread_init(int thread_id, int num_stages) {};
  inline void set_stage_name(int, const std::string&) {}
  inline void thread_start(int stage) {};
  inline void enter_stage(int stage) {};
  inline void push_stage(int stage) {};
  inline void pop_stage() {};
  inline void thread_stop(const std::string& label, const std::string& = std::string()) {};
  inline void dump_data(const std::string& label, std::ostream& =std::cout) {};
  inline int current_stage() {return 0;};
  inline void enumerate_events() {};

#else

  void handle_papi_error(int);
  void accumulate_stage(int);
  void enter_stage_impl(int);
  void push_stage_impl(int);
  void pop_stage_impl();

  inline void enable () {enable_= true;};
  /** Initialize the performance monitoring library. Must be called
      before any other calls (except enable/disable). */
  void init();
  /** Initialize the counters for a multithreaded
      measurement. Thread_id is the ID of the calling thread,
      num_stages the number of independent stages to measure. This
      function should be called for each thread. */  
  void thread_init(int thread_id, int num_stages);
  /** Define the name for a stage, used when outputting the data. */
  void set_stage_name(int, const std::string&);
  /** Start the counters for this thread. stage specifies which stage
      is entered. */
  void thread_start(int stage);
  /** Replaces the current stage with the specified stage. The
      counters are read out and the values for the current stage are
      increased. Then counting starts in the new stage. To avoid
      overhead if counters are disabled, this is an inlined forwarding
      function to enter_stage_impl() that actually does the work.  */
  inline void enter_stage(int stage) {if (enable_) enter_stage_impl(stage); };
  /** Pushes a new stage onto the stack of stages. Counters are read
      out for the current stage, and the new stage becomes the current
      stage. */
  inline void push_stage(int stage) {if (enable_) push_stage_impl(stage); };
  /** Reads out the counters for the current stage and activates the
      previously active stage. */
  inline void pop_stage() {if (enable_) pop_stage_impl(); };

  /** Stop the counters and print out the results. The counters and
      derived values are printed out for all the defined stages. label
      is just a label that is printed out at the top to differentiate
      different measurements. If a filename is supplied, the data is
      written to that file (using a mutex so different threads don't
      clobber each other. */
  void thread_stop(const std::string& label, const std::string& = std::string());
  void dump_data(const std::string& label, const std::string& = std::string());
  /// Returns the stage for which the counters are currently running.
  int current_stage();
  void enumerate_events();

#endif

};

/** This class implements a scoped stage that is automatically
    popped when the object goes out of scope. That way we never have
    to worry about forgetting to pop. */
class hpm::scoped_stage {
public:
  scoped_stage(int stage) { hpm::push_stage(stage); };
  ~scoped_stage() { hpm::pop_stage(); };
};
  


#endif

