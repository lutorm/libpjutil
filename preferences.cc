// Implementations of the functions of the Preferences class, along
// with declarations and definitions of the prefvalue classes, the
// templates of which are explicitly instantiated in this file to
// insulate usersof the Preferences class from dependencies on the
// templates and the CCfits files.

#include <fstream>
#include <cctype>
#include<sstream>
#include<string>
#include "misc.h"
#include "preferences.h"
#include "blitz/array.h"
#include<iostream>
#include <map>
#include "CCfits/CCfits"
#include "exceptions.h"

using namespace std;
using blitz::TinyVector;
typedef TinyVector<double,3> vec3d;
typedef TinyVector<int,3> vec3i;

// *** prefvalue classes ***

class prefs {
protected:
  /// Keeps track of whether the value has been accessed.
  mutable bool used_;
  string comment;
  prefs () : used_(false) {};
  prefs (string cmt): used_(false), comment (cmt) {};
public:
  prefs (const prefs & p): used_(false), comment (p.comment) {};

  virtual ~prefs() {};
  virtual prefs* clone () const =0;
  
  virtual istream& read(istream& is)=0;
  virtual ostream& write(ostream& os)=0;

  bool used() const { return used_; };
  void mark_used() const { used_=true; };
  void set_comment (string cmt) {comment = cmt;};
  virtual void write_fits_key (CCfits::HDU&, const string&) const = 0;
  
};
template void CCfits::Column::write (const valarray<long> &, long, long);

template<class T>
class prefvalue : public prefs {
protected:
   T _v;
public:
  prefvalue() {};
  prefvalue(T val, string cmt = "") : prefs (cmt),_v(val) {};
  prefvalue (const prefvalue & p):prefs (p),_v (p._v) {};
  virtual prefvalue* clone () const {return new prefvalue (*this);};

  const T& v() const { this->used_=true; return _v; };
  virtual istream& read(istream& is);
  virtual ostream& write(ostream& os);
  virtual void write_fits_key (CCfits::HDU &, const string&) const;
};

template <class T>
void prefvalue<T>::write_fits_key (CCfits::HDU& file, const string& name ) const
{
  // use _v directly here so we don't set it to used
  file.addKey(name, _v, comment);
}


// This specialization of the class is necessary because of the
// specialization of write_fits_key (and clone)
template<class T>
class prefvalue<TinyVector<T, 3> >: public prefs {
protected:
  TinyVector <T, 3>  _v;
public:

  prefvalue() {};
  prefvalue(TinyVector<T, 3> val, string cmt = "") : prefs (cmt),_v(val) {};
  prefvalue (const prefvalue & p):prefs (p),_v (p._v) {};
  virtual prefvalue* clone () const {return new prefvalue (*this);};
  const TinyVector<T, 3>& v() const { this->used_=true; return _v; };
  virtual istream& read(istream& is);
  virtual ostream& write(ostream& os);
  virtual void write_fits_key (CCfits::HDU &, const string&) const;
};

// this specialized version writes 3-vectors under the keywords NAMEX,
// NAMEY and NAMEZ, since fits files can't do three-vector keywords. 
template <class T>
void prefvalue<TinyVector<T, 3> >::write_fits_key (CCfits::HDU& file, const string& name ) const
{
  file.addKey(name + "X", v() [0], comment);
  file.addKey(name + "Y", v() [1], "");
  file.addKey(name + "Z", v() [2], "" );
}

inline istream& operator>>(istream& is, prefs& p) 
{ 
  return p.read(is); 
}

inline ostream& operator<<(ostream& os, prefs& p) 
{ 
  return p.write(os); 
}

template<class T>
inline istream& prefvalue<T>::read(istream& is)
{
  is >> _v;
  return is;
}

template<class T>
inline istream& prefvalue<TinyVector<T, 3> >::read(istream& is)
{
  is >> _v;
  return is;
}

template<class T>
inline ostream& prefvalue<T>::write(ostream& os)
{
  os << _v << "\t# " << comment;
  return os;
}

template<class T>
inline ostream& prefvalue<TinyVector<T, 3> >::write(ostream& os)
{
  os << _v << "\t# " << comment;
  return os;
}

template < >
inline ostream& prefvalue<string>::write(ostream& os)
{
  os << "\"" << _v << "\"\t# " << comment;
  return os;
}

bool Preferences::defined(const string& kw) const
{
  map<string, prefs*>::const_iterator i= keywords.find(kw);
  return(i!= keywords.end());
}

Preferences::Preferences (const Preferences & p)
{
  // copy constructor must explicitly copy all keywords
  map<string,prefs*>::const_iterator i = p.keywords.begin();
  while (i != p.keywords.end()) {
    keywords [i->first] = i->second->clone();
    ++i;
  }
}

Preferences::~Preferences()
{
  // The destructor deletes all the pref objects
  map<string, prefs*>::const_iterator i= keywords.begin();
  while(i!= keywords.end())
    delete (i++)->second;
}

bool Preferences::deleteValue (const string& kw)
{
  map<string,prefs*>::iterator i = keywords.find(kw);
  if (i != keywords.end()){
    delete i->second;
    keywords.erase(i);
    return true;
  }
  else
    return false;
}

// copies a keyword from another preferences object
bool Preferences::copyKeyword (const Preferences & p, const string & kw)
{
  if (& p == this)
    return false;

  map<string,prefs*>::const_iterator i = p.keywords.find(kw);
  if (i != p.keywords.end())
    keywords [kw] = i->second->clone();
  else
    return false;

  return true;
}

void Preferences::mark_used(const std::string& kw) const
{
  const prefs*const p =find(kw);
  if(p) 
    p->mark_used();
  else
    throw unknown_keyword();
}


template<class valueType> 
valueType Preferences::getValue(const string& kw, 
				const valueType& v,
				bool use_default) const
{
  const prefs*const p =find(kw);
  if(p) {
    // keyword exists
    // try to dynamic_cast it to the requested type
    //prefs* p = (*i).second;
    const prefvalue<valueType>* vp = dynamic_cast<const prefvalue<valueType>*>(p);
    if(vp)
      // cast succeeded - return value
      return vp->v();
    else {
      // cast failed - throw type mismatch exception
      cerr << "Preferences::getValue: Type mismatch error on keyword \'" 
	<< kw << "\'\n";
      throw type_mismatch();
    }
  }
  else {
    // keyword doesn't exist

    if(use_default)
      // use default value
      return v;

    cerr << "Preferences::getValue: Unknown keyword: \'" 
      << kw << "\'\n";
    throw unknown_keyword();
  }
}

// Specialized version of the template for doubles, which will also
// correctly convert integer keywords.
template<> 
double Preferences::getValue(const string& kw, 
			     const double& v,
			     bool use_default) const
{
  const prefs*const p =find(kw);
  if(p) {
    // keyword exists
    // try to dynamic_cast it to the requested type
    const prefvalue<double>* vp = dynamic_cast<const prefvalue<double>*>(p);
    if(vp)
      // cast succeeded - return value
      return vp->v();
    else {
      // try integer
      const prefvalue<int>* vp2 = dynamic_cast<const prefvalue<int>*>(p);
      if(vp2)
	//succeeded -- return value (implicitly convert into double)
	return vp2->v();
      
      // cast failed - throw type mismatch exception
      cerr << "Preferences::getValue: Type mismatch error on keyword " 
	<< kw << '\n';
      throw type_mismatch();
    }
  }
  else {
    // keyword doesn't exist

    if(use_default)
      // use default value
      return v;

    cerr << "Preferences::getValue: Unknown keyword: " 
      << kw << '\n';
    throw unknown_keyword();
  }
}


// Specialized version of the template for integers, which will also
// correctly convert double keywords.
template<>
int Preferences::getValue(const string& kw,
			  const int& v,
			  bool use_default) const
{
  const prefs*const p =find(kw);
  if(p) {
    // keyword exists
    // try to dynamic_cast it to the requested type
    const prefvalue<int>* vp = dynamic_cast<const prefvalue<int>*>(p);
    if(vp)
      // cast succeeded - return value
      return vp->v();
    else {
      // try double
      const prefvalue<double>* vp2 = dynamic_cast<const prefvalue<double>*>(p);
      if(vp2) {
	// throw if it seems that we are inadvertently casting to int
	// when we have actually specified a double; e.g., we don't
	// want to allow nrays = 10.5
	if(((vp2->v()-floor(vp2->v())) > 1e-6) || abs(vp2->v()) < 0.9999) {
	  cerr << "Preferences::getValue: Integer keyword has non-integer value."
	       << endl;
	  throw type_mismatch();
	}

        return round (vp2->v());
      }
      // cast failed - throw type mismatch exception
      cerr << "Preferences::getValue: Type mismatch error on keyword "
	   << kw << '\n';
      throw type_mismatch();
    }
  }
  else {
    // keyword doesn't exist

    if(use_default)
      // use default value
      return v;
    cerr << "Preferences::getValue: Unknown keyword: "
	 << kw << '\n';
    throw unknown_keyword();
  }
}


void Preferences::setValue(const string& kw, prefs*p)
{
  assert(p);

  if (defined (kw))
    // It's already defined, delete the existing prefvalue
    delete keywords [kw];

  keywords [kw] = p;
}

template<class valueType> 
void Preferences::setValue(const string& kw, 
			   const valueType& val, const string& cmt)
{
  // allocate and assign a prefvalue 
  prefs* p = new prefvalue<valueType>(val, cmt);
  // keywords set with this method 
  setValue (kw, p);
}

// Explicitly instantiate the templates necessary
template class prefvalue<bool> ;
template class prefvalue<int> ;
template class prefvalue<double> ;
template class prefvalue<string> ;
template class prefvalue<blitz::TinyVector<int,3> >;  
template class prefvalue<blitz::TinyVector<float,3> >;  
template class prefvalue<blitz::TinyVector<double,3> >;  

template bool Preferences::getValue (const string &, const bool &, bool) const;
template int Preferences::getValue (const string &, const int &, bool) const;
template string Preferences::getValue (const string &, const string &, bool) const ;
template blitz::TinyVector<int,3> Preferences::getValue (const string &, const blitz::TinyVector<int,3>&, bool) const;
template blitz::TinyVector<double,3> Preferences::getValue (const string &, const blitz::TinyVector<double,3>&, bool) const;

template void Preferences::setValue (const string &, const bool &, const string &);
template void Preferences::setValue (const string &, const string &, const string &);
template void Preferences::setValue (const string &, const int &, const string &);
template void Preferences::setValue (const string &, const double &, const string &);
template void Preferences::setValue (const string &, const blitz::TinyVector<int,3>  &, const string &);
template void Preferences::setValue (const string &, const blitz::TinyVector<float,3>  &, const string &);
template void Preferences::setValue (const string &, const blitz::TinyVector<double,3>  &, const string &);


// Reads a line of <keyword> <value> <comment (if any)>
// The keyword entry must already exist
istream& Preferences::readline_keywordfirst(istream& is)
{
  eatwhite(is); // Read initial whitespace, if any

  // Read the line
  char buf[255];
  is.get(buf,255,'\n');
  
  if(buf[0]!='#') {
    // It's not a whole-line comment
    
    // create a strstream
    istringstream iss;//(buf);
    string kw,v;

    // Read the keyword
    iss >> kw;

    // and read the value (string ones just read until next whitespace as well)
    map<string, prefs*>::iterator entry = keywords.find(kw);
    if(entry!= keywords.end())
      iss >> *(entry->second);
  }

  // And the comment is just junk anyway so we don't parse the rest
  is.get(buf[0]);
  return is;
}

// Reads a line of <keyword> <value> <comment (if any)>
// The keyword entry must already exist
istream& Preferences::readline_keywordlast(istream& is)
{
  eatwhite(is); // Read initial whitespace, if any

  // Read the line
  char buf[255];
  is.get(buf,255,'\n');
  
  if(buf[0]!='#') {
    // It's not a whole-line comment

    // create a strstream
    istringstream iss;//(buf);
    string kw,v;

    // Read the value and keyword
    iss >> v >> kw;
    iss.seekg(0);

    // create a strstream that points to the rest of the stuff (keyword)
    //istrstream kws(&buf[i+1],254-i);
    // and one to the value
    //istrstream vs(buf,i);
    // and read the value (string ones just read until next whitespace as well)
    //string kw;
    //kws >> kw;
    map<string, prefs*>::iterator entry = keywords.find(kw);
    if(entry!= keywords.end())
      iss >> *(entry->second);
  }

  // And the comment is just junk anyway so we don't parse the rest
  is.get(buf[0]);
  return is;
}


bool Preferences::comment_character (char c)
{
  return ((c == '#') || (c == '%') || (c == ';') || (c == '/'));
}

// reads keywords from a file
bool Preferences::readfile (const string & filename)
{
  ifstream file_stream (filename.c_str());

  if (!  file_stream) {
    cerr << "Preferences::readfile: Error opening parameter file " <<
      filename << "\n"; 
    throw file_not_found(filename.c_str());
    //return false;
  }

  return read (file_stream);
}

// reads keywords from a stream
bool Preferences::read (istream & file_stream) 
{
  while (file_stream.good()) {
    // read a line
    eatwhite (file_stream);
    string buffer;
    getline(file_stream, buffer);
    //cout  << buffer << endl; 
    if ((buffer.length () > 0) &&(!comment_character (buffer [0])))
      {
	// it's not a blank line or a whole-line comment
	istringstream keyword_stream (buffer);
	string keyword;

	// read keyword
	keyword_stream >> keyword;

	// this hack is because the vector operator >> doesn't work
	// like it should
	string value;
	getline (keyword_stream, value);
	
	prefs* p = 0;
	vec3d vd;
	vec3i vi;
	double d;
	int i;
	string s, comment;
	bool comment_found = false;

	// Now we rely on the stream to figure out what the type is
	// this doesn't work, the string "2002-8-8" will be read as three consecutive integers by the vector input operator, which clearly isn't right.
	istringstream vs (value);
	if ((vs  >> vi[0] >> vi [1] >> vi [2]) &&
	    (isspace (vs.peek()) || vs.eof())) {
	  //keyword_stream >> vi;
	  p = new prefvalue<vec3i> (vi);
	}
	else {
	  vs.str(value);
	  vs.clear();
	  if (vs >> vd [0] >> vd [1] >> vd [2] ) {
	    //keyword_stream >> vd;
	    p = new prefvalue<vec3d> (vd);
	  }
	  else {
	    vs.str(value);
	    vs.clear();
	    if ((vs  >> i) && (isspace (vs.peek()) || vs.eof())) {
	      //keyword_stream >> i;
	      p = new prefvalue<int> (i);
	  }
	  else {
	    vs.str(value);
	    vs.clear();
	    if ((vs  >> d) && (isspace (vs.peek()) || vs.eof())) {
	      //if (vs >> d) {
	      //keyword_stream >> d;
	      p = new prefvalue<double> (d);
	    }
	    else {
	      // it's a string, so we put it together Word for Word
	      //string word;
	      char c, cc = 0;
	      int n_space = 0;
	      vs.str(value);
	      vs.clear();
	      eatwhite(vs);
	      
	      while (vs.get( c)&&//(vs >> word)  &&
		     (!(comment_character (c) && isspace (cc)))) 
		//(!(comment_character (word [0])&& (word.length()==1))))
		{
		  //s=s+ word + ' ';
		  s+= c;
		  if (isspace (cc))
		    n_space ++;
		  else
		    n_space = 0;
		  cc = c;
		}
	      // erase the comment character and the trailing space
	      if (comment_character (c) && !isspace (cc))
		// I think this means that the actual string ended
		// with a common character which naturally should not
		// be deleted
		--n_space;
	      s.erase (s.length() -(comment_character (c)?1:0)-n_space, s.length());
	      
	      if  ((s == "true") || (s == ".true.") ||
		   (s == ".TRUE.")) 
		// it's a Boolean true 
		p = new prefvalue<bool> (true); 
	      else if  ((s == "false") || (s == ".false.") ||
		   (s == ".FALSE.")) 
	      // it's a Boolean false 
		p = new prefvalue<bool> (false);
	      else
		// it's a string
	      p = new prefvalue<string> (s);
	      comment_found = true; // we've already read the comment
				// character, if any
	    }}}}
	assert (p);
	
	// now we can check for and read the comment
	if (!comment_found) {
	   string word;
	   if ((vs >> word) &&
	       (comment_character (word [0])))
	     comment_found = true;
	}
	if (comment_found) {
	  eatwhite (vs);
	  getline (vs, comment);
	}
	
	p->set_comment(comment);
	
	// and finally add this keyword to the list
	if (keyword == "include_file") {
	  // if it's include_file, we expand that value for variable
	  // substitution and then open that file.
	  readfile (word_expand(dynamic_cast<prefvalue<string>*> (p)->v ())[0]) ;
	  delete p;
	}
	else {
	  //cout << "setting keyword " << keyword << " to " << *p << endl;
	  setValue (keyword, p);
	}
      } // if not whole-line comment
  } //while
  return true;
}


void Preferences::print() const
{
  map<string,prefs*>::const_iterator i= keywords.begin();
  for(;i!= keywords.end(); ++i)
    {
      cout << i->first << "\t" << *(i->second)
	   << endl;
    }
}

void Preferences::print_unused() const
{
  map<string,prefs*>::const_iterator i= keywords.begin();
  while(i!= keywords.end()) {
    if(!i->second->used()) {
      cout << i->first << "\t" << *(i->second) << endl;
    }
    ++i;
  }
}
  
bool  Preferences::write_fits (CCfits::HDU & file) const
{
  // we don't need to check for 8 characters uppercase (and then by
  // inference uniqueness) because ccfits will write keywords with
  // more than 8 characters by using HIERARCH.

  //map <string, int> key_list; // for checking non-uniqueness

  map<string,prefs*>::const_iterator i = keywords.begin ();
  bool trouble (false);
  while (i != keywords.end ()) {
    const string& key = i->first;
    assert(i->second);

    // The first thing to do is to convert the keyword to fits
    // standard, maximum of eight characters all uppercase.
    string fitskey = key;//(key.length() > 8)? (key.substr (0, 8)): key;

    string::iterator si = fitskey.begin();
    bool conforming (true);
    while (si != fitskey.end()) {
      //*si = toupper (*si);
      if ((!isalnum (*si)) && (*si != '-') && (*si != '_'))
	conforming = false;
      ++si;
    }

    // OK, if it was conforming, and we don't have a duplicate, we write it
    if (conforming) {// && (key_list.find(fitskey)!= 0)) {
      try {
	i->second->write_fits_key(file, fitskey);
      } catch (...) {
	// trouble, apparently
	trouble = true;
      } 
      //key_list [fitskey];
    }
    else
      // nonconforming keyword, or duplicate
      trouble = true;

    ++i;
  }
  if (trouble)
    cout<< "FITS keywords contain illegal characters" <<endl;
  
  return !trouble;
}
