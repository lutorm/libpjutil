// complex.h
//
// Declaration of complex number class
//
// Patrik Jonsson 980815
//

#ifndef __complex__
#define __complex__

#include <math.h>
#include <stdarg.h>
#include "biniostream.h"

class complex {
private:
  double r,i; // Real and imaginary parts

 public:
  complex();
  complex(double); // Cast from real number
  complex(int); // Cast from int
  complex(double,double); // cartesian components
  complex(const complex&);
  ~complex();

  // Polar components
  friend complex polar(double,double);

  double& re();
  friend double& re(complex&);
  double& im();
  friend double& im(complex&);
  double abs() const;
  friend double abs(const complex&);
  double arg() const;
  friend double arg(const complex&);

  // Unary negation operator
  complex operator-() const;

  // complex-complex operators
  complex operator+(const complex&) const;
  complex operator-(const complex&) const;
  complex operator*(const complex&) const;
  complex operator/(const complex&) const;

  complex& operator=(const complex&);
  complex& operator+=(const complex&);
  complex& operator-=(const complex&);
  complex& operator*=(const complex&);
  complex& operator/=(const complex&);

  int operator==(const complex&) const;
  int operator!=(const complex&) const;


  // Complex-double operators are fully defined implicitly through
  // the double->complex cast
  // double-complex operators need to be defined
  friend complex operator+(double, const complex&);
  friend complex operator-(double, const complex&);
  friend complex operator*(double, const complex&);

  friend int operator==(double, const complex&);
  friend int operator!=(double, const complex&);


  // I/O operators
  friend ostream& operator<<(ostream&, const complex&);
  friend istream& operator>>(istream&, complex&);

  friend binofstream& operator<<(binofstream&, const complex&);
  friend binifstream& operator>>(binifstream&, complex&);
  
};

// Define imaginary constant i - worry about shadowing if you're used to
// i as a loop variable!
const complex i(0.,1.);

// **********************
// * Inline definitions *
// **********************

// Default constructor
// Creates zero
inline complex::complex()
{
  r=i=0;
}

// Copy constructor
inline complex::complex(const complex& z)
{
  r=z.r;
  i=z.i;
}

// Real constructor - creates a real complex
inline complex::complex(double real)
{
  r=real;
  i=0;
}

inline complex::complex(int real)
{
  r=(double)real;
  i=0;
}

// Explicit constructor - takes arguments in cartesian components
inline complex::complex(double real, double im)
{
  r=real;
  i=im;
}

// Destructor does nothing
inline complex::~complex()
{
}

// Creates a complex from polar components
complex polar(double mag, double arg)
{
  complex z( mag*cos(arg), mag*sin(arg) );

  return z;
}

// Returns a reference to the real part, should work with both assignment
// and reading
inline double& complex::re()
{
  return r;
}

// Same goes for "mathematical style" friend function
inline double& re(complex& z)
{
  return z.r;
}

inline double& complex::im()
{
  return i;
}

inline double& im(complex& z)
{
  return z.i;
}

// Returns absolute value of complex - not assignable
inline double complex::abs() const
{
  return sqrt(r*r+i*i);
}

inline double abs(const complex& z)
{
  return sqrt(z.r*z.r+z.i*z.i);
}

// Returns argument of complex - not assignable
inline double complex::arg() const
{
  return atan2(i,r);
}

inline double arg(const complex& z)
{
  return atan2(z.i,z.r);
}

// Negates a complex number
inline complex complex::operator-() const
{
  return complex(-r,-i);
}

// Complex-complex operators:

// Binary arithmetic operators are defined in terms of the unary ones
inline complex complex::operator+(const complex& z) const
{
  return complex(*this) += z;
}

inline complex complex::operator-(const complex& z) const
{
  return complex(*this) -= z;
}

inline complex complex::operator*(const complex& z) const
{
  return complex(*this) *= z;
}

inline complex complex::operator/(const complex& z) const
{
  return complex(*this) /= z;
}

inline complex& complex::operator=(const complex& z)
{
  r=z.r;
  i=z.i;

  return *this;
}

inline complex& complex::operator+=(const complex& z)
{
  r += z.r;
  i += z.i;

  return *this;
}

inline complex& complex::operator-=(const complex& z)
{
  r -= z.r;
  i -= z.i;

  return *this;
}

inline int complex::operator==(const complex& z) const
{
  return (r==z.r) && (i==z.i);
}

inline int complex::operator!=(const complex& z) const
{
  return (r!=z.r) || (i!=z.i);
}

// double-complex operators are defined by just swapping order
inline complex operator+(double d, const complex& z)
{
  return z+d;
}

inline complex operator-(double d, const complex& z)
{
  return complex(d)-z;
}

inline complex operator*(double d, const complex& z)
{
  return z*d;
}

inline complex operator/(double d,const complex& z)
{
  return complex(d)/=z;
}

inline int operator==(double d, const complex& z)
{
  return z==d;
}

inline int operator!=(double d, const complex& z)
{
  return z!=d;
}


// ***
// *** End of inlines ***
// ***

// ***
// *** complex-complex operators ***
// ***

complex& complex::operator*=(const complex& z)
{
  double magnitude = abs() * z.abs();
  double argument = arg() + z.arg();

  r=magnitude*cos(argument);
  i=magnitude*sin(argument);

  return *this;
}

inline complex& complex::operator/=(const complex& z)
{
  double magnitude = abs() / z.abs();
  double argument = arg() - z.arg();

  r=magnitude*cos(argument);
  i=magnitude*sin(argument);

  return *this;
}

// ***
// *** I/O operators ***
// ***

// User-defined input operator for complex type
// Takes 2 plain numbers
istream& operator>>(istream& s, complex& z)
{
  s >> z.r >> z.i;

  return s;
}

// User-defined output operator for complex type
ostream& operator<<(ostream& s, const complex& z)
{
  s << '(' << z.r << ',' << z.i << ')';
  
  return s;
}

// User-defined binary output operator for complex type
binofstream& operator<<(binofstream& s, const complex& z)
{
  s << z.r << z.i;

  return s;
}

// User-defined binary input operator for complex
binifstream& operator>>(binifstream& s, complex& z)
{
  s >> z.r >> z.i;

  return s;
}

#endif
