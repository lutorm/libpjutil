// message.h
// $Id$

// a class that makes it easy to turn on and off cout/cerr

#ifndef __message__
#define __message__

#include <iostream>

//using namespace std;

class smart_cout {
public:
  static bool enable;
public:
  template<class T>  smart_cout& operator<<(const T&);
  template<class T> const smart_cout& operator<<(const T&) const;
  smart_cout & operator<<(std::ostream & (*f) (std::ostream &))
    {f (std::cout); return *this;} 
  const smart_cout & operator<<(std::ostream & (*f) (std::ostream &)) const
    {f (std::cout); return *this;}
  smart_cout & operator<<(std::ios_base & (*f) (std::ios_base &))
    {f (std::cout); return *this;} 
  smart_cout & operator<<(std::ios & (*f) (std::ios &))
    {f (std::cout); return *this;} 
};

class smart_cerr {
public:
  static bool enable;
public:
  template<class T>  smart_cerr& operator<<(const T&);
  template<class T> const smart_cerr& operator<<(const T&) const;
  smart_cerr & operator<<(std::ostream & (*f) (std::ostream &))
    {f (std::cerr); return *this;} 
  const smart_cerr & operator<<(std::ostream & (*f) (std::ostream &)) const
    {f (std::cerr); return *this;}
  smart_cerr & operator<<(std::ios_base & (*f) (std::ios_base &))
    {f (std::cerr); return *this;} 
  smart_cerr & operator<<(std::ios & (*f) (std::ios &))
    {f (std::cerr); return *this;} 
};

class message {
protected:
  smart_cout cout;
  smart_cerr cerr;

public:
  void set_cout(bool);
  void set_cerr(bool);
  //message();
};

//message::message() : smart_cerr(),smart_cout() {}

template<class T>  smart_cout& smart_cout::operator<<(const T& v) 
{
  //cout << "SMART_COUT presents: ";
  if(enable)
    std::cout << v;
  return (*this);
}

template<class T> const smart_cout& smart_cout::operator<<(const T& v) const
{
  //cout << "SMART_COUT presents: ";
  if(enable)
    std::cout << v;
  return (*this);
}

template<class T> smart_cerr& smart_cerr::operator<<(const T& v) 
{
  //cout << "SMART_CERR presents: ";
  if(enable)
    std::cerr << v;
  return (*this);
}


template<class T> const smart_cerr& smart_cerr::operator<<(const T& v) const
{
  //cout << "SMART_CERR presents: ";
  if(enable)
    std::cerr << v;
  return (*this);
}

#endif
