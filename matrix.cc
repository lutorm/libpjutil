// matrix.cc
// Some non-template matrix routines
// $Id$
// $Log$

#include "matrix.h"

matrix<int> converttoint(const matrix<double>& m)
{
  matrix<int> c(m.rows,m.cols);

  if((!m.value)&&(!c.value)) {
    // If we're not allocated, then just rip through the three components
    c.arr[0]=int(m.arr[0]);
    c.arr[1]=int(m.arr[1]);
    c.arr[2]=int(m.arr[2]);
  }
  else {
    double* vm = m.val();
    int* vc = c.val();
    
    for(int i=0;i<m.rows*m.cols;i++)
      vc[i]=int(vm[i]);
  }
  
    return c;
}

