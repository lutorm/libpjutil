// solver2.cc
//
// Implementation of numerical equation solver routines
// Uses Newton-Raphson
//
// Patrik Jonsson
//
// $Id$

#include "solver2.h"
#include <cmath>
#include <iostream>
#include "blitz/numinquire.h"

using namespace std;

// Call to solve returns solution of f(x)=0. For an equation with multiple
// roots, which root is returned depends on the inital guess
double solver2::solve(double x0)
{
  const int maxsteps = 1000;

  double f;
  double x = x0;
  int Nstep = 0;

  while( (fabs(f=func(x))>accuracy) && (Nstep<maxsteps) ) {
    Nstep++;

    const double fp = der(x);
    if(fp==0) {
      if(verbose)
	cerr << "solver::Solve: Warning, extremum encountered\n";

      // return NaN to indicate that solution is crap
      x=blitz::quiet_NaN(double());
      break;
    }
     else
      x-= f/fp;

    if(fabs(x-x0) < xaccuracy)
      break;
    x0=x;
  }

  if(Nstep==maxsteps) {
    if(verbose)
      cerr << "solver::Solve: Warning, failed to converge\n";

    x=blitz::quiet_NaN(double());
  }

  //cout << Nstep << " steps\n";
  return x;
}
