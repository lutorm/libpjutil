// integrator2.cc
//
// Implementation of numerical integrator routines
// Based on fortran code my Mike Gross
// Uses fifth order Runge-Kutta with adaptive step size
//
// Patrik Jonsson
//
// $Id$
// $Log$
// Revision 1.3  2003/02/04 23:00:24  patrik
// Replaced an unnecessary division (but the class should be modified
// anyway...)
//
// Revision 1.2  1999/03/11 01:03:55  patrik
// Inserted default constructors and "setaccuracy" routines.
//
// Revision 1.1  1999/03/10 22:27:44  patrik
// Implemented integrator2, where the integrand is implemented in a derived
// class instead of taken as a pointer to a global function. (Like lsqfitter2.)
// Parameters to the integrand are no longer passed in the call to integrate but
// defined directly in the derived class. All in all, code is simpler now.
//

#include "integrator2.h"
#include <cmath>
#include <iostream>

using namespace std;

// Call to Integrate returns integral of function defined in constructor
// between a and b.
double integrator2::integrate(double a, double b)
{
  const int maxsteps = 100000000;

  double dx, dxnext;
  int Nstep;

  // Initialise members
  x = a;
  y = 0;
  dx = startstep;
  Nstep = 0;

  while( ( ((x-b)*(b-a))<0 ) && (Nstep<maxsteps)) {
    Nstep++;
    dydx = integrand(x);

    // yscale is used to monitor accuracy. This general purpose choice
    // can be modified if need be.

    yscale = ( (fabs(y)+fabs(dx*dydx))>1e-16 ? fabs(y)+fabs(dx*dydx) : 1e-16 );

    if( ((x+dx-b)*(x+dx-a)) > 0) // Stepsize overshoots, decrease it
      dx = b-x;

    dx = Makestep(dx);
  }

  if(Nstep==maxsteps)
    cout << "integrator::integrate: Warning, failed to converge\n";

  return y;
}

// 5th order Runge-Kutta step
double integrator2::Makestep(double htry)
{
  const double safety=0.9, pgrow=-0.2, pshrink=-0.25, errcon=1.89e-4;

  double errmax, h, yerr, ytemp;
  int underflow;

  h = htry; // Set stepsize to initial accuracy
  errmax = 10;
  underflow = 0;

  while( (errmax>1) && (!underflow) ) {
    ytemp = Runge5(h, yerr); // Take a step

    errmax = fabs(yerr/yscale)/accuracy; // Scale relative to required accuracy

    if(errmax>1) { // Truncation error too large, reduce h
      double htemp = safety*h*pow(errmax,pshrink);
      h = sign( (fabs(htemp)>0.1*fabs(h)) ? fabs(htemp) : 0.1*fabs(h) , h);
      double xnew = x+h;

      if( (xnew==x) && (!underflow) ) {
	underflow = 1;
	cout << "integrator::Makestep: Warning, stepsize underflow\n";
      }
    }
  }


  x+=h;
  y=ytemp;

  // Step succeeded. Estimate and return size of next step

  if(errmax>errcon)
    return safety*h*pow(errmax,pgrow);
  else
    return 5.0*h; // Not more than a factor of 5 increase
}


// Advance solution using 5th order C-K R-K
// Returns y-value and error in reference argument yerr
double integrator2::Runge5(double h, double& yerr)
{
  const double a3=0.3, a4=0.6, a5=1.0, a6=0.875;
  const double c1=37.0/378, c3=250.0/621, c4=125.0/594, c6=512.0/1771;
  const double dc1= c1-2825.0/27648, dc3= c3-18575.0/48384;
  const double dc4= c4-13525.0/55296, dc5=-277.0/14336;
  const double dc6= c6-0.25;

  double ak3,ak4,ak5,ak6;

  ak3 = integrand(x+a3*h);
  ak4 = integrand(x+a4*h);
  ak5 = integrand(x+a5*h);
  ak6 = integrand(x+a6*h);

  // Estimate error as difference between fourth and fifth order
  yerr = h*(dc1*dydx + dc3*ak3 + dc4*ak4 + dc5*ak5 + dc6*ak6);

  // Estimate fifth-order value
  return (y + h*(c1*dydx + c3*ak3 + c4*ak4 + c6*ak6) );
}

// f77 function sign
double integrator2::sign(double val, double sgn)
{
  const double s = (sgn > 0?  1: (sgn < 0?  -1: 0));// sgn/fabs(sgn);
  return fabs(val)*s;
}
