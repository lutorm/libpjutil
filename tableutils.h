// tableutils.h
// Utility routines for sorting/finding items in tables
// A table is an array of numbers (class T) that can be sorted and indexed into
//
// $Id$

#ifndef __tableutils__
#define __tableutils__

#include "matrix.h"
#include <cmath>
#include <iostream>

template<class T> class table;

template<class T>
std::ostream& operator<<(std::ostream&, const table<T>&);
template<class T>
std::istream& operator>>(std::istream&, table<T>&);
template<class T>
binfstream& operator<<(binfstream&, const table<T>&);
template<class T>
binfstream& operator>>(binfstream&, table<T>&);


template<class T>
class table{
private:
  T* arr;
  int len;
  //  int ordered;
  void rangecheck(int) const;
  int brentlocate(T,T,T) const;

  // exception class for out-of-bonds
  class index_error {
  public:
    int i;
    index_error(int a) {i=a;};
  }; 

public:
  table();
  table(int);
  table(T*,int);
  table(const table&);
  table(const matrix<T>&);
  ~table();

  int n() const;
  T& operator[](int); 
  const T& operator[](int) const; 
  table& operator=(const table&);
  table& operator=(T);
  table& operator=(const matrix<T>&); // Assignment from a matrix
  matrix<T> makematrix() const;
  int locate(T) const;
  int locate2(T) const;
  int locate(T,int) const;
  int locate2(T,int) const;

  // I/O routines
  friend std::ostream& operator<< <T>(std::ostream&, const table&);
  friend std::istream& operator>> <T>(std::istream&, table&);

  friend binfstream& operator<< <T>(binfstream&, const table&);
  friend binfstream& operator>> <T>(binfstream&, table&);

  void quicksort();  // Perform a quicksort on the table
};


// Initializes empty table
template<class T> inline table<T>::table()
{
  len = 0;
  arr = 0;
}

// Constructor for n values
template<class T> inline table<T>::table(int n)
{
  len = n;
  arr = new T[len];
}

// Constructor for n values copied from the array a
template<class T> table<T>::table(T* a, int n)
{
  len = n;
  arr = new T[len];

  int i=len;
  while(i--)
    arr[i]=a[i];
}

// Copy constructor
template<class T> table<T>::table(const table& t)
{
  len=t.len;
  arr=new T[len];

  int i=len;
  if(t.arr)   // Only copy data if it is allocated
    while(i--)
      arr[i]=t.arr[i];
}

// Copy constructor from a matrix<t>
template<class T> table<T>::table(const matrix<T>& m)
{
  if(m.cols>1) {
    std::cerr << "table::table(matrix): Can't initialize from 2d matrix\n";
    len=0;
    arr=0;
  }
  else {
    len=m.rows;
    arr=new T[len];
    T* vm=m.val();
    
    // Copy data
      int i=len;
    while(i--)
      arr[i]=vm[i];
  }
}

template<class T> inline table<T>::~table()
{
  if(arr)
    delete[] arr;
}

// Checks if the index is within range and throws an exception if it isn't
template<class T> inline void table<T>::rangecheck(int i) const
{
#ifndef TABLE_NODEBUG
  if( (i<0) || (i>=len) )
    throw index_error(i);
#endif
}

template<class T> inline int table<T>::n() const
{
  return len;
}

// Index operator can be used to both read and write values
template<class T> inline T& table<T>::operator[](int i)
{
  rangecheck(i);

//  if((i>=0)&&(i<len))
    return arr[i];
/*
  else {
    std::cerr << "table::operator[]: Subscript out of range: " << i << '\n';
    // We have to return something, choose the closest value
    if(i<0)
      return arr[0];
    else
      return arr[len-1];
  }
*/
}

// But this one is used for const objects so it can only read
template<class T> inline const T& table<T>::operator[](int i) const
{
  rangecheck(i);

  //if((i>=0)&&(i<len))
    return arr[i];
/*
  else {
    std::cerr << "table::operator[]: Subscript out of range: " << i << '\n';
    // We have to return something, choose the closest value
    if(i<0)
      return arr[0];
    else
      return arr[len-1];
  }
*/
}

// Assignment operator
template<class T> table<T>& table<T>::operator=(const table& t)
{
  // Check if it's already allocated
  if(len!=t.len ) {
    delete[] arr;
    len=t.len;
    arr=new T[len];
  }

  int i=len;
  //if(t.arr) not necessary
  while(i--)
    arr[i]=t.arr[i];

  return (*this);
} 

// Assignment operator from an element
template<class T> table<T>& table<T>::operator=(T d)
{
 // if(!arr) not necessary
 //   arr=new T[len];

  int i=len;
  while(i--)
    arr[i]=d;

  return (*this);
} 

// Assignment operator from a matrix
// This function is a friend of the matrix class
template<class T> table<T>& table<T>::operator=(const matrix<T>& m)
{
  // First check that matrix is 1d
  if(m.cols>1) {
    std::cerr << "table::operator=(matrix): Trying to assign a 2d matrix to a table!\n";
    return *this;
  }

  // Check if we need to reallocate
  if(arr && (len!=m.rows)) {
    delete[] arr;
    arr=0;
  }

  len=m.rows;
  arr=new T[len];
  T* vm=m.val();

  int i=len;
  while(i--)
    arr[i]=vm[i];

  return (*this);
} 

// This function creates a 1d matrix from the table. It is a friend
// of the matrix class
template<class T> matrix<T> table<T>::makematrix() const
{
  matrix<T> m(len,1);
  T* vm=m.val();
  int i=len;

  while(i--)
    vm[i]=arr[i];
  return m;
}

// Returns the index i such that v is in [ arr[i],arr[i+1] ].
// Array must be ordered in either ascending or descending order
// Does bisection search (it's pretty slowly converging, but since our
// function evaluation is just a table lookup it still seems to be faster than
// the more complex methods
template<class T> inline int table<T>::locate(T v) const
{
  const int ascend=( arr[len-1] >=arr[0] ); // Check whether it goes up or down

#ifndef TABLE_NODEBUG
  // Check for values out of range
  if(ascend) {
    if((v<arr[0])||(v>arr[len-1])) {
      std::cerr << "table::locate: sought value is out of range\n";
      return -1;
    }
  }
  else
    if((v>arr[0])||(v<arr[len-1])) {
      std::cerr << "table::locate: sought value is out of range\n";
      return -1;
    }
#endif

  int jl=0,ju=len-1; // Initialize upper/lower limits

  while( (ju-jl) > 1 ) {
    int jm=(ju+jl)>>1; // Compute midpoint
    if( v >= arr[jm] == ascend ) // Is v above or below midpoint?
      jl=jm;
    else
      ju=jm;
  }

  return jl;
}

// This function is different from the above one because if the table is
// not full you need to specify the number of entries (assumed to be in the
// lower entries).
template<class T> inline int table<T>::locate(T v, int num) const
{
  const int ascend=( arr[num-1] >=arr[0] ); // Check whether it goes up or down

#ifndef TABLE_NODEBUG
  // Check for values out of range
  if(ascend) {
    if((v<arr[0])||(v>arr[num-1])) {
      std::cerr << "table::locate: sought value is out of range\n";
      return -1;
    }
  }
  else
    if((v>arr[0])||(v<arr[num-1])) {
      std::cerr << "table::locate: sought value is out of range\n";
      return -1;
    }
#endif

  int jl=0,ju=num-1; // Initialize upper/lower limits

  while( (ju-jl) > 1 ) {
    int jm=(ju+jl)>>1; // Compute midpoint

    if( v >= arr[jm] == ascend ) // Is v above or below midpoint?
      jl=jm;
    else
      ju=jm;
  }
    
  return jl;
}

// I/O routines

template<class T> std::istream& operator>>(std::istream& s, table<T>& m)
{
  T* vm = m.arr;

  for(int i=0; i<m.len; i++)
    s >> vm[i];

  return s;
}

template<class T> std::ostream& operator<<(std::ostream& s, const table<T>& m)
{
  T* vm = m.arr;

  s << "( ";
  for(int r=0;r<m.len;r++) {
    s << vm[r];
    if(r<m.len-1)
      s << ", ";
  }
  s << " )";

  return s;
}

// User-defined binary output operator for matrix type
template<class T> binfstream& operator<<(binfstream& s, const table<T>& m)
{
  T* vm = m.arr;

  for(int i=0;i<m.len;i++)
    s << vm[i];
  return s;
}

// User-defined binary input operator for matrix type
template<class T> binfstream& operator>>(binfstream& s, table<T>& m)
{
  T* vm = m.arr;

  for(int i=0;i<m.len;i++)
    s >> vm[i];
  return s;
}


// Sorts the table using the QuickSort from Numerical Recipes
// Not working
template<class T> void table<T>::quicksort()
{
  const int M=7;   // Final insertion sort size
  const int NSTACK=50;

  unsigned long i,ir=len,j,k,l=1;
  unsigned long* istack = new unsigned long[NSTACK];
  T a,temp;

  for(;;) {
    if(ir-l<M) {
      for(j=l+1;j<=ir;j++) {
	a=arr[j];
	for(i=j-1;i>=l;i--) {
	  if(arr[i]<=a) break;
	  arr[i+1]=arr[i];

	}}}}
  delete[] istack;
}

#define ITMAX 100
#define EPS 3.0e-8

template<class T> inline int table<T>::locate2(T v) const
{
  int i=brentlocate(v,0,len-1);
/*
  int j=locate2(v);
  if(i==j) {
  //if((arr[i]<v)&&(arr[i+1]>v)) {
    //std::cerr << "brentlocate succeeded " << i << ' ' << v << ' ' << arr[i] << ' ' << arr[i+1] << '\n';
*/
    return i;
/*
  }
  else {
    std::cerr << "brentlocate fucked up " << i << ' ' << v << ' ' << arr[i] << ' ' << arr[i+1] << '\n';
    std::cerr << "falling back on bisection\n";
    return j;
  }
*/
}

template<class T> inline int table<T>::locate2(T v,int n) const
{
  int i=  brentlocate(v,0,n-1);
/*  int j=locate2(v,n);
  if(i==j) {
  //if(arr[i]<v)&&(arr[i+1]>v)) {
    //std::cerr << "brentlocate succeeded " << i << ' ' << v << ' ' << arr[i] << ' ' << arr[i+1] << '\n';
*/
    return i;
/*
  }
  else {
    std::cerr << "brentlocate fucked up " << i << ' ' << v << ' ' << arr[i] << ' ' << arr[i+1] << '\n';
    std::cerr << "falling back on bisection\n";
    return j;
  }
*/
}

// Finds the root using Brent's method
// unfortunately it's too complex, so it takes way longer than bisection
// (for most tables, at least)
template<class T> int table<T>::brentlocate(T v, T x1, T x2) const
{
  T tol=0.5;  
  int iter;
  T a=x1,b=x2,c=b,d,e,min1,min2;
  T fa=(*this)[(int)a]-v,fb=(*this)[(int)b]-v,fc,p,q,r,s,tol1,xm;
  
  if ((fa > 0.0 && fb > 0.0) || (fa < 0.0 && fb < 0.0)) {
    std::cerr << v << " not in table or not sorted!\n";
    return 0;
  }

  fc=fb;
  for (iter=1;iter<=ITMAX;iter++) {
    //cout << fa << ' ' << fb << ' ' << fc << '\n';
    //cout << a << ' ' << b << ' ' << c << "\n\n";

    // The first thing that happens is that we arrange so the root is bracketed
    // by a and c and b is the best guess
    // but we don't know if a<c or v.v
    if ((fb > 0.0 && fc > 0.0) || (fb < 0.0 && fc < 0.0)) {
      c=a;
      fc=fa;
      e=d=b-a;
    }
    if (fabs(fc) < fabs(fb)) {
      a=b;
      b=c;
      c=a;
      fa=fb;
      fb=fc;
      fc=fa;
    }
    tol1=2.0*EPS*fabs(b)+0.5*tol;
    xm=0.5*(c-b);
    // i don't think this is a good convergence criterion
    //if (fabs(xm) <= tol1 || fb == 0.0) {
    if((fabs(a-b)==1)&&(fa*fb<0)) {
      //cout << "convergence\na="<< a << " b=" << b << " c=" << c << " fa=" << fa << " fb=" << fb << " fc=" << fc << '\n';
      // OK we've converged but we need to return the smallest element
      return (arr[(int)b]>v?(int)a:(int)b);
    }
    if((fabs(c-b)==1)&&(fc*fb<0)) {
      //cout << "convergence\na="<< a << " b=" << b << " c=" << c << " fa=" << fa << " fb=" << fb << " fc=" << fc << '\n';
      // OK we've converged but we need to return the smallest element
      return (arr[(int)b]>v?(int)c:(int)b);
    }
    if (fabs(e) >= tol1 && fabs(fa) > fabs(fb)) {
      // attempt interpolation
      s=fb/fa;
      if (a == c) {
	p=2.0*xm*s;
	q=1.0-s;
      } else {
	q=fa/fc;
	r=fb/fc;
	p=s*(2.0*xm*q*(q-r)-(b-a)*(r-1.0));
	q=(q-1.0)*(r-1.0)*(s-1.0);
      }
      if (p > 0.0) q = -q;
      p=fabs(p);
      min1=3.0*xm*q-fabs(tol1*q);
      min2=fabs(e*q);
      if (2.0*p < (min1 < min2 ? min1 : min2)) {
	e=d;
	d=p/q;
      }
      else {
	d=xm;
	e=d;
      }
    }
    else {
      d=xm;
      e=d;
    }
    a=b;
    fa=fb;
    if (fabs(d) > tol1)
      b += d;
    else
      b += (xm>=0.0 ? fabs(tol1) : -fabs(tol1));

    if(b>a)
      // b increased - so presumably c>a 
      if(int(a)==int(b))
	b=int(a+1);
      else
	b=int(b);
    else
      // b decreased
      if(int(a)==int(b))
	b=int(a-1);
      else
	b=int(b);

    fb=(*this)[(int)b]-v;
  }
  std::cerr <<"Iterated to hell\n";
  throw index_error(v);
  return 0;
}

#endif
