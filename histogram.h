// histogram.h
//
// Class for making a histogram of data
//
// Patrik Jonsson
//
// $Id$
// $Log$
// Revision 1.3  1999/11/20 03:29:06  patrik
// The problem was that for stathistograms the arrays were allocated twice and
// removed twice. Both histograms now use "stdevindex" to index the arrays
// instead, so inheritance works a little better.
//
// Revision 1.2  1999/11/19 23:24:16  patrik
// Added a function to retrieve the statistics of individual bins from the
// stathistogram. I think there is still some memory problem because the
// class causes segfaults, I'll keep looking.
//
// Revision 1.1  1999/04/17 08:17:06  patrik
// Initial revision of the histogram classes.
//

#ifndef __histogram__
#define __histogram__

#include "matrix.h"


// Used for returning bin values
class bin {
public:
  matrix<double> coords;
  double value;
};

// Used for returning bin values
class statbin {
public:
  matrix<double> coords;
  double mean;
  double median;
  double stdev;
  double n;
};


class histogram {
protected:
  int ndim;
  double* lower;
  double* upper;
  int* nbins;
  double* binsize;

  int nstdevtot;

  double* data;

  // Indexing routines
  //int index(int*);

  // Indexing routines
  int stdevindex(int*);
  
  histogram(); // This constructor is used by the stathistogram constr
public:
  histogram(int ...);
  ~histogram();

  // Add a point to the histogram
  void addpoint(double ...);
  // Add a specified value to the histogram
  void addvalue(double, double ...);
  // Get the bin value of the indexed bin
  bin getbin(int ...);

  // Write out the histogram data
  void write(char*);
};


class stathistogram : public histogram {
private:
  // Shit for standard deviation/average analysis
  matrix<double>* sum;
  matrix<double>* sumsq;


public:
  stathistogram(int ...);
  ~stathistogram();

  // Add a point to the histogram
  void addpoint(double ...);
  // Add a specified value to the histogram
  void addvalue(double, double ...);

  // Write out the histogram data
  void write(char*);

  // Print average and standard deviation for dimension i
  void writestats(int,char* =0);
  // Get the statistics for the i dim of the indexed bin
  statbin getstats(int ...);
};

inline int histogram::stdevindex(int* ji)
{
  int j=ji[0]+1;
  int m=1;

  for(int i=1;i<ndim;i++) {
    m*= nbins[i-1]+2;
    j+= m * (ji[i]+1);
  }
  return j;
}


#endif
