// preferences.h
// a class for keeping track of preferences keywords and values
// $Id$

// The  template members are explicitly instantiated in
// preferences.cc, to avoid having to include the entire CCfits mess.
// This works because the Preferences class can only have keywords of
// certain specified types.

#ifndef __preferences__
#define __preferences__

#include <map> 
#include<string> 
#include<iosfwd> 

namespace CCfits {
  class HDU;
}

class prefs;

class Preferences {
protected:
  std::map<std::string,prefs*> keywords;
  
  bool comment_character (char);
  void setValue(const std::string& kw, prefs*);
  const prefs* find (const std::string &) const;

public:
  class unknown_keyword{};
  class type_mismatch{};

  Preferences() {};
  ~Preferences();
  Preferences (const Preferences &);
  
  bool readfile(const std::string&);
  bool read(std::istream&);

  std::istream& readline_keywordfirst(std::istream&);
  std::istream& readline_keywordlast(std::istream&);

  // This function retrieves a keyword value by doing the dynamic_cast
  // and checking for errors. it also does int->double conversion if
  // necessary. The valueType is only for template resolution, unless
  // use_default is true in which case a missing keyword is returned
  // as default value
  template<class valueType>
  valueType getValue(const std::string& kw, const valueType&, 
		     bool use_default=false) const;
  
  template<class valueType>
    void setValue(const std::string& kw, const valueType& val,
		  const std::string& cmt = "");

  bool deleteValue (const std::string&);
  // copies keyword from one preferences object to another 
  bool copyKeyword (const Preferences &, const std::string &);
  int size () const {return keywords.size();};
  bool defined(const std::string& kw) const;
  void print() const;
  void print_unused() const;
  void mark_used(const std::string& kw) const;

  // Writes keywords to a CCfits HDU
  bool write_fits (CCfits::HDU &) const;
};


inline const prefs* Preferences::find (const std::string & kw) const 
{
  std::map<std::string,prefs*>::const_iterator i = keywords.find(kw);
  const prefs* p;
  if (i == keywords.end())
    p = 0;
  else
    p = i->second;
  
  return p;
}

#endif
