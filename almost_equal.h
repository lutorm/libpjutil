#ifndef __almost_equal__
#define __almost_equal__

#include <blitz/array.h>

/** AlmostEqual function to compare if two floating-points are within
    maxUlps of each other.  Adopted from
    http://www.cygnus-software.com/papers/comparingfloats/comparingfloats.htm.
 */
inline bool almostEqual(float a, float b, int32_t maxUlps)
{
    // Make sure maxUlps is non-negative and small enough that the
    // default NAN won't compare as equal to anything.
    assert(maxUlps > 0 && maxUlps < 4 * 1024 * 1024);
    int32_t aInt = *(int32_t*)&a;
    // Make aInt lexicographically ordered as a twos-complement int
    if (aInt < 0)
        aInt = 0x80000000 - aInt;
    // Make bInt lexicographically ordered as a twos-complement int
    int32_t bInt = *(uint32_t*)&b;
    if (bInt < 0)
        bInt = 0x80000000 - bInt;
    int32_t intDiff = abs(aInt - bInt);
    if (intDiff <= maxUlps)
        return true;
    return false;
}

/** Version without maxUlps argument uses 100. */
inline bool almostEqual(float a, float b)
{
  return almostEqual(a, b, int32_t(100));
}

inline bool almostEqual(double a, double b, int64_t maxUlps)
{
    // Make sure maxUlps is non-negative and small enough that the
    // default NAN won't compare as equal to anything.
    assert(maxUlps > 0 && maxUlps < 4 * 1024 * 1024);
    int64_t aInt = *(int64_t*)&a;
    // Make aInt lexicographically ordered as a twos-complement int
    if (aInt < 0)
        aInt = 0x8000000000000000 - aInt;
    // Make bInt lexicographically ordered as a twos-complement int
    int64_t bInt = *(uint64_t*)&b;
    if (bInt < 0)
        bInt = 0x8000000000000000 - bInt;
    int64_t intDiff = abs(aInt - bInt);
    if (intDiff <= maxUlps)
        return true;
    return false;
}

inline bool almostEqual(double a, double b)
{
  return almostEqual(a, b, int64_t(100));
}

BZ_DECLARE_FUNCTION2(almostEqual);

#endif
