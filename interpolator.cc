// interpolator.cc
//
// Implementations of interpolation routines
// classes linear_interpolator and cspline_interpolator 
//
// Patrik Jonsson 980415
//

#include "interpolator.h"
#include <fstream>

using namespace std;

// Reads interpolation data from file, on format n of points x0 y0 x1 y1 ...
interpolator::interpolator(char* fn)
{
  points_n=0;

  ifstream* infile = new ifstream(fn);
  if(infile->good())
    (*infile) >> points_n;

  initialise();

  for(int i=0;i<points_n;i++) {
    double x;
    (*infile) >> x;
    (*points_x)[i]=x;
    (*infile) >> points_y[i];
  }

  delete infile;
}


// allocates and initialised arrays
void interpolator::initialise()
{
  //  points_x = new double[points_n];
  points_x = new table<double>(points_n);
  points_y = new double[points_n];

  for(int i=0;i<points_n;i++) {
    (*points_x)[i] = 0;
    points_y[i] = 0;
  }
}


// Set's the i'th data point to x,y
int interpolator::setPoint(int i, double x, double y)
{
  //cerr << "setpoint " << i << ' ' << x << ' ' << y << '\n';
  if ( (i>=0) || (i<points_n) ) {
    (*points_x)[i] = x;
    points_y[i] = y;
    return 1;
  }
  else {
    // no such point - return false
    cerr << "interpolator::setPoint(): no such point\n";
    return 0;
  }
}


// do a linear interpolation
double linear_interpolator::interpolate(double x)
{
  //cerr << "interpolation at x=" << x << '\n';
  //cerr << "limits are " << points_x[0] << ' ' << points_x[points_n-1] << '\n';
  // check that we don't want extrapolation
  if( (x<(*points_x)[0]) || (x>(*points_x)[points_n-1]) ) {
    cerr << "linear_interpolator::interpolate(): can't extrapolate\n";
    return 0;
  }

  // find out between which data points x is INA BETTER WAY THAN THIS
  //int i=0;
  //while(points_x[++i]<x);
  // x is between i-1 and i
  int i=points_x->locate(x);
  // x is between i and i+1

  double delta_x = (*points_x)[i+1] - (*points_x)[i];
  double dx      = x           - (*points_x)[i];
  double delta_y = points_y[i+1] - points_y[i];

  double y = dx/delta_x*delta_y+points_y[i];

  return y;
}


// Set's the i'th data point to x,y
int cspline_interpolator::setPoint(int i, double x, double y)
{
  interpolator::setPoint(i,x,y);

  C_setup = 0;

  return 0;
}

int cspline_interpolator::setBC(int ltype, double lpar,
				   int htype ,double hpar)
{
  bc_lotype = ltype;
  bc_loval = lpar;
  bc_hitype = htype;
  bc_hival = hpar;

  C_setup = 0;

  return 0;
}

/*
void cspline_interpolator::setup()
{

void CUBSPL(Tau, C, N, Ibcbeg, Ibcend)
double *Tau, C[][100];
int    N, Ibcbeg, Ibcend;

    double DIVDF1, DIVDF3, Dtau, G;
    int    I, J, L, M;

    L = N - 1;
    for (M=1; M<N; M++) {
      C[2][M] = Tau[M] - Tau[M-1];
L10:   
      C[3][M] = (C[0][M]-C[0][M-1])/C[2][M];
    }

    if ((Ibcbeg-1) == -1) goto L11;
    if ((Ibcbeg-1) ==  0) goto L15;
    if ((Ibcbeg-1) ==  1) goto L16;
L11: 
    if (N > 2) goto L12;
    C[3][0] = 1.0;
    C[2][0] = 1.0;
    C[1][0] = 2.0*C[3][1];
    goto L25;

L12: 
    C[3][0] = C[2][2];
    C[2][0] = C[2][1]+C[2][2];
    C[1][0] = ((C[2][1]+2.0*C[2][0])*C[3][1]*C[2][2]
              + C[2][1]*C[2][1]*C[3][2])/C[2][0];
    goto L19;
L15: 
    C[3][0] = 1.0;
    C[2][0] = 0.0;
    goto L18;
L16: 
    C[3][0] = 2.0;
    C[2][0] = 1.0;
    C[1][0] = 3.0*C[3][1]-C[2][1]/2.0*C[1][0];
L18: 
    if (N == 2) goto L25;
L19: 
    for (M=1; M<L; M++) {    // For M:=1 to L do begin
      G = -C[2][M+1]/C[3][M-1];
      C[1][M] = G*C[1][M-1]+3.0*(C[2][M]*C[3][M+1]+C[2][M+1]*C[3][M]);
      C[3][M] = G*C[2][M-1]+2.0*(C[2][M]+C[2][M+1]);
    } //  (* For M do begin *) 
    if ((Ibcend-1) == -1) goto L21;
    if ((Ibcend-1) == 0)  goto L30;
    if ((Ibcend-1) == 1)  goto L24;
L21: 
    if ((N==3) && (Ibcbeg==0)) goto L22;
    G = C[2][N-1-1]+C[2][N-1];
    C[1][N-1] = ((C[2][N-1]+2.0*G)*C[3][N-1]*C[2][N-1-1]+
               C[2][N-1]*C[2][N-1]*(C[0][N-1-1]-C[0][N-2-1])/C[2][N-1-1])/G;
    G = -G/C[3][N-1-1];
    C[3][N-1] = C[2][N-1-1];
    goto L29;
L22: 
    C[1][N-1] = 2.0*C[3][N-1];
    C[3][N-1] = 1.0;
    goto L28;
L24: 
    C[1][N-1] = 3.0*C[3][N-1]+C[2][N-1]/2.0*C[1][N-1];
    C[3][N-1] = 2.0;
    goto L28;
L25: 
    if ((Ibcend-1) == -1) goto L26;
    if ((Ibcend-1) ==  0) goto L30;
    if ((Ibcend-1) ==  1) goto L24;
L26: 
    if (Ibcbeg>0) goto L22;
    C[1][N-1] = C[3][N-1];
    goto L30;
L28: 
    G = -1.0/C[3][N-1-1];
L29: 
    C[3][N-1] = G*C[2][N-1-1]+C[3][N-1];
    C[1][N-1] = (G*C[1][N-1-1]+C[1][N-1])/C[3][N-1];
L30: 
    for (J=L-1; J>=0; J--) { // For J:=L downto 1 do begin 
      C[1][J] = (C[1][J]-C[2][J]*C[1][J+1])/C[3][J];
    } //  (* For J do begin *) 
    for (I=1; I<N; I++) { // For I:=2 to N do begin 
      Dtau  =  C[2][I];
      DIVDF1 = (C[0][I]-C[0][I-1])/Dtau;
      DIVDF3 = C[1][I-1]+C[1][I]-2.0*DIVDF1;
      C[2][I-1] = 2.0*(DIVDF1-C[1][I-1]-DIVDF3)/Dtau;
      C[3][I-1] = (DIVDF3/Dtau)*(6.0/Dtau);
    } // end; (* For I do begin *) 

    return;
}


double cspline_interpolator::interpolate(double x)
{
void INTERP(X_array, C, Nx, X, Y)
double *X_array, C[][100];
int    Nx;
double *X, *Y;

    double h;
    register int i, j;

    for (i=0; i<Nx; i++) {             // For i:=1 to Nx do begin 
      if ((*X < X_array[i]) || (*X == X_array[i])) {
        if (i == 0) {                  // If i=1 then begin 
          if (*X == X_array[i]) {
            *Y  =  C[0][0];
            goto L31;
	  } else {
            h  =  *X - X_array[i];
            *Y  =  C[0][i]+h*(C[1][i]+h*(C[2][i]+h*C[3][i]/3.0)/2.0);
            goto L31;
          }
	} else {
          j  =  i-1;
          h  =  *X - X_array[j];
          *Y  =  C[0][j]+h*(C[1][j]+h*(C[2][j]+h*C[3][j]/3.0)/2.0);
          goto L31;
	}
      }
    }

    j = Nx - 2;        // j := Nx-1;
    h  =  *X - X_array[j];
    *Y  =  C[0][j]+h*(C[1][j]+h*(C[2][j]+h*C[3][j]/3.0)/2.0); 
L31:
    return;
}
*/
