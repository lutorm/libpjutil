// histogram.cc
//
// Class for making a histogram of data
//
// Patrik Jonsson
//
// $Id$
// $Log$
// Revision 1.9  1999/12/17 00:23:40  patrik
// Fixed the calculation of standard deviation so we divide by n-1 and not n.
//
// Revision 1.8  1999/11/30 03:34:36  patrik
// Now sets stdev to HUGE_VAL if there are no data points.
//
// Revision 1.7  1999/11/21 04:42:30  patrik
// Fixed the stathistogram routines so they work for 1-dimensional histograms
// as well.
//
// Revision 1.6  1999/11/20 03:29:06  patrik
// The problem was that for stathistograms the arrays were allocated twice and
// removed twice. Both histograms now use "stdevindex" to index the arrays
// instead, so inheritance works a little better.
//
// Revision 1.5  1999/11/19 23:24:16  patrik
// Added a function to retrieve the statistics of individual bins from the
// stathistogram. I think there is still some memory problem because the
// class causes segfaults, I'll keep looking.
//
// Revision 1.4  1999/07/20 23:06:49  patrik
// Replaced array declarations with proper new's for cxx.
//
// Revision 1.3  1999/04/28 20:49:00  patrik
// Fixed compilation probs on mips-CC.
//
// Revision 1.2  1999/04/21 19:45:38  patrik
// The histogram class was split into a basic histogram class and a stathistogram
// class that doesn the rms and average calculations, since these represent a
// significant performance penalty.
//
// Revision 1.1  1999/04/17 08:17:06  patrik
// Initial revision of the histogram classes.
//

#include "histogram.h"
#include <stdarg.h>
#include <fstream>
#include <cmath>

using std::ofstream;

// Constructor makes an ndim histogram
// Arguments are number of dimensions and following the lower and upper limits
// and number of bins in each dimension
histogram::histogram(int n ...)
{
  ndim=n;
  lower = new double[ndim];
  upper = new double[ndim];
  nbins = new int[ndim];
  binsize = new double[ndim];
  
  va_list args;
  va_start(args,n);

  int ntot=1;
  nstdevtot=1;

  for(int i=0;i<ndim;i++) {
    lower[i] = va_arg(args,double);
    upper[i] = va_arg(args,double);
    nbins[i] = va_arg(args,int);

    binsize[i] = (upper[i]-lower[i])/nbins[i];

    ntot*=nbins[i];
    nstdevtot*=nbins[i]+2; // We keep track of stuff outside limits as well
  }

  data= new double[nstdevtot];

  // Don't forget to zero the data array
  for(int i=0;i<nstdevtot;i++)
    data[i]=0;

}

histogram::histogram()
{
}

// This constructor must do all initialization for the histogram constructor
// as well since I don't know how to propagate the argument stack
stathistogram::stathistogram(int n ...) : histogram()
{
  ndim=n;
  lower = new double[ndim];
  upper = new double[ndim];
  nbins = new int[ndim];
  binsize = new double[ndim];
  
  va_list args;
  va_start(args,n);

  int ntot=1;
  nstdevtot=1;

  for(int i=0;i<ndim;i++) {
    lower[i] = va_arg(args,double);
    upper[i] = va_arg(args,double);
    nbins[i] = va_arg(args,int);

    binsize[i] = (upper[i]-lower[i])/nbins[i];

    ntot*=nbins[i];
    nstdevtot*=nbins[i]+2; // We keep track of stuff outside limits as well
  }

  data= new double[nstdevtot];

  // Don't forget to zero the data array
  for(int i=0;i<nstdevtot;i++)
    data[i]=0;


  // Allocate stats arrays
  sum = new matrix<double>[nstdevtot];
  sumsq = new matrix<double>[nstdevtot];

  for(int i=0;i<nstdevtot;i++) {
    sum[i]=matrix<double>(ndim,1,0.);
    sumsq[i]=matrix<double>(ndim,1,0.);
  }
}

histogram::~histogram()
{
  delete[] lower;
  delete[] upper;
  delete[] nbins;
  delete[] binsize;
  delete[] data;
}

stathistogram::~stathistogram()
{
  delete[] sum;
  delete[] sumsq;
}

/*
// Calculates the index of the entry with components in j
int histogram::index(int* ji)
{
  int j=ji[0];
  int m=1;

  for(int i=1;i<ndim;i++) {
    m*= nbins[i-1];
    j+= m * ji[i];
  }
  return j;
}
*/

// Calculates the index into the stdev arrays of the entry
// with components in j. component=-1 means below lower limit, and component
// =nbins[i] means above higher limit
// so a normal range of 0-nbins is the usual limits
/*
int histogram::stdevindex(int* ji)
{
  int j=ji[0]+1;
  int m=1;

  for(int i=1;i<ndim;i++) {
    m*= nbins[i-1]+2;
    j+= m * (ji[i]+1);
  }
  return j;
}
*/

// Adds a point to the histogram
void histogram::addpoint(double x ...)
{
  //int bin[ndim];
  int* bin = new int[ndim];

  int fail=0;

  // Extract input data
  va_list args;
  va_start(args,x);
  matrix<double> xx(ndim,1,x);
  for(int i=1;i<ndim;i++)
    xx[i] = va_arg(args,double);

  // Find first bin
  // (WHYCANT THIS BE IN THE LOOP?)
  bin[0]=int(floor((xx[0]-lower[0])/binsize[0]));
    // Take care of out-of-bonds points for stdev
    if(bin[0]<0)
      bin[0]=-1;
    if(bin[0]>=nbins[0])
      bin[0]=nbins[0];
  // Check if it falls outside
    fail=(bin[0]<0)||(bin[0]>=nbins[0]);

  // Find the rest of the bins of this point
  for(int i=1;i<ndim;i++) {
    bin[i]=int(floor((xx[i]-lower[i])/binsize[i]));
    fail = fail || (bin[i]<0) || (bin[i]>=nbins[i]);
  }

  if(!fail) {
    // Add that bin
    int ii = stdevindex(bin);
    data[ii]++;
  }

  delete[] bin;
}

// Adds the value v to a point in the histogram
void histogram::addvalue(double v, double x ...)
{
  int* bin = new int[ndim];
  //int bin[ndim];
  int fail=0;

  // Extract input data
  va_list args;
  va_start(args,x);
  matrix<double> xx(ndim,1,x);
  for(int i=1;i<ndim;i++)
    xx[i] = va_arg(args,double);

  // Find first bin
  bin[0]=int(floor((xx[0]-lower[0])/binsize[0]));
    // Take care of out-of-bonds points for stdev
    if(bin[0]<0)
      bin[0]=-1;
    if(bin[0]>=nbins[0])
      bin[0]=nbins[0];
  // Check if it falls outside
  fail=(bin[0]<0)||(bin[0]>=nbins[0]);

  // Find the rest of the bins of this point
  for(int i=1;i<ndim;i++) {
    bin[i]=int(floor((xx[i]-lower[i])/binsize[i]));
    fail = fail || (bin[i]<0) || (bin[i]>=nbins[i]);
  }

  if(!fail) {
    // Add that bin
    int ii = stdevindex(bin);
    data[ii]+=v;
  }

  delete[] bin;
}


// Writes histogram data to a file.
void histogram::write(char* filename)
{
  ofstream of(filename);

  int* bin = new int[ndim];
  //int bin[ndim];

  int loop=1;
  for(int i=0;i<ndim;i++)
    bin[i]=0;

  // Loop to exhaust all the dimensions
  while(bin[ndim-1]<nbins[ndim-1]) {

    // Write coordinates
    for(int i=0;i<ndim;i++)
      of << binsize[i]*(bin[i]+0.5)+lower[i] << '\t';

    // Write value
    of << data[stdevindex(bin)] << '\n';

    // Increase bin number
    bin[0]++;
    for(int i=0;i<ndim-1;i++)
      if(bin[i]>=nbins[i]) {
	bin[i]=0;
	bin[i+1]++;
      }
  }

  delete[] bin;
}

// Adds a point to the histogram
void stathistogram::addpoint(double x ...)
{
  int* bin = new int[ndim];
  //int bin[ndim];
  //int fail=0;
  
  matrix<double> xx(ndim,1,x);
  matrix<double> xsq(ndim,1,x*x);
  xx[0]=x;
  xsq[0]=x*x;

  if(ndim>1) {
    // Extract input data
    va_list args;
    va_start(args,x);
    for(int i=1;i<ndim;i++) {
      xx[i] = va_arg(args,double);
      xsq[i]= xx[i]*xx[i];
    }
  }
  // Find first bin
  // (WHYCA NT THIS BE IN THE LOOP?)
  bin[0]=int(floor((xx[0]-lower[0])/binsize[0]));
    // Take care of out-of-bonds points for stdev
    if(bin[0]<0)
      bin[0]=-1;
    if(bin[0]>=nbins[0])
      bin[0]=nbins[0];
  // Check if it falls outside
    //fail=(bin[0]<0)||(bin[0]>=nbins[0]);

  // Find the rest of the bins of this point
  for(int i=1;i<ndim;i++) {
    bin[i]=int(floor((xx[i]-lower[i])/binsize[i]));
    //fail = fail || (bin[i]<0) || (bin[i]>=nbins[i]);

    // Take care of out-of-bonds points for stdev
    if(bin[i]<0)
      bin[i]=-1;
    if(bin[i]>=nbins[i])
      bin[i]=nbins[i];
  }

  // Add that bin
  int ii = stdevindex(bin);
  data[ii]++;

  // Increase the counters for standard deviation calculation as well
  sum[ii]+=xx;
  sumsq[ii]+=xsq;

  delete[] bin;
}

// Adds the value v to a point in the histogram
void stathistogram::addvalue(double v, double x ...)
{
  int* bin = new int[ndim];
  //int bin[ndim];
  //int fail=0;

  // Extract input data
  va_list args;
  va_start(args,x);
  matrix<double> xx(ndim,1,x);
  matrix<double> xsq(ndim,1,x*x);
  xx[0]=x;
  xsq[0]=x*x;
  for(int i=1;i<ndim;i++) {
    xx[i] = va_arg(args,double);
    xsq[i]= xx[i]*xx[i];
  }

  // Find first bin
  bin[0]=int(floor((xx[0]-lower[0])/binsize[0]));
    // Take care of out-of-bonds points for stdev
    if(bin[0]<0)
      bin[0]=-1;
    if(bin[0]>=nbins[0])
      bin[0]=nbins[0];
  // Check if it falls outside
    //fail=(bin[0]<0)||(bin[0]>=nbins[0]);

  // Find the rest of the bins of this point
  for(int i=1;i<ndim;i++) {
    bin[i]=int(floor((xx[i]-lower[i])/binsize[i]));
    //fail = fail || (bin[i]<0) || (bin[i]>=nbins[i]);

    // Take care of out-of-bonds points for stdev
    if(bin[i]<0)
      bin[i]=-1;
    if(bin[i]>=nbins[i])
      bin[i]=nbins[i];
  }

  // Add that bin
  int ii = stdevindex(bin);
  data[ii]+=v;

  // Increase the counters for standard deviation calculation as well
  sum[ii]+=xx*v;
  sumsq[ii]+=xsq*v;

  delete[] bin;
}


// Writes histogram data to a file. Does not write out the out-of-bonds
// bins
void stathistogram::write(char* filename)
{
  ofstream of(filename);

  int* bin = new int[ndim];
  //int bin[ndim];
  int loop=1;

  // Loop to exhaust all the dimensions
  while(bin[ndim-1]<nbins[ndim-1]) {

    // Write coordinates
    for(int i=0;i<ndim;i++)
      of << binsize[i]*(bin[i]+0.5)+lower[i] << '\t';

    // Write value
    of << data[stdevindex(bin)] << '\n';

    // Increase bin number
    bin[0]++;
    for(int i=0;i<ndim-1;i++)
      if(bin[i]>=nbins[i]) {
	bin[i]=0;
	bin[i+1]++;
      }
  }

  delete[] bin;
}

// This fucking complicated routine calculates the standard deviation
// for the j'th variable subdivided by the other variables.
void stathistogram::writestats(int j, char* filename)
{
  ofstream of(filename);

  // This whole thing doesn't really make sense if we only have one dim
  if(ndim>1) {
    // We loop over all the dimensions except the one we are
    // statting.
    int* bin = new int[ndim];
    for(int i=0;i<ndim;i++)
      bin[i]=0;

    //int bin[ndim];
    int loop=1;
    int loopendindex=ndim-1;
    if(j==ndim-1)
      loopendindex--;
    
    // Loop to exhaust all the dimensions
    while(bin[loopendindex]<nbins[loopendindex]) {
      
      // Write coordinates
      for(int i=0;i<ndim;i++)
	if(i!=j)
	  of << (upper[i]-lower[i])/nbins[i]*(bin[i]+0.5)+lower[i] << '\t';
      
      
      // Add up sum, sumsq and n for all of this j'th dim bins
      double s=0,ssq=0,n=0;
      for(int k=-1;k<nbins[j]+1;k++) {
	bin[j]=k;
	s+=sum[stdevindex(bin)][j];
	ssq+=sumsq[stdevindex(bin)][j];
	n+=data[stdevindex(bin)];
      }
      // Estimate median by counting numbers;
      double nmed=n/2,median;
      for(int k=-1;k<nbins[j]+1;k++) {
	bin[j]=k;
	double nthis=data[stdevindex(bin)];
	nmed-=nthis;
	if(nmed<0) {
	  median=	(upper[j]-lower[j])/nbins[j]*(k+1+nmed/nthis);
	  k=nbins[j];
	}
      }
      
      // Write statistics
      if(n>1)
	of << s/n << '\t' << sqrt( (ssq-s*s/n)/(n-1) ) << '\t' 
	   << median << '\t' << n << '\n';
      else
	of << s << '\t' << HUGE_VAL << '\t' << s << '\t' << n << '\n';
      
      // Increase bin number
      // but skip the dimension in which we are calculating
      int l=(j==0?1:0);
      bin[l]++;
      for(int i=l;i<loopendindex;i++)
	if( (i!=j) && (bin[i]>=nbins[i]) ) {
	  bin[i]=0;
	  // Increase bin# of next higher significance dimension
	  if(i==j-1)
	    bin[i+2]++;
	  else
	    bin[i+1]++;
	}
    }
    delete[] bin;
  }
  else {
    // If ndim=1 we just output the stats
    j=0;

    // Add up sum, sumsq and n for all of this j'th dim bins
    double s=0,ssq=0,n=0;
    for(int k=-1;k<nbins[0]+1;k++) {
      int bin=k;
      s+=sum[stdevindex(&bin)][j];
      ssq+=sumsq[stdevindex(&bin)][j];
      n+=data[stdevindex(&bin)];
    }
    // Estimate median by counting numbers;
    double nmed=n/2,median;
    for(int k=-1;k<nbins[j]+1;k++) {
      int bin=k;
      double nthis=data[stdevindex(&bin)];
      nmed-=nthis;
      if(nmed<0) {
	median=	(upper[j]-lower[j])/nbins[j]*(k+1+nmed/nthis);
	k=nbins[j];
      }
    }
    if(n>1)
      of << s/n << '\t' << sqrt( (ssq-s*s/n)/(n-1) ) << '\t' 
	 << median << '\t' << n << '\n';
    else
      of << s << '\t' << HUGE_VAL << '\t' << s << '\t' << n << '\n';
    
  }
  of.close();
}

// Get the bin value of the indexed bin.
// The out-of-bonds bins can be retrieved by indexing -1 or nbins[i]
// But the coordinate values will be incorrect
bin histogram::getbin(int i1 ...)
{
  int* bnum = new int[ndim];
  //int bnum[ndim];
  bnum[0]=i1;

  bin b;
  b.coords=matrix<double>(ndim,1,0.);
  b.coords[0] = binsize[0]*(bnum[0]+0.5)+lower[0];

  // Extract input data
  va_list args;
  va_start(args,i1);
  for(int i=1;i<ndim;i++) {
    bnum[i] = va_arg(args,int);
    b.coords[i] = binsize[i]*(bnum[i]+0.5)+lower[i];
  }

  // Get the bin value
  b.value = data[stdevindex(bnum)];

  delete[] bnum;

  return b;
}

// Get statistics over the j'th dimension, given the bins in the other dims
statbin stathistogram::getstats(int j ...)
{
  int* bnum = new int[ndim];

  statbin b;
  b.coords=matrix<double>(ndim,1,0.);
  b.coords[j]=0;

  // Extract input data
  va_list args;
  va_start(args,j);
  for(int i=0;i<ndim;i++) {
    if(i!=j) {
      bnum[i] = va_arg(args,int);
      b.coords[i] = binsize[i]*(bnum[i]+0.5)+lower[i];
    }
  }

  // Add up sum, sumsq and n for all of this j'th dim bins
  double s=0,ssq=0,n=0;
  for(int k=-1;k<nbins[j]+1;k++) {
    bnum[j]=k;
    int ind=stdevindex(bnum);
    s+=sum[ind][j];
    ssq+=sumsq[ind][j];
    n+=data[ind];
  }
  // Estimate median by counting numbers (assumes uniform distr within bin)
  double nmed=n/2,median;
  for(int k=-1;k<nbins[j]+1;k++) {
    bnum[j]=k;
    double nthis=data[stdevindex(bnum)];
    nmed-=nthis;
    if(nmed<0) {
      b.median=	(upper[j]-lower[j])/nbins[j]*(k+1+nmed/nthis);
      k=nbins[j];
    }
  }
  
  b.n=n;
  if(n>1) {
    b.mean=s/n;
    b.stdev=sqrt( (ssq-s*s/n)/(n-1) );
  }
  else {
    b.mean=s;
    b.stdev=HUGE_VAL;
    b.median=s;
  }

  delete[] bnum;

  return b;
}

