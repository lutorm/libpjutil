#include <cstdio>
#include "hpm.h"
#include <iostream>
#include <sstream>
#include <fstream>
#include <vector>
#include <boost/thread.hpp>
#include <boost/thread/mutex.hpp>
#include <boost/lexical_cast.hpp>
#include <blitz/array.h>

bool hpm::enable_= true;

using namespace std;

// If we have the HPM library, the functions are just wrappers.
#ifdef HAVE_LIBHPM

#include "libhpm.H"
#error hpm implementation needs to be done

#elif defined(HAVE_LIBPAPI)

// if we have PAPI, we need to translate the functions to something reasonable
#include <papi.h>
#include <papiStdEventDefs.h>
#include <pthread.h>

const int nevents=3;
int Events[nevents] = {PAPI_TOT_CYC, PAPI_TOT_INS, PAPI_RES_STL
		       //PAPI_L3_DCA, PAPI_L3_TCM
//0x40000004, 0x40000006
//PAPI_L1_DCM, PAPI_LD_INS, 
 };
int nevents_used=nevents;

/// Mutex used to serialize the writing of the output data.
boost::mutex mutex;

/** This structure keeps the counter values for the stages. */
struct thread_data {
  int id;
  vector<int> stage_stack;
  /// The counter values for the stages
  vector<blitz::TinyVector<long_long, nevents> > values;
  vector<string> stage_names;
  /// The number of times the counters were accumulated for that stage
  vector<int> num;
  /// The number of times the stage was entered (not incremented by
  /// pushing/popping stages on top of it.
  vector<int> num_entry;
};

/** This is thread-local pointer keeps the data for the different
    threads. Using this, we don't have to allocate thread-specific
    data when creating the threads, so the creating thread doesn't
    have to know what we want to do here. The drawback is that the
    static initializer means you have to link to boost_thread just for
    linking libPJutil even if you don't use this module. */
boost::thread_specific_ptr<thread_data> tdata;

void hpm::init()
{
  int status=PAPI_library_init(PAPI_VER_CURRENT);
  if(status<0) {
    cerr << "Error initializing PAPI" << endl;
    return;
  }

  const int nc = PAPI_num_counters();
  cout << "Initializing PAPI: " << nc << " counters available. Tracking events:\n";
  if(nevents_used>nc) {
    cerr << "Not enough counters for specified events.\n";
    nevents_used=nc;
  }

  char EventCodeStr[PAPI_MAX_STR_LEN];
  for(int i=0; i<nevents_used; ++i) {
    if (PAPI_event_code_to_name(Events[i], EventCodeStr) == PAPI_OK)
      printf("\t%s\n", EventCodeStr);
  }
}


void hpm::enumerate_events()
{
  char EventCodeStr[PAPI_MAX_STR_LEN];

  cout << "Available PAPI events:\n";
  int EventCode = 0 | PAPI_NATIVE_MASK;
  do {
    /* Translate the integer code to a string */
    if (PAPI_event_code_to_name(EventCode, EventCodeStr) == PAPI_OK)
      
      /* Print all the native events for this platform */
      printf("Name: %s\nCode: %x\n", EventCodeStr, EventCode);
    
  } while (PAPI_enum_event(&EventCode, 0) == PAPI_OK);
}
 
void hpm::handle_papi_error(int err)
{
  switch (err) {
  case PAPI_EINVAL:
    cerr << "PAPI_EINVAL\n";
    break;
  case PAPI_EISRUN:
    cerr << "PAPI_EISRUN\n";
    break;
  case PAPI_ESYS:
    cerr << "PAPI_ESYS: " << strerror(errno) << endl;;
    break;
  case PAPI_ENOMEM:
    cerr << "PAPI_ENOMEM\n";
    break;
  case PAPI_ENOEVNT:
    cerr << "PAPI_ENOEVNT\n";
    break;
  default:
    cerr << "Unknown error: " << err << endl;
  }
}


void hpm::thread_init(int thread_id, int num_stages)
{
  if (!enable_) return;

  // we add one stage for the overhead calculation
  tdata.reset(new thread_data);
  assert(tdata.get());
  tdata->id=thread_id;
  tdata->num.assign(num_stages+1, 0);
  tdata->num_entry.assign(num_stages, 0);
  tdata->values.resize(num_stages+1);
  tdata->stage_names.resize(num_stages);
  for(int i=0;i<num_stages+1;++i)
    for(int j=0;j<nevents_used;++j)
      tdata->values[i][j]=0;
  tdata->stage_stack.reserve(10);

  int retval=PAPI_OK;
  if ((retval=PAPI_thread_init(pthread_self)) != PAPI_OK) {
    cerr << "Error initializing PAPI thread: ";
    handle_papi_error(retval);
    enable_=false;
  }
}

void hpm::set_stage_name(int stage, const string& name) {
  if (!enable_) return;
  assert(stage<=tdata->values.size());
  tdata->stage_names[stage]=name;
}

inline void hpm::accumulate_stage(int stage)
{
  // add counters to the specified stage
  int retval=PAPI_OK;
  if((retval=PAPI_accum_counters((long long*)&tdata->values[stage+1],
				 nevents_used))!=PAPI_OK) {
    handle_papi_error(retval);
    enable_=false;
  }
  ++tdata->num[stage+1];

  // and overhead
  if((retval=PAPI_accum_counters((long long*)&tdata->values[0], nevents))!=PAPI_OK) {
    cerr << "Error accumulating counters: ";
    handle_papi_error(retval);
    enable_=false;
  }
  ++tdata->num[0];
}

void hpm::thread_start(int stage)
{
  if (!enable_) return;
  assert(stage<=tdata->values.size());

  int retval=PAPI_OK;
  tdata->stage_stack.clear();
  tdata->stage_stack.push_back(stage);
  tdata->num_entry[stage]++;
  if((retval=PAPI_start_counters(Events, nevents_used))!=PAPI_OK) {
    cerr << "Error starting counters: ";
    handle_papi_error(retval);
    enable_=false;
    return;
  }
}

void hpm::enter_stage_impl(int stage)
{
  assert(stage<=tdata->values.size());

  accumulate_stage(tdata->stage_stack.back());

  tdata->stage_stack.back()=stage;
  tdata->num_entry[stage]++;
}

void hpm::push_stage_impl(int stage)
{
  assert(stage<=tdata->values.size());

  accumulate_stage(tdata->stage_stack.back());

  tdata->stage_stack.push_back(stage);
  tdata->num_entry[stage]++;
}

void hpm::pop_stage_impl()
{
  assert(!tdata->stage_stack.empty());

  accumulate_stage(tdata->stage_stack.back());

  tdata->stage_stack.pop_back();
}

void hpm::dump_data(const std::string& label, const std::string& outfile)
{
  if (!enable_) return;

  stringstream s;
  blitz::TinyVector<long long, nevents> net, raw, tot;

  const bool sub_overhead=true;
  s << "T" << tdata->id << " Performance data for \"" << label << "\"\n";
  char EventCodeStr[PAPI_MAX_STR_LEN];

  // first loop over stages to sum up the totals
  tot=0;
  for(int st=1;st<tdata->values.size();++st) {
    raw = tdata->values[st];
    net = raw - tdata->values[0]*tdata->num[st]/tdata->num[0];
    tot += net;
  }

  // now loop again to output
  for(int st=1;st<tdata->values.size();++st) {
    raw = tdata->values[st];
    net = raw - tdata->values[0]*tdata->num[st]/tdata->num[0];

    const string label("T"+boost::lexical_cast<string>(tdata->id)+
		       " S"+boost::lexical_cast<string>(st-1));
    s.precision(2);
    s << scientific;
    s << label << " Stage " << st-1 << " (\"" << tdata->stage_names[st-1] << "\", entered " << tdata->num_entry[st-1] << " times)\n";
    for(int i=0;i<nevents_used;++i) {
      PAPI_event_code_to_name(Events[i],EventCodeStr);
      s << label << '\t' << EventCodeStr << ":\t" << net[i]
	<< "\t(raw: " << raw[i] << ", " << 1-1.0*net[i]/raw[i] << " overhead)\n";
    }

    s << fixed;
    s << label << '\n';
    s << label << "\tcycles/total:\t" << 1.0*net[0]/tot[0] << '\n';
    s << label << "\tInstr/cyc:\t" << 1.0*net[1]/net[0] << "\t(overall: " << 1.0*tot[1]/tot[0]<<")\n";
    if(nevents_used>3)
      s << label << "\tLLC ref/ins.:\t" << 1.0*net[3]/net[1] << "\t(overall: " << 1.0*tot[3]/tot[1]<<")\n";
    if(nevents_used>4) {
      s << label << "\tLLC miss/ins.:\t" << 1.0*net[4]/net[1] << "\t(overall: " << 1.0*tot[4]/tot[1]<<")\n";
      s << label << "\tLLC miss rate:\t" << 1.0*net[4]/net[3] << "\t(overall: " << 1.0*tot[4]/tot[3]<<")\n";
    }
    if(nevents_used>2)
      s << label << "\tcyc stalled:\t" << 100.0*net[2]/net[0] << "%\t(overall: " << 100.0*tot[2]/tot[0]<<"%)\n";
    if(st<tdata->values.size()-1)
      s << "T"+boost::lexical_cast<string>(tdata->id);
    s << '\n';
  }

  if (outfile!="") {
    boost::mutex::scoped_lock lock(mutex);
    ofstream os(outfile.c_str(), ios::out | ios::app);
    os << s.str();
    os.close();
  }
  else
    cout << s.str();
}

void hpm::thread_stop(const string& label, const std::string& outfile)
{
  if (!enable_) return;

  accumulate_stage(tdata->stage_stack.back());

  dump_data(label, outfile);

  // now stop the counters (don't do this earlier because it would
  // clobber the arrays)
  PAPI_stop_counters((long long*)&tdata->values[tdata->stage_stack.back()], 
		     nevents_used);

  // reset the counter data for next time
  for(int i=0;i<tdata->values.size();++i)
    for(int j=0;j<nevents_used;++j)
      tdata->values[i][j]=0;
};

int hpm::current_stage() { return enable_ ? tdata->stage_stack.back() : -1; };

#endif

