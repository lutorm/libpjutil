// matrix.h
//
// Declaration of matrix-vector routines
//
// Patrik Jonsson
//
// $Id$

#ifndef __matrix__
#define __matrix__

#include <iostream>
#include <math.h>
#include <stdarg.h>
#include "biniostream.h"

//#include "vec3.h"

// Most of these are unnecessary, but I guess it doesn't hurt...
template<class T> class table;
template<class T> class matrix;
matrix<int> converttoint(const matrix<double>& m);
template<class T> matrix<T> transp(const matrix<T>& m);
template<class T> matrix<T>  emult(const matrix<T> &, const matrix<T> &);
template<class T>  matrix<T> operator+(T, const matrix<T>&);
template<class T>  matrix<T> operator-(T, const matrix<T>&);
template<class T> matrix<T> operator*(T, const matrix<T>&);
template<class T> int operator==(T, const matrix<T>&);
template <class T > int operator!=(T, const matrix<T>&);
template<class T>  matrix<T> cross(const matrix<T>&, const matrix<T>&);
template<class T>  T dot(const matrix<T>&, const matrix<T>&);
template<class T>  matrix<T> norm(const matrix<T>&);
template<class T>  matrix<T> invert(matrix<T>);
template<class T>  std::ostream& operator<<(std::ostream&, const matrix<T>&);
template<class T>  std::istream& operator>>(std::istream&, matrix<T>&);
template<class T>  binfstream& operator<<(binfstream&, const matrix<T>&);
template<class T>  binfstream& operator>>(binfstream&, matrix<T>&);

template<class T> class matrix {
  friend class table<T>;
private:
  short int rows,cols; // Numbers of rows and columns in matrix
  T* value;      // Pointer to data array
  T  arr[3];     // Used if rows*cols<=3 to avoid free store allocation
  //  char alloc;     // 1 if array is allocated and needs to be freed

  // Private constructors for internal use
  matrix(int,int,T*);
  void allocate();

  // Simulates pointer to data array, either returns
  // value or &arr depending on allocation
  T* val() const;

  void initialise(); // ONLY HERE BECAUSE OF THE STUPID BUG IN ALPHA G++

  //void error_handler();
  void mult(const matrix&, const matrix&);
  void error(char*);

  // Inversion support routines
  void ludcmp(int*, T*);
  void lubksb(int*, matrix&) const;
  matrix tqli(matrix&,matrix&);
  void tred2(matrix&,matrix&);

 public:
    matrix();
  //matrix(int);
  matrix(int,int);
  matrix(int, int, T);
  matrix(double,double,double); // This won't compile with T,T,T on !gcc
  matrix(const matrix&);
  //matrix(const vec3<T>&);
  matrix(int, int, T, T, ...);
  void asscolvecs(const matrix&,const matrix&,const matrix&);
  //assemblerowvectors(const matrix&,const matrix& ...);
  ~matrix();

  class range {}; // exception class for "out-of-range operation"
  class size {};  // exception class for "illegal size for requested operation"

  int getrows() const;
  int getcols() const;
  int rangecheck(int r,int c) const; // Checks if given indices are valid
  int samesize(const matrix& m) const; // Checks if matrices have same size
  T get(int,int) const;
  void set(int,int,T);
   
  // Matrix operators
  operator T() const; // Convert a single-value matrix to its equivalent
  friend matrix<int> converttoint(const matrix<double>& m);
  template<class R> operator matrix<R> ();
  
  matrix operator-() const;
  matrix operator[](int) const; // Array notation for extraction
  void transp ();
  //friend matrix transp <T>(const matrix&);
  matrix& blockSet(int,int,const matrix&);
  matrix blockGet(int,int,int,int);
 
  // Matrix-matrix operators
  matrix operator+(const matrix&) const;
  matrix operator-(const matrix&) const;
  matrix operator*(const matrix&) const; // Matrix product
  matrix operator/(const matrix&) const; // Element-wise division
  //friend matrix emult <T>(const matrix&, const matrix&); // Element-wise multiplication
  
  matrix& operator=(const matrix&); // Possibly size-changing
  matrix& operator+=(const matrix&);
  matrix& operator-=(const matrix&);
  matrix& operator*=(const matrix&); // Possibly size-changing
  matrix& operator/=(const matrix&); // Element-wise division
  matrix& emult(const matrix&); // Element-wise multiplication
  //friend table<T>& table<T>::operator=(const matrix&);
  //friend table<T>::table<T>(const matrix&);
  //friend matrix table<T>::makematrix() const;
  
  int operator==(const matrix&) const;
  int operator!=(const matrix&) const;
  int operator<(const matrix&) const;
  int operator>(const matrix&) const;
  int operator<=(const matrix&) const;
  int operator>=(const matrix&) const;
    
  // Matrix-scalar operators (element-wise operations)
  matrix operator+(T) const;
  matrix operator-(T) const;
  matrix operator*(T) const;
  matrix operator/(T) const;
  
  
  //friend matrix operator+ <T>(T, const matrix&);
  //friend matrix operator- <T>(T, const matrix&);
  //friend matrix operator* <T>(T, const matrix&);
  
  matrix& operator=(T); // 
  matrix& operator+=(T);
  matrix& operator-=(T);
  matrix& operator*=(T);
  matrix& operator/=(T);

  int operator==(T) const;
  int operator!=(T) const;
  //friend int operator==<T>(T, const matrix&);
  //friend int operator!=<T>(T, const matrix&);

  // Vector operations
  friend matrix cross <T>(const matrix&, const matrix&);
  friend T dot <T>(const matrix&, const matrix&);
  T mag() const;
  //    { return sqrt(dot(*this));};
  friend matrix norm <T>(const matrix&);
  matrix expand() const;
  matrix cart2sph() const;  
  matrix sph2cart() const;

  // Linear algebra
  friend matrix invert <T>(matrix);
  void gaussj(matrix*, int);
  matrix eigen();

  // I/O operators
  friend std::ostream& operator<< <T>(std::ostream&, const matrix&);
  friend std::istream& operator>> <T>(std::istream&, matrix&);

  friend binfstream& operator<< <T>(binfstream&, const matrix&);
  friend binfstream& operator>> <T>(binfstream&, matrix&);
  
};

#include "tableutils.h"

// **********************
// * Inline definitions *
// **********************

// This initialises a matrix to be an unallocated 0x0 matrix
// IF THIS IS CALLED ON AN INITIALISED MATRIX BIG FUCKUP WILL OCCUR!!!
// THIS IS ONLY CALLED FROM STUPID.CC BEACUSE OF THE BUG IN ALPHA G++
template<class T> inline void matrix<T>::initialise()
{
  rows = 0;
  cols = 0;

  value=0;
  //  alloc = 0;
}

// Default matrix constructor
// Creates 0*0 unallocated matrix
template<class T> inline matrix<T>::matrix()
{
  rows = 0;
  cols = 0;
  
  value=0;
  //  alloc = 0;
}

// Constructor for making a "reference matrix" with value
// pointing to an already allocated part of a matrix array
// (this routine only used by matrix::operator[])
template<class T> matrix<T>::matrix(int r, int c, T* v)
{
  rows = r;
  cols = c;

  // This is the only case when we need the alloc member...
  // We can use the arr part to indicate that it's not to be deleted
  arr[0] = 1;
  value = v;
  //  alloc = 0;
}

// Constructor for matrix of specified size
// matrix is not initialised for efficiency reasons
// This used to be private but for efficiency reasons it's important
// that it can be used so what the hell...
template<class T> inline matrix<T>::matrix(int r, int c)
{
  rows = r;
  cols = c;

  allocate();
}

// Constructor for matrix of specified size
// matrix is initialised to v
template<class T> matrix<T>::matrix(int r, int c, T v)
{
  rows = r;
  cols = c;

  allocate();
  
  *this=v;
}

// val() returns the pointer to the data storing array
// It replaces the use of value now when alloc has been eliminated
// and we don't have value pointing to the allocated array or to &arr.
template<class T> inline T* matrix<T>::val() const
{
  if(value)
    // If a pointer is stored in value, return that
    return value;
  else
    // If not, return the address of arr
    return (T*)&arr;
}

// Takes 3 columns vectors and creates a matrix from it
template<class T> void inline matrix<T>::asscolvecs(const matrix& v1,
					       const matrix& v2, 
					       const matrix& v3)
{
  // Find dimensions
  if(v1.cols!=1) {
    std::cerr << "asscolvecs:: can only assemble column vectors\n";
  }
  if( (cols!=3) || (rows!=v1.rows) ) {
    // We need to reallocate
    if( (value) && (arr[0]==0) )
      delete[] value;
    
    rows = v1.rows;
    cols = 3;
    allocate();
  }

  // Now we fill the values
  T* v=val();
  T* vv=v1.val();
  for(int r=0;r<rows;r++)
    v[r*cols]=vv[r];
  vv=v2.val();
  for(int r=0;r<rows;r++)
    v[r*cols+1]=vv[r];
  vv=v3.val();
  for(int r=0;r<rows;r++)
    v[r*cols+2]=vv[r];
}


// Allocates memory to fill matrix (if rows*cols>3).
// * Already allocated memory is no longer deleted, this was only used
// in the assignment operator and is moved there for less overhead.
// The "alloc" flag is not used anymore, its function is in the 
// preallocated data array to save memory.
template<class T> inline void matrix<T>::allocate()
{
#ifdef __alpha
arr[0]=0;
arr[1]=0;
arr[2]=0;
#endif
  if((rows*cols)<=3) {
    // don't need to allocate, just set value=0
    value = 0;
    //    value = arr;
    //    alloc = 0;
  }
  else {
    // allocate memory and set allocation flag
    value = new T[rows*cols];
    //alloc = 1;
    arr[0]=0;
  }
}


// Destructor only has to delete allocated array
template<class T> inline matrix<T>::~matrix()
{
  //  if(alloc)
  // Check if array is allocated - if arr[0]!=0 we just have a 'reference'
  if( (value) && (arr[0]==0) )
    delete[] value;
}

// Returns number of rows in matrix
template<class T> inline int matrix<T>::getrows() const
{
  return rows;
}

// Returns number of columns in matrix
template<class T> inline int matrix<T>::getcols() const
{
  return cols;
}

// Checks if element (r,c) exists in matrix
// If not, throws range() exception, otherwise returns with 1
template<class T> inline int matrix<T>::rangecheck(int r,int c) const
{
  if (! ((r>=0)&&(r<rows) && (c>=0)&&(c<cols)) ) {
    std::cerr << "matrix<T>::rangecheck: Index out of range: " << r<<','<< c <<'\n' 
	      << *this << '\n';
    return 0;
    //throw range();
  }
  else
    return 1;
}

// Checks if matrices are the same size
template<class T> inline int matrix<T>::samesize(const matrix<T>& m) const
{
  return ((m.cols==cols)&&(m.rows==rows));
}

// Converts 1*1 matrix to T - let's hope it's not implicitly invoked...
template<class T> inline matrix<T>::operator T() const
{
  T* v = val();

  if((rows==1)&&(cols==1))
    //    return value[0];
    return v[0];
  else {
    std::cerr << "matrix<T>::operator T(): Cannot convert non-1*1 matrix to T\n";
    std::cerr << "  size is " << rows << '*' << cols << "\n";
    std::cerr << "  this is " << this << '\n';
//    throw size();
    return 0;
  }
}

template<class T> inline matrix<T> matrix<T>::operator+(const matrix<T>& m) const
{
  return matrix(*this) += m;
}

template<class T> inline matrix<T> matrix<T>::operator-(const matrix<T>& m) const
{
  return matrix(*this) -= m;
}

template<class T> inline matrix<T> matrix<T>::operator/(const matrix<T>& m) const
{
  return matrix(*this) /= m;
}

// Element-wise multiplication
template<class T> inline matrix<T> emult(const matrix<T>& m1, const matrix<T>& m2)
{
  matrix<T> p(m1);
  p.emult(m2);

  return p;
}

template<class T> inline matrix<T> matrix<T>::operator+(T d) const
{
  return matrix(*this) += d;
}

template<class T> inline matrix<T> matrix<T>::operator-(T d) const
{
  return matrix(*this) -= d;
}

template<class T> inline matrix<T> matrix<T>::operator*(T d) const
{
  return matrix(*this) *= d;
}

template<class T> inline matrix<T> matrix<T>::operator/(T d) const
{
  return matrix(*this) /= d;
}

template<class T> inline matrix<T> operator+(T d, const matrix<T>& m)
{
  return matrix<T>(m) += d;
}

template<class T> inline matrix<T> operator-(T d, const matrix<T>& m)
{
  return matrix<T>(-m) += d;
}

template<class T> inline matrix<T> operator*(T d, const matrix<T>& m)
{
  return matrix<T>(m) *= d;
}

template<class T> inline int operator==(T d, const matrix<T>& m)
{
  return (m==d);
}

template<class T> inline int operator!=(T d, const matrix<T>& m)
{
  return (m!=d);
}


// ***
// *** End of inlines ***
// ***


// Copy constructor
template<class T> matrix<T>::matrix(const matrix& m)
{
  rows=m.rows;
  cols=m.cols;
  
  //  alloc = 0;

  if(!m.value) {
    // Not allocated, just copy the three components of arr
    value=0;
    arr[0]=m.arr[0];
    arr[1]=m.arr[1];
    arr[2]=m.arr[2];
  }
  else {
    // It's just a plain allocated matrix.
    allocate();

    T* v = val();
    T* mv = m.val();

    for(int i=0;i<rows*cols;i++)
      v[i] = mv[i];
  }
}


// Constructor for explicit initialisation
// Using "arbitrary number of arguments" notation
template<class T> matrix<T>::matrix(int r,int c, T d1, T d2, ...)
{
  va_list ap;

  rows=r;
  cols=c;

  allocate();

  T* v = val();

  v[0]=d1;
  v[1]=d2;

  va_start(ap,d2);
  for(int i=2;i<rows*cols;i++)
    v[i]=va_arg(ap,T);

  va_end(ap);
}


// Constructor for explicit 3-vector initialisation
// There is a problem using this for ints because of ambiguity...
template<class T> matrix<T>::matrix(double a, double b, double c)
{
  rows = 3;
  cols = 1;

  //  alloc = 0;
  value=0;
  allocate();

  T* v = val();

  v[0]=a;
  v[1]=b;
  v[2]=c;
}

    
// Returns a matrix element
template<class T> T matrix<T>::get(int r, int c) const
{
  if(rangecheck(r,c)){
    T* v = val();
    return v[r*cols+c];
  }
}

// Sets a matrix element
template<class T> void matrix<T>::set(int r, int c, T number)
{
  if(rangecheck(r,c)) {
    T* v = val();
    v[r*cols+c]=number;
  }
}

// Inserts m into this, starting at rs,cs
template<class T> matrix<T>& matrix<T>::blockSet(int rs, int cs, 
						 const matrix<T>& m)
{
  if( (rs+m.rows>rows) || (cs+m.cols>cols) ) {
    std::cerr << "matrix::blockSet: block doesn't fit!\n";
    return *this;
  }

  T* v = val();
  T* vm = m.val();
    
  for(int r=0,R=0,RR=rs*cols;r<m.rows;r++,R+=m.cols,RR+=cols)
    for(int c=0;c<m.cols;c++)
      v[RR+c+cs]=vm[R+c];

  return *this;
}

// Extracts a block from this
template<class T> matrix<T> matrix<T>::blockGet(int rs, int cs,int nr,int nc)
{
  if( (rs+nr>rows) || (cs+nc>cols) ) {
    std::cerr << "matrix::blockGet: block doesn't fit!\n";
    return *this;
  }

  matrix<T> m(nr,nc);

  T* v = val();
  T* vm = m.val();
    
  for(int r=0,R=0,RR=rs*cols;r<m.rows;r++,R+=m.cols,RR+=cols)
    for(int c=0;c<nc;c++)
      vm[R+c]=v[RR+c+cs];

  return m;
}

// ***
// *** Matrix operators ***
// ***

// Unary negation operator
template<class T> matrix<T> matrix<T>::operator-() const
{
  matrix a(rows,cols);

  if(!value) {
    a.arr[0]=-arr[0];
    a.arr[1]=-arr[1];
    a.arr[2]=-arr[2];
  }
  else {
    T* v = val();
    T* va = a.val();
    
    for(int i=0;i<rows*cols;i++)
      va[i]=-v[i];
  }

  return a;
}

// Index operator extracts the r'th row of a matrix and
// returns it as a row vector ( a (cols*1) matrix)
// Two calls to this function a[r][k] will return the (r,k) element
// as a 1*1 matrix. (Hopefully this will be automatically converted to T
// if we want...)
// Note that we return a "ghost matrix" which has a data pointer
// to this matrix data. This should be safe and will enable notation
// like m[r][c]=3.
template<class T> matrix<T> matrix<T>::operator[](int r) const
{
  if(rangecheck(r,0)) {
    T* newvalue = val() + r*cols;
    return matrix<T>(cols,1,newvalue);
  }
  else {
    std::cerr << "matrix<T>::operator[]: index out of range\n";
    return *this;
  }

}

// Transposes this
template<class T> void matrix<T>::transp()
{
  T* v = val();

  for(int r=0,R=0;r<rows;r++,R+=cols)
    for(int c=0,C=0;c<r;c++,C+=cols) {
      T temp=v[R+c];
      v[R+c]=v[C+r];
      v[C+r]=temp;
    }
}

// Transposes matrix
template<class T> matrix<T> transp(const matrix<T>& m)
{
  matrix<T> a(m.cols,m.rows);

  T* va = a.val();
  T* vm = m.val();

  for(int r=0;r<a.rows;r++)
    for(int c=0;c<a.cols;c++)
      va[r*a.cols+c]=vm[c*m.cols+r];

  return a;
}

// ***
// *** Matrix-matrix operators ***
// ***

// Matrix multiplication (returns matrix of different size)
template<class T> matrix<T> matrix<T>::operator*(const matrix& m) const
{
  // Check that multiplication can be done
  if( (m.cols==1) && (m.rows==1)) {
    // m is actually a scalar - multiply
    T scalar = *(m.val());
    return (*this)*scalar;    
  }
  else if((rows==1)&&(cols==1)) {
    // this is actually a scalar - multiply
    T scalar = *val();
    matrix ab(m.rows,m.cols);
    T* vm  = m.val();
    T* vab = ab.val();

    for(int i=0;i<m.cols*m.rows;i++)
      vab[i] = vm[i]*scalar;
    return ab;
  }
  else if(cols==m.rows) {
    // do the full multiplication shit
    const int newrows=rows;
    const int newcols=m.cols;
    const int dotlen=cols;

    matrix ab(newrows,newcols);
    T* v  = val();
    T* vm = m.val();
    T* vab = ab.val();

    
    // Make temporary transposed array of m to improve memory access
    T* tempm = new T[m.rows*m.cols];
    //matrix<T> tempm(m.cols,m.rows);
    
    for(int r=0,R=0;r<m.cols;r++,R+=m.rows)
      for(int c=0,C=0;c<m.rows;c++,C+=m.cols) {
	tempm[R+c]=vm[C+r];
	//(tempm.val())[R+c]=vm[C+r];
      }
//cout << "tempm " << tempm << '\n';

    for(int r=0,R=0,RR=0;r<newrows;r++,R+=newcols,RR+=cols) // rows
      for(int c=0,C=0;c<newcols;c++,C+=m.rows) {// cols
	T dot=0;
	for(int i=0;i<dotlen;i++) { // contraction loop
	  dot+=v[RR+i]*tempm[C+i];
	}
	vab[R+c]=dot;
      }
	  //	  vab[r*ab.cols+c]+=v[r*cols+i]*tempm[c*m.cols+i];
	  //vab[r*ab.cols+c]+=v[r*cols+i]*vm[i*m.cols+c];
    //vab[R+c]+=v[RR+i]*vm[I+c];

   delete[] tempm;
    return ab;
  }
  else {
    std::cerr << "multiply: Matrices not appropriate size for multiplication\n";
    return *this;
    // throw size();
  }
}

// Matrix assignment operator
template<class T> matrix<T>& matrix<T>::operator=(const matrix& m)
{
  if (!samesize(m)) {
    // If *this is not appropriate size it is (re)allocated

    // Check if we have allocated memory
    // We have allocated memory if the pointer is nonzero AND
    // arr[0]==0. For the 'reference matrix' arr[0]=1.
    if( (value) && (arr[0]==0) )
      delete[] value;

    rows = m.rows;
    cols = m.cols;
    allocate();
  }

  if((!value)&&(!m.value)) {
    // if not allocated rip through arr
    arr[0]=m.arr[0];
    arr[1]=m.arr[1];
    arr[2]=m.arr[2];
  }
  else {
    T* v = val();
    T* vm = m.val();
    
    for(int i=0;i<rows*cols;i++)
      v[i]=vm[i];
  }

  return *this;
}

// Adds matrix m to this - have to be of same size
template<class T> matrix<T>& matrix<T>::operator+=(const matrix& m)
{
  if (samesize(m)) {
    if((!value)&&(!m.value)) {
      // If we're not allocated, then just rip through the three components
      T ma0=m.arr[0];
      T ma1=m.arr[1];
      T ma2=m.arr[2];

      arr[0]+=ma0;
      arr[1]+=ma1;
      arr[2]+=ma2;
    }
    else {
      T* v = val();
      T* vm = m.val();
      
      for(int i=0;i<rows*cols;i++)
	v[i]+=vm[i];
    }

    return *this;
  }
  else {
    std::cerr << "matrix::operator+=: Cannot add matrices of different sizes\n";
    return *this;
    // throw size();
  }
}

// Subtracts matrix m from this - have to be of same size
template<class T> matrix<T>& matrix<T>::operator-=(const matrix& m)
{
  if (samesize(m)) {
    if((!value)&&(!m.value)) {
      // If we're not allocated, then just rip through the three components
      T ma0=m.arr[0];
      T ma1=m.arr[1];
      T ma2=m.arr[2];

      arr[0]-=ma0;
      arr[1]-=ma1;
      arr[2]-=ma2;
    }
    else {
      // If we are, do it the old-fashioned way
      T* v = val();
      T* vm = m.val();

      for(int i=0;i<rows*cols;i++)
	v[i]-=vm[i];
    }
    return *this;
  }
  else {
    std::cerr << "matrix::operator-=: Cannot subtract matrices of different sizes\n";
    return *this;
    //throw size();
  }
}

// Element-wise division - have to be of same size
template<class T> matrix<T>& matrix<T>::operator/=(const matrix& m)
{
  if (samesize(m)) {
    if((!value)&&(!m.value)) {
      // If we're not allocated, then just rip through the three components
      T ma0=m.arr[0];
      T ma1=m.arr[1];
      T ma2=m.arr[2];

      arr[0]/=ma0;
      arr[1]/=ma1;
      arr[2]/=ma2;
    }
    else {
      // If we are, do it the old-fashioned way
      T* v = val();
      T* vm = m.val();

      for(int i=0;i<rows*cols;i++)
	v[i]/=vm[i];
    }
    return *this;
  }
  else {
    std::cerr << "matrix::operator/=: Cannot element divide matrices of different sizes\n";
    return *this;
    //throw size();
  }
}

// Element-wise multiplication - have to be of same size
template<class T> matrix<T>& matrix<T>::emult(const matrix& m)
{
  if (samesize(m)) {
    if((!value)&&(!m.value)) {
      // If we're not allocated, then just rip through the three components
      T ma0=m.arr[0];
      T ma1=m.arr[1];
      T ma2=m.arr[2];

      arr[0]*=ma0;
      arr[1]*=ma1;
      arr[2]*=ma2;
    }
    else {
      // If we are, do it the old-fashioned way
      T* v = val();
      T* vm = m.val();

      for(int i=0;i<rows*cols;i++)
	v[i]*=vm[i];
    }
    return *this;
  }
  else {
    std::cerr << "matrix::emult: Cannot element multiply matrices of different sizes\n";
    return *this;
    //throw size();
  }
}


// Compares this with matrix m
// matrices are equal if same size and all elements equal
template<class T> int matrix<T>::operator==(const matrix& m) const
{
  if (!samesize(m))
    return 0;

  T* v = val();
  T* vm = m.val();
  
  for(int i=0;i<rows*cols;i++)
    if(v[i]!=vm[i])
      return 0;
    return 1;
}

// Compares and negates this with matrix m
template<class T> int matrix<T>::operator!=(const matrix& m) const
{
  if (!samesize(m))
    return 1;

  T* v = val();
  T* vm = m.val();

  for(int i=0;i<rows*cols;i++)
    if(v[i]!=vm[i])
      return 1;
  return 0;
}

// Compares this with matrix m
// matrix is less if all indices are less
template<class T> int matrix<T>::operator<(const matrix& m) const
{
  if (!samesize(m))
    return 0;

  T* v = val();
  T* vm = m.val();
  
  for(int i=0;i<rows*cols;i++)
    if(v[i]>=vm[i])
      return 0;
    return 1;
}

// Compares this with matrix m
// matrix is leq if all indices are leq
template<class T> int matrix<T>::operator<=(const matrix& m) const
{
  if (!samesize(m))
    return 0;

  T* v = val();
  T* vm = m.val();
  
  for(int i=0;i<rows*cols;i++)
    if(v[i]>vm[i])
      return 0;
    return 1;
}

// Compares this with matrix m
// matrix is greater if all indices are greater
template<class T> int matrix<T>::operator>(const matrix& m) const
{
  if (!samesize(m))
    return 0;

  T* v = val();
  T* vm = m.val();
  
  for(int i=0;i<rows*cols;i++)
    if(v[i]<=vm[i])
      return 0;
    return 1;
}

// Compares this with matrix m
// matrix is geq if all indices are geq
template<class T> int matrix<T>::operator>=(const matrix& m) const
{
  if (!samesize(m))
    return 0;

  T* v = val();
  T* vm = m.val();
  
  for(int i=0;i<rows*cols;i++)
    if(v[i]<vm[i])
      return 0;
    return 1;
}


// ***
// *** Matrix-scalar operators (element-wise operations) ***
// ***

// Assigns constant value to matrix
template<class T> matrix<T>& matrix<T>::operator=(T d)
{
  if(!value) {
    // If we're not allocated, then just rip through the three components
    arr[0]=d;
    arr[1]=d;
    arr[2]=d;
  }
  else {
    T* v = val();
    
    for(int i=0;i<rows*cols;i++)
      v[i] = d;
  }

  return *this;
}

// Adds d to all elements of matrix
template<class T> matrix<T>& matrix<T>::operator+=(T d)
{
  if(!value) {
    // If we're not allocated, then just rip through the three components
    arr[0]+=d;
    arr[1]+=d;
    arr[2]+=d;
  }
  else {
    T* v = val();
    
    for(int i=0;i<rows*cols;i++)
      v[i] += d;
    }
  
  return *this;
}

// Subtracts d from all elements of matrix
template<class T> matrix<T>& matrix<T>::operator-=(T d)
{
  if(!value) {
    // If we're not allocated, then just rip through the three components
    arr[0]-=d;
    arr[1]-=d;
    arr[2]-=d;
  }
  else {
    T* v = val();
    
    for(int i=0;i<rows*cols;i++)
      v[i] -= d;
  }
    return *this;
}

// Multiplies all elements of matrix by d
template<class T> matrix<T>& matrix<T>::operator*=(T d)
{
  if(!value) {
    // If we're not allocated, then just rip through the three components
    arr[0]*=d;
    arr[1]*=d;
    arr[2]*=d;
  }
  else {
    T* v = val();
    
    for(int i=0;i<rows*cols;i++)
      v[i] *= d;
  }

  return *this;
}

// Divides all elements of matrix by d
template<class T> matrix<T>& matrix<T>::operator/=(T d)
{
  if(!value) {
    // If we're not allocated, then just rip through the three components
    arr[0]/=d;
    arr[1]/=d;
    arr[2]/=d;
  }
  else {
    T* v = val();
    
    for(int i=0;i<rows*cols;i++)
      v[i] /= d;
  }

  return *this;
}

// Compares this to constant value d
// matrix-double are equal if all elements equal d
template<class T> int matrix<T>::operator==(T d) const
{
  T* v = val();

  for(int i=0;i<rows*cols;i++)
    if(v[i] != d)
      return 0;
  return 1;
}

// Not-equal operator
// matrix-double not equal if one element != d
template<class T> int matrix<T>::operator!=(T d) const
{
  T* v = val();

  for(int i=0;i<rows*cols;i++)
    if(v[i] != d)
      return 1;
  return 0;
}

// Calculates standard Euclidean Dot Product of two vectors
template<class T> T dot(const matrix<T>& a, const matrix<T>& b)
{
  if((a.cols!=1)||(b.cols!=1)||(a.rows!=b.rows)) {
    std::cerr << "dot(matrix<T>,matrix<T>): Matrices not appropriate size\n";
    //throw matrix<T>::size();
    return 0;
  }
  else {
    T ab=0;
    T* va = a.val();
    T* vb = b.val();

    if(a.rows==3)
      ab=va[0]*vb[0]+va[1]*vb[1]+va[2]*vb[2];
    else 
      for(int r=0;r<a.rows;r++)
	ab+=va[r]*vb[r];

    return ab;
  }
}

// Calculates cross product of (3-vector)x(3-vector)
template<class T> matrix<T> cross(const matrix<T>& a, const matrix<T>& b)
{
  if((a.rows==3)&&(b.rows==3)&&(a.cols==1)&&(b.cols==1)) {

    matrix<T> axb(3,1);
    T* va = a.val();
    T* vb = b.val();
    T* vaxb = axb.val();

    vaxb[0]=va[1]*vb[2]-va[2]*vb[1];
    vaxb[1]=va[2]*vb[0]-va[0]*vb[2];
    vaxb[2]=va[0]*vb[1]-va[1]*vb[0];

    return axb;
  }
  else {
    std::cerr << "vector::cross: Cannot take cross for non-3-vectors\n";
    //throw matrix<T>::size();
    return matrix<T>(0,0);
  }
}

// This routine expands an n-dimensional vector to n+1 dimensions
// and infers the last component under the assumptions that:
// * we want a unit vector
// * last component is positive
template<class T> matrix<T> matrix<T>::expand() const
{
  if(cols==1) {
    matrix<T> a(rows+1,1);
    T* v = val();
    T* va = a.val();

    for(int r=0;r<rows;r++)
      va[r]=v[r];
    
    va[rows]=sqrt(1-dot(*this,*this));
    
    return a;
  }
  else {
    std::cerr << "matrix<T>::expand(): Cannot expand multicolumn matrices\n";
    //throw matrix<T>::size();
  }
}

template<class T> inline T matrix<T>::mag() const
{
  return sqrt(dot(*this,*this));
}

template<class T> inline matrix<T> norm(const matrix<T>& m)
{
  return m/m.mag();
}

// Transforms from cartesian to spherical coordinates
template<class T> matrix<T> matrix<T>::cart2sph() const
{
  matrix<T> polar(3,1);
  const T* v=val();
  T* vm=polar.val();

  vm[0] = this->mag();
  vm[1] = acos( v[2]/vm[0] );
  vm[2] = atan2( v[1], v[0] );

  return polar;
}

// Transforms from spherical to cartesian coordinates
template<class T> matrix<T> matrix<T>::sph2cart() const
{
  matrix<T> cart(3,1);
  const T* v=val();
  T* vm=cart.val();
  T mag=v[0];

  T sv1=sin( v[1] );

  vm[0] = mag*sv1*cos( v[2] );
  vm[1] = mag*sv1*sin( v[2] );
  vm[2] = mag*cos( v[1] );

  return cart;
}


// ***
// *** I/O operators ***
// ***

// User-defined input operator for matrix type
// Takes rows*cols plain numbers
template<class T> std::istream& operator>>(std::istream& s, matrix<T>& m)
{
  T* vm = m.val();

  for(int i=0; i<m.rows*m.cols; i++)
    s >> vm[i];

  return s;
}

// User-defined output operator for matrix type
template<class T> std::ostream& operator<<(std::ostream& s, const matrix<T>& m)
{
  T* vm = m.val();

  if((m.rows==0)&&(m.cols==0)) // empty matrix
    s << "|0x0 matrix|";
  if((m.rows==1)&&(m.cols==1)) // 1*1 matrices are output just like a T
    s << vm[0];
  else if(m.cols==1) { // Vectors are output in ( x, x, x ) format
    s << "( ";
    for(int r=0;r<m.rows;r++) {
      s << vm[r];
      if(r<m.rows-1)
	s << ", ";
    }
    s << " )";
  }
  else { // Matrices are output as amatrix with "|" as boundary
    s << '\n';
    for(int r=0;r<m.rows;r++) {
      s << "|\t";
      for(int c=0;c<m.cols;c++) {
	s << vm[r*m.cols+c];
	s << '\t';
      }
      s << "|\n";
    }
  }
  
  return s;
}

// User-defined binary output operator for matrix type
template<class T> binfstream& operator<<(binfstream& s, const matrix<T>& m)
{
  T* vm = m.val();

  for(int i=0;i<m.rows*m.cols;i++)
    s << vm[i];
  return s;
}

// User-defined binary input operator for matrix type
template<class T> binfstream& operator>>(binfstream& s, matrix<T>& m)
{
  T* vm = m.val();

  for(int i=0;i<m.rows*m.cols;i++)
    s >> vm[i];
  return s;
}

#define TINY 1.0e-20;

// What used to be a is now this
template<class T>
void matrix<T>::ludcmp(int *indx, T *d)
{
  int imax;
  T big,dum,sum,temp;

  // Get the pointers to speed it up
  const int n=rows;
  matrix<T> vv(n,1);
  T* va = val();
  T* vvv = vv.val();

  *d=1.0;

  // Loop over rows to get implicit scaling info
  for (int i=0,I=0;i<n;i++,I+=n) {
    big=0.0;
    for (int j=0;j<n;j++)
      if ((temp=fabs(va[I+j])) > big) 
	big=temp;
    if (big == 0.0)
      std::cerr << "Singular matrix in routine ludcmp\n";

    vvv[i]=1.0/big; // Save the scaling
  }

  for (int j=0,J=0;j<n;j++,J+=n) { // Loop over columns in Crout's method
    //cout << "\nstart loop for j=" << j <<'\n';

    for (int i=0,I=0;i<j;i++,I+=n) { // Equation 2.3.12 for i<j
      sum=va[I+j];
      for (int k=0,K=0;k<i;k++,K+=n)
	sum -= va[I+k]*va[K+j];
      va[I+j]=sum;
    }

    //cout << j << 'a' << a;

    big=0.0;    // Init search for largest pivot element
    for (int i=j,I=J;i<n;i++,I+=n) {
      // i=j part of 2.3.12 and i=j+1...n of 2.3.13
      sum=va[I+j];
      for (int k=0,K=0;k<j;k++,K+=n)
	sum -= va[I+k]*va[K+j];
      va[I+j]=sum;
      dum = vvv[i]*fabs(sum);
      if ( dum >= big) {
	//cout << "new imax " << i << '\t' << '\n';
	// Is the figure of merit of this pivot better than the best so far?
	big=dum;
	imax=i;
      }
    }
    //cout << j << 'b' << a;
    if (j != imax) { // Do we need to interchange rows?
      //cout << "interchanging " << imax << "<->" << j << "\n";
      int IMAX=imax*n;
      for (int k=0;k<n;k++) { // yes
	dum=va[IMAX+k];
	va[IMAX+k]=va[J+k];
	va[J+k]=dum;
      }
      *d = -(*d); // and update parity
      //cout << "Changing scale factor\n";
      vvv[imax]=vvv[j]; // and interchange scale factor
      //cout << j << 'c' << a;
    }
    indx[j]=imax;
    if (va[J+j] == 0.0) va[J+j]=TINY;
    if (j != (n-1)) { // Finally, divide by the pivot element
      dum=1.0/(va[J+j]);
      for (int i=j+1,I=J+n;i<n;i++,I+=n) 
	va[I+j] *= dum;
    }
    //cout << j << 'd' << a;

  } // end column loop for j

}

// what used to be a is now this
template<class T>
void matrix<T>::lubksb(int *indx, matrix<T>& b) const
{
  int ii=-1,ip;
  T sum;

  const int n=rows;
  T* va=val();
  T* vb=b.val();

  for (int i=0,I=0;i<n;i++,I+=n) {
    ip=indx[i];
    sum=vb[ip];
    vb[ip]=vb[i];
    if (ii>=0)
      for (int j=ii;j<=i-1;j++) 
	sum -= va[I+j]*vb[j];
    else if (sum) 
      ii=i;
    vb[i]=sum;
  }
  for (int i=n-1,I=n*(n-1);i>=0;i--,I-=n) {
    sum=vb[i];
    for (int j=i+1;j<n;j++) 
      sum -= va[I+j]*vb[j];
    vb[i]=sum/va[I+i];
  }
}

template<class T>
matrix<T> invert(matrix<T> A)
{
  if(A.rows!=A.cols) {
    std::cerr << "inverse: Can't take inverse of a non-square matrix\n";
    return A;
  }

  int n=A.rows;
  matrix<T> B(n,n);
  int* indx = new int[n];

  T* vB=B.val();

  // Do the LU decomposition
  T d;
  A.ludcmp(indx,&d);

  // Back-substitute to get inverse
  for(int j=0;j<n;j++) {
    matrix<T> col(n,1,(T)0);
    T* vcol=col.val();
    vcol[j]=1.0;
    A.lubksb(indx,col);
    for(int i=0,I=0;i<n;i++,I+=n)
      vB[I+j]=vcol[i];
  }
  delete[] indx;

  return B;
}
    
// Linear equation solution by Gauss-Jordan elimination
// A is the input matrix of size n*n
// b is an array of m st right hand side vectors that will be solved
// A is returned as what?
// What used to be A is now this
template<class T>
void matrix<T>::gaussj(matrix<T>* b,int m)
{
  //  short int n=A.getrows(); // problem here - is n = nfit or npar?
  //short int n=nfit;
  // Is n = the size of the matrix?
  short int n=rows;
  //matrix<T>& A = *this;
  // Get pointers to speed things up
  T* vA=val();
  T** vb = new T*[m];
  for(int i=0;i<m;i++)
    vb[i]=b[i].val();

  typedef short int itype;
  itype* vic=new itype[n];
  itype* vir=new itype[n];
  itype* vip=new itype[n];
  for(int i=0;i<n;i++) {
    vic[i]=0;
    vir[i]=0;
    vip[i]=0;
  }
  int icol,irow;

  for (int i=0;i<n;i++) {
    T big=0.0;
    for (int j=0,J=0;j<n;j++,J+=n)
      if (vip[j] != 1)
	for (int k=0;k<n;k++) {
	  if (vip[k] == 0) {
	    if (fabs(vA[J+k]) >= big) {
	      big=fabs(vA[J+k]);
	      irow=j;
	      icol=k;
	    }
	  } 
	  else if (vip[k] > 1) 
	    //nrerror("gaussj: Singular Matrix-1");
	    std::cerr << "gaussj: Singular matrix, 1!\n";
	}
    vip[icol]+=1;

    // We now have the pivot element, so we interchange rows, if needed,
    // to put the pivot element on the diagonal. The columns are not
    // physically interchanged, only relabelled:
    // indxc[i] is the column of the ith pivot element, is the ith column
    // that is reduced, while indxr[i] is the row in which that pivot element
    // was originally located. With this form of bookkeeping, the solution b's
    // will end up in the correct order, and the inverse matrix will be
    // scrambled by columns.
    int IROW=irow*n;
    int ICOL=icol*n;    
    if (irow != icol) {
      for (int l=0;l<n;l++) {
	T temp=vA[IROW+l];
	vA[IROW+l]=vA[ICOL+l];
	vA[ICOL+l]=temp;
      }
      for (int l=0;l<m;l++) {
	T temp=vb[l][irow];
	vb[l][irow]=vb[l][icol];
	vb[l][icol]=temp;
      }
    }

    // We are now ready to divide the pivot row by the pivot element
    // located at irow,icol

    vir[i]=irow;
    vic[i]=icol;
    
    if (vA[ICOL+icol] == 0.0)
      //nrerror("gaussj: Singular Matrix-2");
      std::cerr << "gaussj: Singular matrix, 2!\n";
    
    T pivinv=1.0/vA[ICOL+icol];
    vA[ICOL+icol]=1.0;

    for (int l=0;l<n;l++) 
      vA[ICOL+l] *= pivinv;
    for (int l=0;l<m;l++) 
      vb[l][icol] *= pivinv;

    // Next, reduce the rows
    for (int ll=0,LL=0;ll<n;ll++,LL+=n)
      if (ll != icol) { // except for the pivot row, of course.
	T dum=vA[LL+icol];
	vA[LL+icol]=0.0;
	for (int l=0;l<n;l++) 
	  vA[LL+l] -= vA[ICOL+l]*dum;
	for (int l=0;l<m;l++) 
	  vb[l][ll] -= vb[l][icol]*dum;
	  //	  (b[l])[ll] -= (b[l])[icol]*dum;
      }
  }

  // This is the end of the main loop over columns of the reduction. It only
  // remains to unscramble the solution in view of the column interchanges
  // We do this by interchanging pairs of columns in reverse order that
  // the permutation was built up.
  for (int l=n-1;l>=0;l--)
    if (vir[l] != vic[l])
      for (int k=0,K=0;k<n;k++,K+=n) {
	T temp=vA[K+vir[l]];
	vA[K+vir[l]]=vA[K+vic[l]];
	vA[K+vic[l]]=temp;
      }
  // And we are done

  delete[] vb;
  delete[] vic;
  delete[] vir;
  delete[] vip;
}

// Householder reduction of a real, symmetric matrix
// This is the matrix. The arguments d and e are returned as the diagonal
// and off-diagonal elements, respectively
// The matrix is turned into the matrix Q that effects the transformation
template<class T>
void matrix<T>::tred2(matrix& d,matrix& e)
{
  int l,k,j,i;
  T scale,hh,h,g,f;
  int n=rows;
  d=matrix<T>(n,1); // Diagonal elements
  e=matrix<T>(n,1); // Off-diagonal elements
  // shortcut reference
  matrix<T>& a(*this);

  for (i=n-1;i>=1;i--) {
    l=i-1;
    h=scale=0.0;
    if (l > 0) {
      for (k=0;k<=l;k++)
	scale += fabs(a[i][k]);
      if (scale == 0.0) // if scale=0 to machine precision, skip
	e[i]=a[i][l];
      else {
	for (k=0;k<=l;k++) {
	  a[i][k] /= scale; // Use scaled a's
	  h += a[i][k]*a[i][k]; // form sigma in h
	}
	f=a[i][l];
	g=(f >= 0.0 ? -sqrt(h) : sqrt(h));
	e[i]=scale*g;
	h -= f*g;    // Now h is eq 11.2.4
	a[i][l]=f-g; // Store u in the ith row of a
	f=0.0;
	for (j=0;j<=l;j++) {
	  a[j][i]=a[i][j]/h; // Store u/H in ith column of a
	  g=0.0;             // form an element of A.u in g
	  for (k=0;k<=j;k++)
	    g += a[j][k]*a[i][k];
	  for (k=j+1;k<=l;k++)
	    g += a[k][j]*a[i][k];
	  e[j]=g/h;          // Form an element of p in temp. unused elem in e
	  f += e[j]*a[i][j];
	}
	hh=f/(h+h);  // Form K, eq 11.2.11
	for (j=0;j<=l;j++) { // form q and store in e, overwriting p
	  f=a[i][j];
	  e[j]=g=e[j]-hh*f;
	  for (k=0;k<=j;k++) // reduce a, eq 11.2.13
	    a[j][k] -= (f*e[k]+g*a[i][k]);
	}
      }
    } else
      e[i]=a[i][l];
    d[i]=h;
  }

  d[0]=0.0;
  e[0]=0.0;

  // Make eigenvectors
  for (i=0;i<n;i++) {
    l=i-1;
    if (d[i]) { // Skip this when i=0;
      for (j=0;j<=l;j++) {
	g=0.0;
	for (k=0;k<=l;k++)
	  g += a[i][k]*a[k][j];
	for (k=0;k<=l;k++)
	  a[k][j] -= g*a[k][i];
      }
    }
    d[i]=a[i][i];
    a[i][i]=1.0;
    for (j=0;j<=l;j++)
      a[j][i]=a[i][j]=0.0;
  }
}

template<class T>
T pythag(T a, T b)
{
        double absa,absb;
        absa=fabs(a);
        absb=fabs(b);
        if (absa > absb) 
	  return absa*sqrt(1.0+absb*absb/(absa*absa));
        else 
	  return (absb == 0.0 ? 0.0 : absb*sqrt(1.0+absa*absa/(absb*absb)));
}

// what kind of retarded function is this?
#define SIGN(a,b) ((b) >= 0.0 ? fabs(a) : -fabs(a))

// tridiagonal QL implicit eigenvalue/eigenvector solution
// d and e are the diagonal and off-diagonal elements
// this is the matrix Q from tred2 that will give us the eigenvectors
// Matrix is turned into the eigenvector matrix
// Returned is a matrix of eigenvalues
template<class T>
matrix<T> matrix<T>::tqli(matrix& d, matrix& e)
{
  int m,l,iter,i,k;
  T s,r,p,g,f,dd,c,b;
  int n=rows;

  matrix& Q(*this);

  for (i=1;i<n;i++) // Reorder the e's
    e[i-1]=e[i];
  e[n-1]=0.0;
  for (l=0;l<n;l++) {
      iter=0;
    do {
      for (m=l;m<n-1;m++) {
	dd=fabs(d[m])+fabs(d[m+1]);
	if ((T)(fabs(e[m])+dd) == dd) 
	  break;
      }
      if (m != l) {
	if (iter++ == 30)
	  std::cerr << "Too many iterations in tqli";
	g=(d[l+1]-d[l])/(2.0*e[l]);
	r=pythag(g,1.0);
	g=d[m]-d[l]+e[l]/(g+SIGN(r,g));
	s=c=1.0;
	p=0.0;
	for (i=m-1;i>=l;i--) {
	  f=s*e[i];
	  b=c*e[i];
	  e[i+1]=(r=pythag(f,g));
	  if (r == 0.0) {
	    d[i+1] -= p;
	    e[m]=0.0;
	    break;
	  }
	  s=f/r;
	  c=g/r;
	  g=d[i+1]-p;
	  r=(d[i]-g)*s+2.0*c*b;
	  d[i+1]=g+(p=s*r);
	  g=c*r-b;
	  for (k=0;k<n;k++) {
	    f=Q[k][i+1];
	    Q[k][i+1]=s*Q[k][i]+c*f;
	    Q[k][i]=c*Q[k][i]-s*f;
	  }
	}
	if (r == 0.0 && i >= l) continue;
	d[l] -= p;
	e[l]=g;
	e[m]=0.0;
      }
    } while (m != l);
  }
  return d;
}

#endif

