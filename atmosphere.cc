// simple numerical calculation of the US standard atmosphere model,
// taken from atmosculator.com and converted to SI

#include "atmosphere.h"
#include <algorithm>
#include <iostream>
#include "blitz/numinquire.h"

using namespace std;

// height of layer upper boundaries
const double atmosphere::h_layer_[]={11e3,20e3,32e3,47e3,51e3,71e3,84.852e3};

// parameters in fitting functions for temperature
const double atmosphere::temp_par1_[]={ 1, 0.751865, 
					0.682457, 0.482561, 
					0.939268, 1.434843, 
					1.237723 };
const double atmosphere::temp_par2_[]={ -44330.722, blitz::huge(double()),
					288150, 102920.84, 
					blitz::huge(double()), -102920.84,
					-144075 };

// parameters in fitting functions for pressure
const double atmosphere::pressure_par1_[]={ 1, 0.223361,
					    0.988626, 0.898309,
					    0.00109456, 0.838263,
					    0.917131 };
const double atmosphere::pressure_par2_[]={ -44330.722, 10999.927,
					    198912.48, 55282.49,
					    46999.855, -176150.63,
					    -194437.71 };
const double atmosphere::pressure_par3_[]={ 5.255876, 6341.6688,
					    -34.16320, -12.20114,
					    7922.3616, 12.20114,
					    17.08160 };
// type of fitting function, 0 = power law, 1 = exponential
const bool atmosphere::pressure_type_[]={ 0, 1, 0, 0, 1, 0, 0 };

double atmosphere::temperature(double h) const
{
  assert(h>=0);

  // find layer
  const int i=lower_bound(h_layer_, h_layer_+n_layers_, h) - h_layer_;

  if (i>n_layers_) {
    // outside of atmosphere
    return 0;
  }

  const double T = T0_* ( temp_par1_[i] + h/temp_par2_[i] );

  //cout << "height " << h << ": layer " << i << ", temp " << T << endl;

  return T;
}

double atmosphere::pressure(double h) const
{
  assert(h>=0);

  // find layer
  const int i=lower_bound(h_layer_, h_layer_+n_layers_, h) - h_layer_;

  if (i>n_layers_) {
    // outside of atmosphere
    return 0;
  }

  double P;

  if (pressure_type_[i]) {
    // use exponential formula
    P = P0_*pressure_par1_[i] * exp(-(h-pressure_par2_[i])/pressure_par3_[i]);
  }
  else {
    // use power law
    P = P0_ * pow( pressure_par1_[i] + h / pressure_par2_[i], pressure_par3_[i]);
  }

  //cout << "height " << h << ": layer " << i << ", pressure " << P << endl;

  return P;
}

