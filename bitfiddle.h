/*
    Copyright 2003-2012 Patrik Jonsson, code@familjenjonsson.org,
    unless otherwise specified below.

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

/** \file 

    Various handy bit-fiddling functions.

*/

#ifndef __bitfiddle__
#define __bitfiddle__


/** Defines a bitmask of type T with the N lowest bits set to one. */
template<typename T, uint8_t N> struct bitmask {
  static const T mask = (T(1)<<N)-1;
};

/** Defines a bitmask of type T with the n lowest bits set to
    one. This is for when n is not a constant-expression. */
template<typename T> T bitmask_n(uint8_t n) {
  return (T(1)<<n)-1;
};


/** Defines the third dimension, ie the value among 012 that is not I
    or J. This makes use of the fact that in any permutation of 012,
    each bit is set exactly once. 
*/
template<uint8_t I, uint8_t J> struct meta_third_dim {
  static const uint8_t dim3 = '\x03' ^ I ^ J;
};

/** Runtime version of the third dimension function. */
inline uint8_t third_dim(uint8_t d1, uint8_t d2) {
  return '\x03' ^ d1 ^ d2;
};

/** This function swaps two N-bit chunks in data. The chunks start at
    s1 and s2 and must not overlap. Based on example in
    http://graphics.stanford.edu/~seander/bithacks.html 
*/
template<typename T, int N> 
void bitswap(T& data, uint8_t s1, uint8_t s2)
{
  // First make a temporary with the two bit ranges XORed against each other
  const T temp( ((data >> s1) ^ (data >> s2)) & bitmask<T, N>::mask);
  // Then XOR the bits with the temporary.
  data ^= ((temp << s1) | (temp << s2));
};


/** Swaps two values without using a temporary. Based on example in
    http://graphics.stanford.edu/~seander/bithacks.html I wonder
    whether this actually is faster than using a temporary. Note that
    this INCORRECT if a and b is the same value.
*/
template<typename T>
void xorswap(T& a, T&b)
{
  assert(&a!=&b);
  a ^= b; b ^= a; a ^= b;
}

/** Counts the number of bits set in an integer, also from the
    bithacks site. This takes the number of bits set iterations. */
inline uint8_t count_bits(uint32_t value)
{
 uint8_t c=0;
 for (; value; ++c)
   value &= value - 1; // clear the least significant bit set
 return c;
}

static const uint8_t MultiplyDeBruijnBitPosition[32] = 
{
  0, 9, 1, 10, 13, 21, 2, 29, 11, 14, 16, 18, 22, 25, 3, 30,
  8, 12, 20, 28, 15, 17, 24, 7, 19, 27, 23, 6, 26, 5, 4, 31
};

/** Finds the log-base2 of an integer. From the bithacks site. */
inline uint8_t log2(uint32_t value)
{
  value |= value >> 1; // first round down to one less than a power of 2 
  value |= value >> 2;
  value |= value >> 4;
  value |= value >> 8;
  value |= value >> 16;
  
  return MultiplyDeBruijnBitPosition[(uint32_t)(value * 0x07C4ACDDU) >> 27];
}

#endif
