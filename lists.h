// lists.h
//
// Declaration and inline defines of a linked list class
//
// Patrik Jonsson

/*
980506: init() was moved from public to private since initialization now
seems to work when compiling under egcs-1.0.
980507: Added a function list_base::isempty() that checks whether there are
any nodes inserted in the list.
980507: Added another iterator class, list_node_iter, which instead of
returning the items in the list returns the list_nodes. It is secure for
operations done on the returned list_node but not for any other.
*/

// $Id$
// $Log$
// Revision 1.2  1998/11/22 11:25:11  patrik
// Added a function to add a list_node directly to a list_base, without
// going via gettail(). I also realised that I don't think the lists are
// secure to multiple attempts to put them in lists, list corruption may
// result. This needs to be checked out.
//

#ifndef __lists__
#define __lists__

template<class T> class list_node;

// class list_base contains the head and tail pointers of the list
template<class T>
class list_base {
private:
  int destr;
  list_node<T> head;
  list_node<T> tail;

  void init(); // Init the pointers.

public:
  list_base();
  list_base(int);
  ~list_base();

  void setnondestructive() {destr=0;}; // Determines if the list_nodes should
  void setdestructive() {destr=1;};    // be deleted with the list_base

  list_node<T>* gethead();
  list_node<T>* gettail();
  list_node<T>* getfirst();
  list_node<T>* getlast();

  int isempty() const;
};


// class list_node contains a node in the list and a pointer to the
// thing (T) we're interested in.
template<class T>
class list_node {
  friend class list_base<T>;
 private:
  list_node* prev_node;
  list_node* next_node;
  T* item_ptr;

 public:
  list_node();
  list_node(T*);
  ~list_node();

  list_node* next() const;
  list_node* prev() const;
  T*    item() const;

  void insert(list_node*); // Inserts this before node in list
  void append(list_node*); // Inserts this after node in list
  void append(list_base<T>*); // Inserts this at the end of list_base
  void remove(); // Removes this from whatever list it is in
  int inlist() const; // Checks if this node is in a list
};


// Class list_iter contains pointers to a list_base and a list_node
// plus functions to loop through list and return items
template<class T>
class list_iter {
  //friend class list_base;
private:
  list_base<T>* l;
  list_node<T>* i;

public:
  list_iter(list_base<T>*);

  void init();   // Initialises iteration to start with first item
  T* iter();     // Returns next node in iteration, or null if over
};

// Class list_node_iter contains pointers to a list_base and a list_node
// plus functions to loop through list and return *the list_node*
// This is more perilous since the user might do something with the list using
// the node. It is implemented such that the user may remove the returned node
// from the list, but none other (ie can't fiddle with next node).
template<class T>
class list_node_iter {
  //friend class list_base;
private:
  list_base<T>* l;
  list_node<T>* i;

public:
  list_node_iter(list_base<T>*);

  void init();   // Initialises iteration to start with first node
  list_node<T>* iter();     // Returns next node in iteration, or null if over
};

  
// ***
// *** Inline definitions ***
// ***


// ***
// *** class list_base ***
// ***

// This initialises list pointers to an empty list
// Previous concerns about having this public no longer apply and list
// integrity is assured. If you still compile with gcc 2.7.2 
// (whoever does that) may run into trouble.
template<class T> inline void list_base<T>::init()
{
  head.prev_node = tail.next_node = 0;
  head.next_node = &tail;
  tail.prev_node = &head;
}

// Default constructor for list_base sets up an empty list
// and defaults to a destructive list
template<class T> inline list_base<T>::list_base()
{
  init();
  destr=1;
}

// Constructor for list_base sets up an empty list
// and set destructivity
template<class T> inline list_base<T>::list_base(int d)
{
  init();
  destr=d;
}

// Destructor deletes all nodes in list AND
// all items in nodes if destructive flag is set
// (The user needs to excercise care here to make sure that only 
// one list is set destructive and maintain other pointers if it is not.)
template<class T> list_base<T>::~list_base()
{
  list_node<T>* n = head.next_node;

  while(n!=&tail) {
    if(n->item()&&destr)
      delete n->item_ptr;
    n = n->next();
    delete(n->prev());
  }
}

// gethead returns pointer to the list head
template<class T> inline list_node<T>* list_base<T>::gethead()
{
  return &head;
}

// gettail returns pointer to the list tail
template<class T> inline list_node<T>* list_base<T>::gettail()
{
  return &tail;
}

// getfirst returns pointer to the first node
template<class T> inline list_node<T>* list_base<T>::getfirst()
{
  if(!isempty())
    return head.next();
  else
    return 0;
}

// getlast returns pointer to the last node
template<class T> inline list_node<T>* list_base<T>::getlast()
{
  if(!isempty())
    return tail.prev();
  else
    return 0;
}

// isempty return true if there are nodes in the list, and false
// if head is linked to tail and vice versa
template<class T> inline int list_base<T>::isempty() const
{
  if(head.next() == &tail) 
    // No items in list
    if(tail.prev() == &head)
      // and not this way either.
      return 1;
    else {
      // Empty one way or another - something's fucked
      cerr << "list_base::isempty() : List is corrupted!\n";
      return 0;
    }
  else
    return 0;
}

// ***
// *** class list_node ***
// ***

// Default constructor creates a node that doesn't point to an item
template<class T> inline list_node<T>::list_node()
{
  prev_node=next_node=0;
  item_ptr = 0;
}

// Constructor creates a node that points to thing
template<class T> inline list_node<T>::list_node(T* thing)
{
  prev_node=next_node=0;
  item_ptr = thing;
}

// The destructor makes sure that it correctly removes the node from the list
// so that list structure is not compromised
template<class T> inline list_node<T>::~list_node()
{
  if(inlist())
    remove();
}

// Returns pointer to next node in list
template<class T> inline list_node<T>* list_node<T>::next() const
{
  return next_node;
}

// Returns pointer to previous node in list
template<class T> inline list_node<T>* list_node<T>::prev() const
{
  return prev_node;
}

// Returns the item pointer
template<class T> inline T* list_node<T>::item() const
{
  return item_ptr;
}

// Inserts this before node l in list. If it is already in a list, it
// is removed first to ensure that lists don't get corrupted.
template<class T> inline void list_node<T>::insert(list_node* l)
{
  // Remove if it is in a list
  if( prev_node && next_node )
    remove();

  if( l->prev() ) {
    prev_node = l->prev();
    next_node = l;
    l->prev()->next_node = this;
    l->prev_node = this;
  }
  else
    cerr << " list_node::insert: Cannot insert before head of list\n";
}

// Inserts this after node l in list. If l is not the last node in list
// the rest of the list is appended after this.
template<class T> inline void list_node<T>::append(list_node* l)
{

  if(l->next_node) {
    prev_node = l;
    next_node = l->next();
    l->next()->prev_node = this;
    l->next_node = this;
  }
  else
    cerr << " list_node::append: Cannot append after tail of list\n";
}

// Inserts this at the end of the list in list_base, ie before list_base->tail
template<class T> inline void list_node<T>::append(list_base<T>* b)
{
  insert(b->gettail());
}

// Removes this from list. If this is not the last in the list,
// the list is rejoined.
template<class T> inline void list_node<T>::remove()
{
  if(prev_node&&next_node) {
    prev()->next_node = next_node;
    next()->prev_node = prev_node;
    next_node = prev_node = 0;
  }
  else
    cerr << " list_node::remove: Node is not in a list\n";
}

// Checks to see if this is in a list
template<class T> inline int list_node<T>::inlist() const
{
  return (next_node&&prev_node);
}

// ***
// *** class list_iter ***
// ***

// Constructor creates a list_iter pointing to list_base
template<class T> inline list_iter<T>::list_iter( list_base<T>* list )
{
  l = list;
  init();
}

// Initialises iteration pointer
template<class T> inline void list_iter<T>::init()
{
  i = l->gethead();
}

// Returns next item in list, or null if we have finished
template<class T> inline T* list_iter<T>::iter()
{
  if(i) {
    i=i->next();
    return i->item();
  }
  else
    return 0;
}


// ***
// *** class list_iter ***
// ***

// Constructor creates a list_node_iter pointing to list_base
template<class T> inline list_node_iter<T>::list_node_iter( list_base<T>* list)
{
  l = list;
  init();
}

// Initialises iteration pointer, note difference here from list_iter class,
// we are initially standing on the first node in the list, not the head node.
template<class T> inline void list_node_iter<T>::init()
{
  i = l->gethead()->next();
}

// Returns next node in list, or null if we have finished
template<class T> inline list_node<T>* list_node_iter<T>::iter()
{
  // i is already pointing to the node to be returned, but we already
  // now step to the next in case the user removes i from the list before
  // next iteration.
  if(i) {
    list_node<T>* temp = i;
    if( i != l->gettail() ) {
      i=i->next();
      return temp;
    }
    else
      return i=0;
  }
  else
    return 0;
}

#endif






