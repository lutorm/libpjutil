#ifndef __partition__
#define __partition__

#include <vector>
#include <algorithm>
#include <numeric>
#include "blitz/array.h"

/* Calculates the partition of the cost in the vector into npart
   partitions such that the maximum cost is minimized. This is a
   partition problem, which we can solve with a dynamic programming
   approach, inserting each divider progressively. (See Skiena's
   "Algorithm Design Manual".) This algorithm is O(npart*N^2). The
   elements in the returned vector points to first element in each of
   the npart partitions (the first element is always 0). */
template <typename T>
std::vector<size_t>
partition(int npart, const std::vector<T>& cost)
{
  const int N=cost.size();
  const int K=npart;

  // generate partial sum of cost
  std::vector<T> p;
  p.push_back(0);
  std::partial_sum(cost.begin(), cost.end(), std::back_inserter(p));
  
  // store the minimum cost of the (n,k) problem in a 2D array. The
  // indices are (n-1,k-1), which makes the k-index unit stride which is
  // advantageous since the inner loop is over k.
  blitz::Array<T, 2> M(N,K);
  blitz::Array<size_t, 2> d(N,K);

  // initialize array
  M(0, blitz::Range::all()) = cost[0];
  for(int i=1; i<N; ++i)
    M(i,0) = p[i+1];

  // the recurrence relation solves the problem for each larger k,
  // building on the solution from k-1.
  for(int n=1; n<N; ++n) {
    for(int k=1; k<K; ++k) {
      M(n,k) = blitz::huge(T());
      for(int i=0; i<n; ++i) {
	const T cost = std::max(M(i,k-1), p[n+1]-p[i+1]);
	if(M(n,k)>cost) {
	  M(n,k) = cost;
	  d(n,k) = i;
	}
      }
    }
  }

  // reconstruct the partition positions. 
  std::vector<size_t> parts;
  size_t n=N-1;
  for(int k=K-1; k>0; --k) {
    n=d(int(n), k); // can't index an Array with size_t currently.
    parts.push_back(n+1);
  }
  parts.push_back(0);
  std::reverse(parts.begin(), parts.end());

  return parts;
}


#endif

