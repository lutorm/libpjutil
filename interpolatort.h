// $Id$
/*
  Interpolation routines assume that x values are rising. If extrapolation
  is attempted all hell will break loose...
*/

#ifndef __interpolatort__
#define __interpolatort__

#include <fstream>
#include "blitz/array.h"
#include<string>
#include<vector>
#include<algorithm>
#include<exception>
#include "boost/shared_ptr.hpp"

template<typename T_dep, typename T_indep, int N>
class interpolator_base {
public:
  typedef blitz::TinyVector<int, N> T_index;
  typedef blitz::TinyVector<T_indep, N> T_coord;
protected:

  // axis coordinates for the grid
  // blitz::TinyVector<std::vector<T_indep>, N> points_indep;
  
  // tiny vector is UNUSABLE for this purpose, because operator []
  // const returns by value!!
  std::vector<T_indep> points_indep [N];
  
  // lower and upper limits of the valid interpolation range
  blitz::TinyVector<T_indep, N> lower, upper;

  // data points
  blitz::Array<T_dep, N> points_dep;

  // controls whether we clamp the value at teh boundary or throw
  bool no_clamp_;

  // this class is used to count the recursions of reduce
  template <int R> class counter_class {};
  
  template<int R>
  T_dep reduce (T_dep* v, const T_coord& f, const counter_class<R> ) const;
  T_dep reduce (T_dep* v, const T_coord& f, const counter_class<1> ) const;

  /// Finds the indices spanning the supplied point. Returned are the
  /// index and the fractional distance of the point within the bin.
  std::pair<T_index, T_coord> find_index (const T_coord&) const;
public:
  interpolator_base (bool no_clamp): points_dep (blitz::ColumnMajorArray<N> ()), 
			no_clamp_(no_clamp) {};
  interpolator_base (const blitz::TinyVector< std::vector<T_indep> , N>&,
		     bool no_clamp);
  // Reads interpolation data from file
  interpolator_base(const std::string&, bool no_clamp);

  // Set axis values in a specified dimension (may invalidate data
  // points if the number of independent points is changed)
  void setAxis (int, const std::vector<T_indep>&) ;
  // Sets all axis values (invalidate data) 
  void setAxes (const blitz::TinyVector< std::vector<T_indep> , N>&);

  // Retrieve data array
  const blitz::Array<T_dep, N>& data() const {return points_dep;};
  // Retrieve axes points
  const std::vector<T_indep>& axis(int i) const {
    assert(i<N);return points_indep[i];};

  void writedata(const std::string&); // Writes the interpolation table

  const blitz::TinyVector<T_indep, N>& lower_range () const {
    return lower;}
  const blitz::TinyVector<T_indep, N>& upper_range () const {
    return upper;}

  // exception thrown by the constructor if the file read fails
  class data_error {};
};

template<typename T_dep, typename T_indep, int N>
class interpolator: public interpolator_base<T_dep, T_indep, N> {
public:
  typedef interpolator_base<T_dep, T_indep, N> T_base;
  //typedef typename interpolator_base<T_dep, T_indep, N>::T_index T_index;
  typedef typename T_base::T_index T_index;
  //typedef typename interpolator_base<T_dep, T_indep, N>::T_coord T_coord;
  typedef typename T_base::T_coord T_coord;
  typedef typename T_base::template counter_class<N> counter_class;

  
  interpolator (bool allow_outside=false) :
    interpolator_base<T_dep, T_indep, N> (allow_outside) {};
  interpolator (const blitz::TinyVector< std::vector<T_indep> , N>& axes,
		bool allow_outside=false):
    interpolator_base<T_dep, T_indep, N> (axes, allow_outside) {};
  interpolator(const std::string& file, bool allow_outside=false):
    interpolator_base<T_dep, T_indep, N> (file, allow_outside) {};
  // Reads interpolation data from file

  void setPoint(const T_index& i, const T_dep& v){ // set a data point
    this->points_dep (i) = v;};

  void setPoints(blitz::Array<T_dep, N> points){ // sets all data points
    this->points_dep= points;};

  // Does the interpolation
  T_dep interpolate(const T_coord& p) const;
  BZ_DECLARE_MEMBER_FUNCTION_RET(interpolator,//<T_dep, T_indep, N>,
				 interpolate, T_dep);

  /// Just do a simple nearest-point lookup without interpolating.
  T_dep nearest(const T_coord& p) const;
  BZ_DECLARE_MEMBER_FUNCTION_RET(interpolator,//<T_dep, T_indep, N>,
				 nearest, T_dep);
};  

template<typename T_dep, int M, typename T_indep, int N>
class interpolator<blitz::Array< T_dep, M>, T_indep, N>:
  public interpolator_base<blitz::Array<T_dep, M>, T_indep, N>
{
  typedef blitz::Array<T_dep, M> T_point;
  typedef interpolator_base<T_point, T_indep, N> T_base;
public:
  typedef typename T_base::T_index T_index;
  typedef typename T_base::T_coord T_coord;
  //typedef typename interpolator_base<blitz::Array<T_dep, M>, T_indep, N>::T_index T_index;
  //typedef typename interpolator_base<blitz::Array<T_dep, M>, T_indep, N>::T_coord T_coord;
  
  interpolator (bool allow_outside=false) :
    interpolator_base<blitz::Array<T_dep, M>, T_indep, N> (allow_outside) {};
  interpolator (const blitz::TinyVector< std::vector<T_indep> , N>& axes,
		bool allow_outside=false):
    interpolator_base<T_dep, T_indep, N> (axes, allow_outside) {};

  void setPoint(const T_index& i, const T_point& v){ // set a data point
    this->points_dep (i).reference(v.copy());};

  void setPoints(blitz::Array<T_dep, N> points){ // sets all data points
    this->points_dep.reference(points.copy());};

  // Initializes the data points
  void initializePoints (const T_point& value);
  T_point interpolate(const T_coord& p) const;

  T_point nearest(const T_coord& p) const;
};  

/*
// Specialization for interpolating blitz arrays
template<typename T_dep, int M, typename T_indep, int N>
class interpolator<blitz::Array<T_dep, M>, T_indep, N> {
public:
  typedef blitz::TinyVector<int, N> T_index;
  typedef blitz::TinyVector<T_indep, N> T_coord;
  typedef blitz::Array<T_dep, M> T_point;
private:
  std::vector<T_indep> points_indep [N];
  
  // lower and upper limits of the valid interpolation range
  blitz::TinyVector<T_indep, N> lower, upper;

  // data points
  blitz::Array<T_dep, N+M> points_dep;

  // this class is used to count the recursions of reduce
  template <int R> class counter_class {};
  
  template<int R>
  T_dep reduce (T_dep* v, const T_coord& f, const counter_class<R> ) const;
  T_dep reduce (T_dep* v, const T_coord& f, const counter_class<1> ) const;

public:
  interpolator () {};
  interpolator (const blitz::TinyVector< std::vector<T_indep> , N>&);

  // Set axis values in a specified dimension (may invalidate data
  // points if the number of independent points is changed)
  void setAxis (int, const std::vector<T_indep>&) ;
  // Sets all axis values (invalidate data) 
  void setAxes (const blitz::TinyVector< std::vector<T_indep> , N>&);
  
  void setPoint(const T_index& i, const T_point& v){ // set a data point
    blitz::TinyVector<T_indep, N+M> lower (points_dep.lbound()),
      upper (points_dep.ubound());
    for (int j = 0; j < N; ++j) {
      lower = i [j];
      upper = i [j];
    }
    RectDomain<N+M> d (lower, upper);
    points_dep (d) = v;};

  // Initializes the data points (necessary for e.g. blitz arrays)
  void initializePoints (const T_dep& value);
  
  // Does the interpolation
  T_dep interpolate(const T_coord& p) const;
  void writedata(const std::string&); // Writes the interpolation table

  const blitz::TinyVector<T_indep, N>& lower_range () const {
    return lower;}
  const blitz::TinyVector<T_indep, N>& upper_range () const {
    return upper;}

  // exception thrown by the constructor if the file read fails
  class data_error {};
};
*/

// constructor takes data from file
template<typename T_dep, typename T_indep, int N>
interpolator_base<T_dep, T_indep, N>::interpolator_base(const std::string& filename, 
							bool no_clamp)
  // the old ndim_interpolator used column major arrays, so we'd
  // better duplicate that behavior here...
  : points_dep (blitz::ColumnMajorArray <N> ()), no_clamp_(no_clamp)
{
  using std::cerr;
  using std::endl;
  std::ifstream file(filename.c_str());
  
  if(!file.good()) {
    cerr << "Error opening interpolation data file: " << filename << '\n';
    throw data_error ();
  }
  
  // File good - let's hope data are ok now
  int ndim;
  file >> ndim;
  if (ndim != N) {
    cerr << "Error: file contains " << ndim << "-dimensional data" << endl;
    return;
  }

  T_index n_points;
  
  // Read number of points in each dimension
  for(int i=0;i<N;i++) {
    file >> n_points [i];
    points_indep [i].resize(n_points [i]);
  }

  // Read the grid values in each dimension
  for(int i=0;i<N;i++) {
    for(int j=0;j<points_indep [i].size();j++) {
      file >> points_indep[i][j];
      if (j > 0 && points_indep [i] [j] <= points_indep [i] [j-1]) {
	cerr << "Error: Independent values are not monotonically increasing!"  << endl;
	throw data_error ();
      }
    }
    lower [i] = points_indep [i].front();
    upper  [i] = points_indep [i].back ();
  }
    
  // Allocate and read dependent values
  points_dep.resize(n_points);
  for(typename blitz::Array<T_dep, N>::iterator i =points_dep.begin();
      i !=points_dep.end(); ++i)
    file >> (*i);
  
}

template<typename T_dep, typename T_indep, int N>
interpolator_base<T_dep, T_indep, N>::interpolator_base (const blitz::TinyVector<
					       std::vector<T_indep> , N >& a,
							 bool no_clamp):
  points_dep (blitz::ColumnMajorArray<N> ()), no_clamp_(no_clamp)
{
  setAxes (a) ;
}

template<typename T_dep, typename T_indep, int N>
void
interpolator_base<T_dep, T_indep, N>::setAxes (const blitz::TinyVector< std::vector<T_indep> , N >& a)
{
  // points_indep = a;
  T_index n_points ;
  for (int i = 0 ; i < N ; ++i) {
    points_indep [i] = a [i];
    n_points [i] = a [i].size();
    lower [i] = points_indep [i].front(); 
    upper [i] = points_indep [i].back ();
  }
  
  points_dep.resize(n_points);
}

template<typename T_dep, typename T_indep, int N>
void interpolator_base<T_dep, T_indep, N>::setAxis (int n ,
					       const std::vector<T_indep>& points)
{
  if ((n < 0) || (n > N))
    throw data_error ();
  
  // See if we need to resize the array
  T_index i = points_dep.extent();
  if (i [n] != points.size()) {
    i [n] = points.size();
    points_dep.resizeAndPreserve(i) ;
  }
  points_indep [n] = points ;
  lower [n] = points_indep [n].front(); 
  upper [n] = points_indep [n].back ();
}

template<typename T_dep, int M, typename T_indep, int N>
void interpolator<blitz::Array< T_dep, M>, T_indep, N>::initializePoints (const T_point& value)
{
  //using interpolator_base<T_dep, T_indep, N>::points_dep;
  for (typename blitz::Array< T_point, N>::iterator i = this->points_dep.begin();
       i != this->points_dep.end(); ++i)
    i->reference (value.copy());
}


template<typename T_dep, typename T_indep, int N>
std::pair<typename interpolator_base<T_dep, T_indep, N>::T_index,
						typename interpolator_base<T_dep, T_indep, N>::T_coord>
interpolator_base<T_dep, T_indep, N>::find_index(const T_coord& x) const
{
  std::pair<T_index, T_coord> return_value;
  T_index up;
  // find upper index of the 2 grid points involved
  for(int k=0;k<N;++k) {
    T_indep xk = x [k];
    const std::vector<T_indep>& pik= points_indep [k];
    int u;

    if (xk < pik.front()) {
      if (no_clamp_) {
	std::cerr << "Error: Extrapolation requested at " << xk << " < " << pik.front() << std::endl;
	throw 0;
      }
      else {
	// clamp
	xk=pik.front();
      }
    }
    if (xk > pik.back()) {
      if (no_clamp_) {
	std::cerr << "Error: Extrapolation requested at " << xk << " > " << pik.back() << std::endl;
	throw 0;
      }
      else {
	// clamp
	xk=pik.back();
      }
    }

    assert(xk>=pik.front());
    assert(xk<=pik.back());

    if(xk!=pik [0]) {
      
      const typename std::vector<T_indep>::const_iterator b = pik.begin(); 
      const typename std::vector<T_indep>::const_iterator e = pik.end (); 
      const typename std::vector<T_indep>::const_iterator ui =
        std::lower_bound (b, e, xk);
      
      assert (ui > b);
      //|| (xk >= pik [0])) ;
      assert (ui < e);
      u = ui - b;
    }
    else
      // This is a special case because lower_bound won't work
      u = 1;

    assert (u >= 1);
    assert (u <=pik.size());

    up [k] = u;
    const double piiji = pik[u-1];
    return_value.second [k]=(xk-piiji)/(pik[u]-piiji);
    assert(return_value.second[k]>=0);
    assert(return_value.second[k]<=1);
  }
  return_value.first = up - 1;
  return return_value;
}

// do a linear interpolation
template<typename T_dep, typename T_indep, int N>
T_dep
interpolator<T_dep, T_indep, N>::interpolate(const T_coord& x) const
{
  typename std::pair<T_index, T_coord> indf = find_index (x);
  // Our point is now between indices low and up

  // Extract sub array and reduce

  // Since we know the size of the buffer required for the sub array,
  // we don't need to allocate it dynamically
  const int npairs = 1 << N;
  T_dep buffer [npairs];
  T_index m;

  for (int i = 0; i < npairs; ++i) {
    for (int k = 0; k < N; ++k) {
      m [k] = indf.first [k] + ((i & (1 << k)) >> k);
    } 
    buffer [i] = points_dep (m);
  }

  return T_dep (
		this->reduce (buffer, indf.second, 
			      typename T_base::template counter_class<N>()
		  ));
}


// do a nearest-point lookup
template<typename T_dep, typename T_indep, int N>
T_dep
interpolator<T_dep, T_indep, N>::nearest(const T_coord& x) const
{
  typename std::pair<T_index, T_coord> indf = find_index (x);
  // The requested point is between indf.first and indf.first+1
  // with fractional distance indf.second

  // If fractional distance < 0.5 then we use lower, otherwise upper
  T_index ind;
  for (int i=0; i<N; ++i)
    ind[i] = (indf.second[i]<0.5) ? indf.first[i] : indf.first[i]+1;
  //const T_index i(where(indf.second<0.5, indf.first, indf.first+1));

  return points_dep(ind);
}


template<typename T_dep, int M, typename T_indep, int N>
typename interpolator<blitz::Array< T_dep, M>, T_indep, N>::T_point
interpolator<blitz::Array< T_dep, M>, T_indep, N>::interpolate (const T_coord& x) const
{
  typename std::pair<T_index, T_coord> indf = find_index (x);
  // Our point is now between indices low and up

  // Extract sub array and reduce

  // Since we know the size of the buffer required for the sub array,
  // we don't need to allocate it dynamically
  const int npairs = 1 << N;
  T_point buffer [npairs];
  T_index m;

  for (int i = 0; i < npairs; ++i) {
    for (int k = 0; k < N; ++k) {
      m [k] = indf.first [k] + ((i & (1 << k)) >> k);
    }
    buffer [i].reference (points_dep (m).copy());
  }

  return T_point (this->reduce (buffer, indf.second, 
				typename T_base::template counter_class<N>()
				//interpolator_base<blitz::Array< T_dep, M>, T_indep, N>::counter_class<N>()
));
}


// do a nearest-point lookup
template<typename T_dep, int M, typename T_indep, int N>
typename interpolator<blitz::Array< T_dep, M>, T_indep, N>::T_point
interpolator<blitz::Array< T_dep, M>, T_indep, N>::nearest (const T_coord& x) const
{
  typename std::pair<T_index, T_coord> indf = find_index (x);
  // The requested point is between indf.first and indf.first+1
  // with fractional distance indf.second

  // If fractional distance < 0.5 then we use lower, otherwise upper
  T_index ind;
  for (int i=0; i<N; ++i)
    ind[i] = (indf.second[i]<0.5) ? indf.first[i] : indf.first[i]+1;
  //const T_index i(where(indf.second<0.5, indf.first, indf.first+1));

  return points_dep(ind);
}


template<typename T_dep, typename T_indep, int N>
template<int R>
inline T_dep
interpolator_base<T_dep, T_indep, N>::reduce(T_dep*v, const T_coord& f,
					counter_class<R> ) const
{
  // we can reduce the arrays in place
  const int a=1<<(R-1);
  const int b=N-R;

  for(int i=0;i<a;++i) {// Collapse first dimension
    const int j=(i<<1);
    v[i] = (v[j+1]-v[j])*f[b] + v[j];
  }

  // recurse
  return T_dep (reduce(v,f, counter_class<R-1> ()));
}

template<typename T_dep, typename T_indep, int N>
inline T_dep
interpolator_base<T_dep, T_indep, N>::reduce(T_dep*v, const T_coord& f,
					counter_class<1> ) const
{
  // End of recursion
  return T_dep((v[1]-v[0])*f[N-1] +v [0]);
}

/// Reads the old interpolator files.
template<typename T>
typename boost::shared_ptr<interpolator<T, T, 1> >
read_old_interpolator (const std::string& file) 
{
  typedef interpolator<T, T, 1> T_interpolator;

  std::ifstream infile (file.c_str ()) ;
  int n = 0;
  infile >> n ;
  if (!infile.good())
    // probably could not open file
    throw typename interpolator_base<T, T, 1>::data_error ();
  
  std::vector<T> input ((std::istream_iterator<T> (infile)),
			std::istream_iterator<T> ()) ;

  if ((input.size()% 2) || (input.size()== 0) )
    // input file should have two columns
    throw typename interpolator_base<T, T, 1>::data_error ();
  
  std::vector<T> x, y;
  for (typename std::vector<T>::iterator i = input.begin() ; 
       i != input.end() ; ) {
    x.push_back(*i++);
    assert (i != input.end());
    y.push_back(*i++);
  }
  assert (x.size() == y.size());
  boost::shared_ptr<T_interpolator> inter(new T_interpolator) ;
  inter->setAxis(0, x) ;
  for (int i = 0; i < x.size() ; ++i) {
    inter->setPoint(i, y [i]);
  } 

  return inter;
}


#endif

