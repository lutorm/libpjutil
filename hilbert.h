/*
    Copyright 2003-2012 Patrik Jonsson, code@familjenjonsson.org

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

/** \file 

    Routines for determining the position of a point in space on a
    Hilbert curve. Based on the algorithm described in
    "Vertex-labeling algorithms for the Hilbert space filling curve"
    by John J. Bartholdi, III and Paul Goldsman, January 11, 2000.

*/

#ifndef __hilbert__
#define __hilbert__

#include "blitz/array.h"
#include "bitfiddle.h"

#ifdef HAVE_BOOST_SERIALIZATION
#include <boost/serialization/serialization.hpp>
#endif

namespace hilbert {
  class hilbert_code_2D;
  class hilbert_cell_2D;

  class cell_code ;
  class qpoint;
  template <typename> class octree_cell;
  class hilbert_state;
  class c_state;

  hilbert_code_2D encode_point(const blitz::TinyVector<double, 2>&, int);
  cell_code encode_point(const blitz::TinyVector<double, 3>&, int);
  cell_code encode_point(const qpoint&);

  std::ostream& operator<<(std::ostream&, const hilbert_code_2D&);
  std::ostream& operator<<(std::ostream&, const cell_code&);
  std::ostream& operator<<(std::ostream&, const hilbert_state&);
  std::ostream& operator<<(std::ostream&, c_state);
  template<typename T>
  std::ostream& operator<<(std::ostream&, const octree_cell<T>&);
  std::ostream& operator<<(std::ostream&, const qpoint&);
}


/** Class representing the code of a point on a n-level 2D Hilbert curve. */
class hilbert::hilbert_code_2D {
public:
  /// The level of the Hilbert curve.
  unsigned int n_;

  /// The hilbert code (interpreted as a base-4 number between 0-1
  /// with n digits to the right of the decimal (quadral) point).
  unsigned long c_;

  hilbert_code_2D (): c_ (0), n_ (0) {} ;
  hilbert_code_2D (unsigned long cc, unsigned int nn): c_ (cc), n_ (nn) {};

  /** Right-appends the digit i to the Hilbert code. */
  void add_right (unsigned int i) {
    assert(n_ < sizeof(unsigned long)*4);
    assert(i < 4);
    c_ = (c_<<2) | i;
    ++n_;
  }

  
};

/** Class representing a Hilbert cell in 2D. This is a Hilbert code
    along with the 4 vertices of the cell which are used in the
    vertex-labeling algorithm. */
class hilbert::hilbert_cell_2D {
public:
  typedef blitz::TinyVector<double, 2> T_point;

  /// The Hilbert code of the current cell
  hilbert_code_2D code_;

  /// The dimensions of the axes_ (must be a permutation of 01)
  blitz::TinyVector<int, 2> dims_;

  /// The base vertex (a in the BH paper)
  T_point a_;

  /** The axes lengths of the cells. These are by construction the
      distances between point a and points bd. Point c is implicit, it
      is always the vector sum b+d. */
  T_point axes_;

  /** Initializes a Hilbert cell to a 0th order Hilbert curve (i.e. a
      single cell). The cell must be axis-aligned. */
  hilbert_cell_2D (const T_point& a, const T_point& axes) :
    a_(a), axes_(axes), dims_(0,1), code_() {};
  
  /** Transforms the cell into the subcell that contains point p. */
  void descend(const T_point& p);
};


class hilbert::cell_code {
public:
  // The maximal tree level that we can encode
  const static uint8_t maxlevel_ = 8*sizeof(uint64_t)/3;

  /// The level of the octree
  uint8_t n_;

  /** The cell code (interpreted as a base-8 number between 0-1
      with n digits to the right of the decimal (octal?) point in the
      style of B&H00.) This means that for a level n octree, the
      number is in the range [0, 8^N[. */
  uint64_t c_;

  cell_code (): c_ (0), n_ (0) {} ;
  cell_code (uint64_t cc, uint8_t nn): c_ (cc), n_ (nn) {};

  uint64_t code() const { return c_; };
  uint8_t level() const { return n_; };

#ifdef HAVE_BOOST_SERIALIZATION
  friend class boost::serialization::access;
  /// Serialization support.
  template<class T_arch>
  void serialize(T_arch& ar, const unsigned int version) {
    ar & n_ & c_;
  };
#endif

  /** Equality implies both level and code are equal. */
  bool operator==(const cell_code& rhs) const {
    return c_==rhs.c_ && n_==rhs.n_; };
  bool operator!=(const cell_code& rhs) const {
    return c_!=rhs.c_ || n_!=rhs.n_; };
  /** Less than for codes with different levels implies that the
      higher-level cell truncated to the lower level is less
      than. This defines an ordering where <> results in "neither
      contained within the other", which is useful. */
  bool operator<(const cell_code& rhs) const {
    const unsigned int minlevel =std::min(n_, rhs.n_);
    return (c_>>(3*(n_-minlevel))) < (rhs.c_>>(3*(rhs.n_-minlevel))); };
  bool operator<=(const cell_code& rhs) const {
    const unsigned int minlevel =std::min(n_, rhs.n_);
    return (c_>>(3*(n_-minlevel))) <= (rhs.c_>>(3*(rhs.n_-minlevel))); };

  /** Right-appends the digit i to the Hilbert code. */
  const cell_code& add_right (uint8_t i) {
    assert (i<8);
    assert(level() < maxlevel_);

    c_ = (c_<<3) | i;
    ++n_;
    return *this;
  }

  /** Truncates the Hilbert code to a new level below the
      current. This is equivalent to just truncating i digits in the
      Hilbert index, so e.g. 0.523 -> 0.5. If the code is already that
      level or below, nothing is done.  */
  const cell_code& truncate(uint8_t newlevel) { 
    if(n_>newlevel) {
      c_ >>= (3*(n_-newlevel)); n_=newlevel; }
    return *this; };

  /** Truncates the code by one digit. */
  const cell_code& truncate() { return truncate(level()-1); };

  /** Extends the level of the Hilbert code to a higher level,
      rounding up. This is equivalent to adding '7's to the Hilbert
      index, so e.g. 0.523 -> 0.52377. */
  const cell_code& extend_high(uint8_t newlevel) { 
    while (n_<newlevel) add_right(7); 
    return *this; };

  /** Extends the level of the Hilbert code to a higher level,
      rounding down. This is equivalent to adding '0's to the Hilbert
      index, so e.g. 0.523 -> 0.52300. */
  const cell_code& extend_low(uint8_t newlevel) { 
    while (n_<newlevel) add_right(0); 
    return *this; };
  
  /** Extracts the octant for the specified level. Legal levels are
      between 1 and level(). Extracting level 0 is not meaningful
      since it's always 0. */
  uint8_t extract_level(uint8_t l) const {
    assert(l<=level()); assert(l>0);
    return ((code()>>(3*(level()-l)))&0x07); };

  /** Checks whether the specified point is inside this point. This
      can only be true if the level of code is at least as high as
      this, and then the code must be identical to this up to our level. */
  bool contains(const cell_code& c) const {
    if(c.level()>=level()) {
      return c.code()>>(3*(c.level()-level())) == this->code();
    }
    else 
      return false;
  };

  /** Pre-increment operator increases the code by one (at current
      level). Note that this may carry.  */
  const cell_code& operator++() { ++c_; return *this; };
};


/** A quantized 3-vector type that stores each component in a 32-bit
    integer. The components are stored as numbers with variable
    numbers of bits, where a point with n bits uniquely defines a cell
    in a n-level octree. */
class hilbert::qpoint {
public:
  typedef blitz::TinyVector<uint32_t, 3>  T_pos;
  typedef blitz::TinyVector<uint8_t, 3>  T_pbits;
private:
  uint8_t n_;

  T_pos p_;

  /** Turns a double in the range 0-1 into the integer representation
      by converting to the range 1-2 and extracting n bits of the
      mantissa. Values outside the allowed range are truncated to the
      nearest allowed value. */
  static uint32_t quantize(double d, uint8_t n=cell_code::maxlevel_) {
    d+=1;
    if(d<=1) return 0;
    if(d>=2) return bitmask_n<uint64_t>(n);
    assert(d>1); assert(d<2);
    // mantissa for iee754 double is 52 bits, we want the maxlevel MSBs of that
    uint64_t& dd = *reinterpret_cast<uint64_t*>(&d);
    return (dd>>(52-n)) & bitmask_n<uint64_t>(n);
  };

#ifdef HAVE_BOOST_SERIALIZATION
  friend class boost::serialization::access;
  /// Serialization support.
  template<class T_arch>
  void serialize(T_arch& ar, const unsigned int version) {
    ar & n_ & p_;
  };
#endif

public:
  /** Default constructor sets position to be the level-0 (root)
      node. */
  qpoint() : n_(0), p_(0,0,0) {};

  qpoint(uint32_t x, uint32_t y, uint32_t z, uint8_t n) :
    n_(n), p_(x,y,z) {};

  qpoint(const T_pos& p, uint8_t n) :
    n_(n), p_(p) {};

  qpoint(const blitz::TinyVector<double, 3>& p, uint8_t n) :
    n_(n), p_(quantize(p[0], n), quantize(p[1], n), quantize(p[2], n)) {};

  /** By default we convert a vec3d to maximal resolution. */
  qpoint(const blitz::TinyVector<double, 3>& p) :
    n_(cell_code::maxlevel_), 
    p_(quantize(p[0], n_), quantize(p[1], n_), quantize(p[2], n_)) {};

  /** Back-convert a qpoint into a 3d vector. */
  operator blitz::TinyVector<double, 3>() const {
    return blitz::TinyVector<double, 3>
      (unquantize(p_[0], level()),
       unquantize(p_[1], level()),
       unquantize(p_[2], level())); };

  /** Turns an integer representation back into a double. This is done
      just by or-ing the number into the mantissa, shifted correctly. */
  static double unquantize(uint32_t q, uint8_t n) {
    if(q==0) return 0;
    double d(1);
    uint64_t& dd = *reinterpret_cast<uint64_t*>(&d);
    assert((dd&bitmask_n<uint64_t>(52))==0);
    dd |= uint64_t(q)<<(52-n);
    return d-1;
  };


  const T_pos& pos() const { return p_; };
  uint8_t level() const { return n_; };

  bool operator==(const qpoint& rhs) const {
    return (n_==rhs.n_) && all(p_==rhs.p_); };
  bool operator!=(const qpoint& rhs) const {
    return (n_!=rhs.n_) || any(p_!=rhs.p_); };

  uint32_t operator[](int i) const { return p_[i]; };
  uint32_t& operator[](int i) { return p_[i]; };

  qpoint& operator<<=(int i) {
    p_ <<= i; n_+=i; return *this; };
  qpoint& operator>>=(int i) {
    p_ >>= i; n_-=i; return *this; };
  qpoint& operator&=(uint32_t v) {
    p_ &= v; return *this; };
  
  qpoint operator<<(int i) const {
    qpoint temp(*this); temp <<=i; return temp; };
  qpoint operator>>(int i) const {
    qpoint temp(*this); temp >>=i; return temp; };
  qpoint operator&(uint32_t v) const {
    qpoint temp(*this); temp &= v; return temp; };

  /** Adds another bit to the vector by shifting in bit 0 of the
      supplied components. */
  void add_right(uint8_t x, uint8_t y, uint8_t z) {
    add_right(T_pbits(x,y,z)); };

  void add_right(T_pbits pos) {
    assert(blitz::all(pos<2));
    p_<<=1;
    p_ |= pos;
    ++n_;
  };

  /** Truncates the qpoint to a new level below the current. This is
      equivalent to just truncating i digits. */
  const qpoint& truncate(uint8_t newlevel) { 
    if(n_>newlevel) {
      p_ >>= (n_-newlevel); n_=newlevel; }
    return *this; };

  /** Extends the level of the qpoint to a higher level, rounding
      up. This is equivalent to adding '1's to all 3 axes, so
      e.g. (0.11, 0.00, 0.01) -> (0.1111, 0.0011, 0.0111). */
  const qpoint& extend_high(uint8_t newlevel) { 
    while (n_<newlevel) add_right(1,1,1);
    return *this; };

  /** Extends the level of the qpoint to a higher level, rounding
      down. This is equivalent to adding '0's to all 3 axes, so
      e.g. (0.11, 0.00, 0.01) -> (0.1100, 0.0000, 0.0100). */
  const qpoint& extend_low(uint8_t newlevel) { 
    while (n_<newlevel) add_right(0,0,0);
    return *this; };

  /** Extracts the pbits for the specified level. Legal levels are
      between 1 and level(). Extracting level 0 is not meaningful
      since it's always 0. */
  T_pbits extract_level(uint8_t l) const {
    assert(l<=level()); assert(l>0);
    return T_pbits((pos()>>(level()-l))&0x01); };

  /** Checks whether the specified point is inside this point. This
      can only be true if the level of qp is at least as high as
      this, and then qp must be identical to this up to our level. */
  bool contains(const qpoint& qp) const {
    if(qp.level()>=level()) {
      return qp>>(qp.level()-level()) == *this;
    }
    else 
      return false;
  };

  /** Pre-increment operator adds one to all numbers. Applying this
      from a level-0 state is the way to set it to end, which is
      (1,1,1). */
  qpoint& operator++() {++p_[0]; ++p_[1]; ++p_[2]; return *this; };
};


/** This class represents the extra information associated with an
    octree cell to be able to perform a Hilbert ordering. This
    information consists of a permutation of the dimensions and the
    direction of the 3 axes, which can be encoded in one byte. For an
    octree traversal stack this information needs to be saved at each
    level, so it's good to have a compact representation. 

    The data is stored as follows: Bits 0-1 stores the first
    dimension, 2-3 the second. Because we know it is a permutation of
    012, there is no need to store the third dimension. Then bits 4-6
    are the signs of the three axes.

    Apparently there is no way to encode a uint8_t literal except as a
    character constant, hence the use of those.  */
class hilbert::hilbert_state {
  uint8_t data_;

public:

  hilbert_state(uint8_t dim1, uint8_t dim2, 
		bool a1, bool a2, bool a3) {
    write_data(dim1, dim2, a1, a2, a3); };

  hilbert_state() { write_data(0, 1, 0, 0, 0); };

  /** Assembles the data byte from the specified dimensions and axes. */
  void write_data(uint8_t dim1, uint8_t dim2, 
		  uint8_t a1, uint8_t a2, uint8_t a3) {
    assert(dim1<3); assert(dim2<3); assert(dim1!=dim2);
    data_ = dim1 | (dim2<<'\x02') |
      (a1<<'\x04') | (a2<<'\x05') | (a3<<'\x06'); };

  /** Return the dimension mapped to i. For dimension 0 and 1 we just
      return the corresponding bits. For dimension 2, we have to
      calculate it but this is easy because in a permutation of 012
      both bits are set exactly once. */
  uint8_t dim(uint8_t i) const {
    assert(i<3);
    if(i<2)
      return (data_>>(i<<'\x01')) & '\x03';
    else
      return ('\x03' ^ data_ ^ (data_>>'\x02')) & '\x03';
  };

  /** Returns 1 or 0 depending on the axis direction, where 0 is a
      positive axis direction. The directions are stored in bits
      4-6. */
  uint8_t axis(uint8_t i) const {
    assert(i<3);
    return ((data_ >> '\x04') >> i) & '\x01'; };

  /** Swaps dimensions d1 and d2 in the ordering. This affects both
      dims_ and axes_. The logic is easier if d1<d2, so if they are
      ordered otherwise we swap them. */
  void swap_dims(uint8_t d1, uint8_t d2) {
    assert(d1!=d2); assert(d1<3); assert(d2<3);
    if(d2<d1) xorswap(d1,d2);

    if(d2==2) {
      // if d2 is 2 we compute that value and then put it in d1
      const uint8_t dim2=dim(d2);
      data_ &= ~('\x03' << (d1 << '\x01'));
      data_ |= dim2<< (d1 << '\x01');
    }
    else {
      // this means we swap the locations of dims 0 and 1. 
      assert(d1==0); assert(d2==1);
      bitswap<uint8_t, 2>(data_, '\x00', '\x02');
    }

    // swap axis bits
    bitswap<uint8_t, 1>(data_, '\x04'|d1, '\x04'|d2);
  };

  /** Reorders the dimensions so the new order is (d1, d2, d3). */
  void reorder_dims(uint8_t d1, uint8_t d2, uint8_t d3) {
    assert(d1<3); assert(d2<3); assert(d1!=d2);
    // just rewrite the data from the extracted values
    write_data(dim(d1), dim(d2), axis(d1), axis(d2), axis(d3)); };

  /** Flips the direction of the selected axis. */
  void flip_axis(uint8_t i) {
    data_ ^= ('\x01'<< '\x04') <<i; };

  blitz::TinyVector<uint8_t, 3> octant2pos(uint8_t octant) const;

  void descend(uint8_t octant);

  /** Returns the state vector resulting from descending into the
      subcell in the specified octant. */
  hilbert_state descendant(uint8_t octant) const {
    hilbert_state h(*this); h.descend(octant); return h; };

  /** Determine which octant of the cell a point is in (assuming it is
      in the cell). Since if it is in the cell, we know that the
      positions are identical to the precision of the cell, this only
      depends on the first higher-precision bit of the point. It is
      these bits that are supplied in the argument. */
  uint8_t determine_octant(blitz::TinyVector<uint8_t,3> pbits) const {

    // The relevant question is whether the point is different from
    // the octant that the base vertex resides in. Since the base
    // vertex is determined by the axis signs, what matters is whether
    // the bits are different from the axis bits.
    const uint8_t dim0=dim(0);
    const uint8_t dim1=dim(1);

    const uint8_t d0 = pbits[dim0] ^ axis(0);
    const uint8_t d1 = pbits[dim1] ^ axis(1);
    const uint8_t d2 = pbits[third_dim(dim0,dim1)] ^ axis(2);

    // The d's now correspond to the index of the point in the
    // coordinate system hiven by the Hilbert axes. The only
    // difference between the Hilbert ordering and normal ordering is
    // the conversion from these indices to the octant. (And of course
    // the fact that for a normal ordering, the axes never change.)

    uint8_t oct = d2<<2 | (d1<<1 ^ d2<<1) | (d0^d1^d2);
    // normal ordering would be: oct = d2<<2 | d1<<1 | d0;

    assert(oct<8);
    return oct;
  };
};


/** This class represents a C-ordering of cells. */
class hilbert::c_state {
public:
  blitz::TinyVector<uint8_t, 3> octant2pos(uint8_t octant) const {
    blitz::TinyVector<uint8_t, 3> p;
    p[2] = octant & '\x01';
    p[1] = (octant & '\x02') >> 1;
    p[0] = (octant & '\x04') >> 2;
    return p;
  };

  uint8_t determine_octant(blitz::TinyVector<uint8_t, 3> pbits) const {
    return pbits[2] | (pbits[1]<<1) | (pbits[0]<<2);
  };

  /** Descending is a no-op since a c_state has no state. */
  void descend(uint8_t) const {};
};


/** Class representing a Hilbert cell in 3D. This is a Hilbert code, a
    qpoint and a hilbert_state. */
template <typename state_type>
class hilbert::octree_cell {
public:
  typedef qpoint T_point;
  typedef state_type T_state;

private:

  T_state s_;

  /// The base vertex (a in the BH paper). Each component is stored as
  /// a 0.nnn with level digits in the LSBs.
  T_point a_;

  /// The Hilbert code of the current cell
  cell_code code_;

public:
  /** Initializes a Hilbert cell to a 0th order Hilbert curve (i.e. a
      single cell). Since the geometry is always assumed to be the
      unit cube, the base vertex is always the origin and the axes are
      012. */
  octree_cell () :
    a_(0,0,0,0), s_(), code_() {};

  /** Transforms the cell into the subcell that contains point p. */
  void descend(const T_point& p);

  /** Transforms the cell into the subcell that contains the index
      given by the three components. */
  void descend(const blitz::TinyVector<uint8_t, 3>& pbits);

  /** Transforms the cell into the subcell that contains octant. */
  void descend(uint8_t octant);

  const T_point& position() const { return a_; };
  const cell_code& code() const { return code_; };
  const T_state& state() const { return s_; };
};


template <typename state_type>
void hilbert::octree_cell<state_type>::descend (const T_point& p)
{
  // The point is supposed to be in the cell, so the level() MSBs must
  // be identical to the base vertex.
  assert(T_point(p).truncate(code().level())==position());

  // Get the next-level bits of p
  blitz::TinyVector<uint8_t, 3> pbits (p.extract_level(code_.level()+1));

  descend(pbits);
}

template <typename state_type>
void 
hilbert::octree_cell<state_type>::descend (const blitz::TinyVector<uint8_t, 3>& pbits)
{
  // get the octant
  const uint8_t octant = s_.determine_octant(pbits);

  // update code
  code_.add_right(octant);

  // update state
  s_.descend(octant);

  a_.add_right(pbits);
}

template <typename state_type>
void hilbert::octree_cell<state_type>::descend (uint8_t octant)
{
  // update code
  code_.add_right(octant);

  // update state
  s_.descend(octant);

  a_.add_right(s_.octant2pos(octant));
}


/** Outputs the state of a Hilbert cell, for debugging purposes. */
template <typename state_type>
std::ostream&
hilbert::operator<<(std::ostream& os, const octree_cell<state_type>& c)
{
  os << "Cell code: " << c.code()
     << " state: " << c.state()
     << " base vertex: " << c.position();
  return os;
}



/** Computes the position of the octant given the Hilbert state. This
    is used when computing the 3D point corresponding to a Hilbert
    code. */
inline blitz::TinyVector<uint8_t, 3>
hilbert::hilbert_state::octant2pos(uint8_t octant) const
{
  blitz::TinyVector<uint8_t, 3> pbits;
  // We start by setting the pbits to reflect the octant of the base
  // vertex.  We do this based on the axis values, since a positive
  // axis value means the base vertex is on the lower end of the cell,
  // and vice versa.
  const uint8_t d0 = dim(0);
  const uint8_t d1 = dim(1);
  const uint8_t d2 = third_dim(d0, d1);

  pbits[d0] = axis(0);
  pbits[d1] = axis(1);
  pbits[d2] = axis(2);

  // Now we need to shift the base vertex depending on the octant
  switch(octant) {
  case 0:
    // no vertex shift
    break;
    
  case 1:
    // base vertex flips dim 0
    pbits[d0] ^= 1;
    break;

  case 2:
    // base vertex flips dim 0 and 1
    pbits[d0] ^= 1;
    pbits[d1] ^= 1;
    break;

  case 3:
    // base vertex flips dim 1 only
    pbits[d1] ^= 1;
    break;

  case 4:
    // base vertex flips dim 1 and 2
    pbits[d1] ^= 1;
    pbits[d2] ^= 1;
    break;

  case 5:
    // base vertex flips is all dims
    pbits[d0] ^= 1;
    pbits[d1] ^= 1;
    pbits[d2] ^= 1;
    break;

  case 6:
    // base vertex flips in dim 0 and 2
    pbits[d0] ^= 1;
    pbits[d2] ^= 1;
    break;

  case 7:
    // base vertex flips in dim 2 only
    pbits[d2] ^= 1;
    break;

  default:
    assert(0);
  }
  return pbits;
}


/** Updates the state of the Hilbert cell to the result of descending
    into the specified octant. The operations were deduced from Figs
    2.8 & 2.9 in BG00.  */
inline void hilbert::hilbert_state::descend (uint8_t octant)
{
  switch(octant) {
  case 0:
    // d0' = d2, d1' = d1, d2' = d0
    swap_dims(0,2);
    break;

  case 1:
    // d0' = d0, d1' = d2, d2' = d1
    swap_dims(1,2);
    break;

  case 2:
    // d0' = d0, d1' = d1, d2' = d2
    break;

  case 3:
    // d0' = -d2, d1' = -d0, d2' = d1
    flip_axis(0);
    flip_axis(2);
    reorder_dims(2,0,1);
    break;

  case 4:
    // d0' = d2, d1' = -d0, d2' = -d1
    flip_axis(0);
    flip_axis(1);
    reorder_dims(2,0,1);
    break;

  case 5:
    // d0' = d0, d1' = d1, d2' = d2
    break;

  case 6:
    // d0' = d0, d1' = -d2, d2' = -d1
    flip_axis(1);
    flip_axis(2);
    swap_dims(1,2);
    break;

  case 7:
    // d0' = -d2, d1' = d1, d2' = -d0
    flip_axis(0);
    flip_axis(2);
    swap_dims(0,2);
    break;

  default:
    assert(0);
  }
}

    
#endif
