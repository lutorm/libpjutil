/// \file misc.cc
/// Miscellaneous routines. Some functions that come in handy, mostly
/// to do with path manipulation.
/// $Id$

#include "misc.h"
#include <iostream>
#include <ctype.h>
#include<string> 
#include <wordexp.h>

using namespace std;

/// Seeds the rand48 random number generator (obsolete)
void seed(long int s)
{
#ifdef LINUXPPC
  srand(s);
#else
  srand48(s);
#endif
}

/// Eats whitespace from the stream.  Stops at the first
/// non-whitespace character
istream& eatwhite(istream& is)
{
  char c;
  while(is.get(c)) {
    if(isspace(c)==0) {
      is.putback(c);
      break;
    }
  }
  return is;
}

using std::string; 

/// Checks whether string has a certain suffix.
/// Returns true if s has the specified suffix.
bool suffix_is (const string & s, const string & suffix)
{
  string::size_type i = s.find(suffix);
  return !( (i == string::npos) || (i != s.length() - suffix.length())); 
}

/// Extracts the directory name. Everything up to the last / in s is returned
string extract_directory (const string & s)
{
  string::size_type directory_end = s.find_last_of('/');
  string directory;
  if (directory_end != string::npos) // name contains a directory
    directory = s.substr(0,directory_end + 1);

  return directory;
}

/// Strips the directory. Everything including the last / is removed from
/// the string s.
string strip_directory (const string & s)
{
  string::size_type directory_end = s.find_last_of('/');
  string file;
  if (directory_end != string::npos) // name contains a directory
    file = s.substr(directory_end + 1);
  else
    file = s;

  return file;
}

/// Strips the suffix. The last . and whatever is after is removed from
/// the string s.
string strip_suffix (const string & s)
{
  string::size_type base_end = s.find_last_of('.');
  string base;
  if (base_end != string::npos) // name contains a suffix
     base = s.substr(0, base_end);
  else
    base = s;

  return base;
}

/// Does word expansion. Calls the libc function wordexp to do tilde,
/// variable, command and wildcard expansion. Returns a string vector.
std::vector<string> word_expand(const string& s)
{
  wordexp_t w;
  std::vector<string> sexp;
  if(int err=wordexp(s.c_str(),&w,0)) {
    // failed, 
    cerr << "Word expand failure on: "<< s << endl;
    sexp.push_back(s);
  }
  else { 
    // success, copy the (possibly many) separate words
    for(int i=0;i<w.we_wordc;++i)
      sexp.push_back(w.we_wordv[i]);
    wordfree(&w);
  }
  return sexp;
}
