// lsqfitter.h
//
// Declaration of numerical least squares fitter class
// Translation to c++ from Numerical Recipes C program
//
// Patrik Jonsson 980227
//
// 980726: Added the possibility of explicitly supplying the dyda function
// instead of having to rely on the (expensive) numerical dyda. Eventually,
// we might want to have the user supply the functions by inheriting the
// lsqfitter class and putting the function in there. Then there would be no
// problem exchanging pointers between getdyda and the supplied function if
// we wanted to.

/*
One lsqfitter object fits a specific function y=f(x,a0...an) to a number
of data points x0,y0 ... xm,ym by finding the combination of parameters
a0...an that minimizes the square of the residuals.

The fitter object is initialized by specifying a pointer to the fitting
function, and the number of parameters the function takes.

The call to fit gives arrays of the datapoints to be fit, the number of
data points, and an array of initial guesses for the parameters. Optionally,
one can also supply an array designating which parameters should be fixed
at their initial values and not included in the fit.
*/

#ifndef __lsqfitter__
#define __lsqfitter__

#include "matrix.h"

class lsqfitter {
private:
  matrix<double> alpha;
  matrix<double> covar;
  double  chisq;
  double  alambda;

  const matrix<double>* x;
  const matrix<double>* y;
  const matrix<char>* fixpar;
  const matrix<double>* sig;
  matrix<double>* par;
  int npar; // Number of parameters
  int nfit; // Number of parameters to fit
  int ndata; // Number of data points

  // Threshold values that *can* be user set
  // Don't change them unless you know what you're doing
  double DY_INITIALSTEP;
  double INITLAMBDA;
  double SUCCESSINCR;
  double FAILDECR;
  double ABSACC;
  double RELACC;
  int FAILRETRIES;

  // Needed accuracy in iterating for the dyda derivatives.
  // I think we don't need to be REALLY accurate here since we'll probably
  // converge pretty well anyway.
  static const double  DY_ACCURACY;

  int verbose;

  matrix<double> oneda;
  double ochisq;
  matrix<double> atry;
  matrix<double> beta;
  matrix<double> da;

  double (*func)(double,matrix<double>);  // Pointer to function to fit
  matrix<double>(*dydafunc)(double,matrix<double>); // Pointer to param deriv.

  void covsrt();
  void gaussj(matrix<double>& A, matrix<double>* b,int m);
  void mrqcof(matrix<double>*, matrix<double>*, matrix<double>*);
  int mrqmin();
  //void swap(double& a, double& b);
  // This computes the derivative dy/da of func numerically
  matrix<double> getdyda( double,const matrix<double>&);
public: 
  // Constructor using the numerical derivative
 lsqfitter( double(*f)(double, matrix<double>), double );
 // constructor supplying the derivative function - faster!
 lsqfitter( double(*f)(double, matrix<double>),
	    matrix<double>(*df)(double, matrix<double>) );

 // Do not print fitting progress
 void setSilent();
 void setNormal();
 void setVerbose();

 // Set fit routine parameters
 void setAccuracy(double,double);
 void setFailretries(int);
 void setInitialStep(double);
 void setSuccessincr(double);
 void setFaildecr(double);

  // Perform fit, returning chisquare
  double fit(const matrix<double>* xx, const matrix<double>* yy,
	     const matrix<double>* ss, matrix<double>* pp);

  // Perform fit with fixed parameters indicated by fixa
  double fit(const matrix<double>* xx, const matrix<double>* yy,
	     const matrix<double>* ss, matrix<double>* pp,
	     const matrix<char>* ff);

};

inline void lsqfitter::setSilent()
{
  verbose=0;
}

inline void lsqfitter::setNormal()
{
  verbose=1;
}

inline void lsqfitter::setVerbose()
{
  verbose=2;
}

inline void lsqfitter::setAccuracy(double abs,double rel)
{
  ABSACC=abs;
  RELACC=rel;
}

inline void lsqfitter::setFailretries(int n)
{
  FAILRETRIES=n;
}

inline void lsqfitter::setSuccessincr(double si)
{
  SUCCESSINCR=si;
}

inline void lsqfitter::setFaildecr(double fd)
{
  FAILDECR=fd;
}

inline void lsqfitter::setInitialStep(double is)
{
  INITLAMBDA=is;
}

/*
inline void lsqfitter::swap(double& a, double& b)
{
  double temp=a;
  a=b;
  b=temp;
}
*/


#endif

