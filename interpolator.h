// interpolator.h
//
// Declaration of interpolation routines
// classes linear_interpolator and cspline_interpolator 
//
// Patrik Jonsson 980415
//

/*
Interpolation routines assume that x values are rising. If extrapolation
is attempted the routines will return 0.
*/
// $Id$
// $Log$
// Revision 1.3  1999/07/20 23:17:56  patrik
// Fixed return type warnings in cxx.
//
// Revision 1.2  1999/04/28 05:51:20  patrik
// The x-axis now uses a table<double> instead of an array, so looking up
// x values is now done somewhat more intelligently then just looking through
// the whole array.
//

#ifndef __interpolator__
#define __interpolator__

#include <iostream>
#include "tableutils.h"

class interpolator {
protected:
  int points_n;
  table<double>* points_x;
  //double* points_x;
  double* points_y;

  void initialise();

  interpolator(int);
  interpolator(int,double*,double*);
  interpolator(char*); // Reads interpolation data from an ascii file
  ~interpolator();
 public:

  int setPoint(int,double,double); // set a data point
};


// *** class linear_interpolator ***


class linear_interpolator : public interpolator {
protected:
  
public:
  linear_interpolator(int);
  linear_interpolator(int,double*,double*);
  linear_interpolator(char*); // Reads interpolation data from an ascii file
  ~linear_interpolator();

  double interpolate(double);  // interpolate y from given x
};


// *** class cspline_interpolator ***


class cspline_interpolator : public interpolator {
protected:
  int C_setup; // Flags when we have calculated the coefficients
  double* C[4]; // cspline polynomial coefficients
  int bc_lotype;
  double bc_loval;
  int bc_hitype;
  double bc_hival;

  void setup(); // Calculates the coefficient matrix

public:
  cspline_interpolator(int);
  cspline_interpolator(int,double*,double*);
  ~cspline_interpolator();

  int setPoint(int,double,double); // set a data point
  int setBC(int,double,int,double);
  double interpolate(double);  // interpolate y from given x  
};

// *** INLINES ***

// *** class interpolator ***

// Constructor allocates room for n data points and sets them to zero
inline interpolator::interpolator(int n)
{
  //cerr << "constructor constrcting " << n << " data points\n";
  points_n = n;
  initialise();
}

// Copies data points from arrays x and y
inline interpolator::interpolator(int n, double* x, double* y)
{
  points_n = n;
  initialise();

  for(int i=0;i<points_n;i++) {
    points_x[i] = x[i];
    points_y[i] = y[i];
  }
}

// Destructor deletes arrays
inline interpolator::~interpolator()
{
  delete points_x;
  delete[] points_y;
}

// *** class linear_interpolator ***

// Constructors don't have to do any more
inline linear_interpolator::linear_interpolator(int n) : interpolator(n)
{
  //cerr << "li init " << n <<'\n';
}

inline linear_interpolator::linear_interpolator(int n, double* x, double* y) :
  interpolator(n,x,y)
{}

inline linear_interpolator::~linear_interpolator()
{}

inline linear_interpolator::linear_interpolator(char* c) : interpolator(c)
{}

// *** class cspline_interpolator ***
inline cspline_interpolator::cspline_interpolator(int n) : interpolator(n)
{
  // allocate coefficient arrays
  for(int i=0;i<4;i++)
    C[i] = new double[n];

  C_setup=0;
}



#endif

