// simple numerical calculation of the US standard atmosphere model,
// taken from atmosculator.com and converted to SI

#ifndef __atmosphere__
#define __atmosphere__

#include "constants.h"

class atmosphere {
  
  // base values 
  double T0_;
  double P0_;

  // mean molecular mass of air in kg/mol
  const static double M_ = 0.0289644; 

  // temp and pressure fitting parameters
  static const int n_layers_=7;
  static const double h_layer_[];
  static const double temp_par1_[];
  static const double temp_par2_[];
  static const double pressure_par1_[];
  static const double pressure_par2_[];
  static const double pressure_par3_[];
  static const bool pressure_type_[];

public:
  atmosphere() : T0_(15+273.15), P0_(101.325e3) {};

  double temperature(double h) const;
  double pressure(double h) const;
  double density(double h) const { 
    const double rho=pressure(h)*M_/(constants::R*temperature(h));
    // avoid NaN
    return (rho==rho)?rho:0; };

};

#endif
