// rungekutta.cc
// Integrates 1st order ODE's using a 5th order Runge-Kutta method with 
// adaptive step size, from Numerical Recipes.
//
// $Id$

#include "rungekutta.h"
#include "matrix.h"
#include<iostream> 

using namespace std;

matrix<double> RKintegrator::a = matrix<double>(7,1,0.,0.,0.2,0.3,
						0.6,1.0,0.875);

matrix<double> RKintegrator::b = 
                matrix<double>(7,6, 0.,   0.,   0.,   0.,   0.,   0.,
			            0.,   0.,   0.,   0.,   0.,   0.,
			            0.,   0.2,  0.,   0.,   0.,   0.,
			            0.,   0.075,0.225,0.,   0.,   0.,
			            0.,   0.3, -0.9,  1.2,  0.,   0.,
			            0.,-11./54, 2.5,-70./27,35./27,0.,
	  0., 1631./55296, 175./512, 575./13824, 44275./110592, 253./4096 );

matrix<double> RKintegrator::c = matrix<double>(7,1,
				    0., 37./378, 0., 250./621, 125./594, 0.,
						512./1771 );

matrix<double> RKintegrator::dc = matrix<double>(7,1, 
			     0, 37./378-2825./27648, 0.,
				250./621-18575./48384,
				125./594-13525./55296, 0.,
				512./1771-0.25 );

// Advances the solution one step, keeping the truncation error under control
void RKintegrator::RKadvance()
{
  // Constants
  const double safety=0.9;
  const double powgrow = -0.2;
  const double powshrink = -0.25;
  const double errcon = pow(5/safety,1/powgrow);

  // Initial guess step size from last time
  double h=hnext;
  double errmax;
  matrix<double> ytemp;

  // Set the initial derivative
  curdydx = dydxfunction(curx,cury);

  while(1) {
    // Take a step
    ytemp=RKstep(curx,cury,curdydx,h);

    // Find maximal truncation error in that step
    errmax=0;
    for(int i=0;i<ndim;i++) {
      if(fabs(yerr[i]/yscale[i])>errmax)
	errmax=fabs(yerr[i]/yscale[i]);
    }
    errmax /= eps;

    if(errmax<=1.0)
      break;  // Step succeeded

    // Step failed - reduce stepsize
    double htemp= safety*h*pow(errmax,powshrink);
    h= (h>=0?(htemp>0.1*h?htemp:0.1*h):(htemp<0.1*h?htemp:0.1*h));

    // If a maximum step is set, check that we don't exceed it
    if(hmax && (fabs(h)>hmax) )
      h=h/fabs(h)*hmax;

    // Check for underflow
    if(curx+h==curx) {
      cerr << "RKintegrator:: Hey, I'm in bad shape here - step size underflow!\n";
      break;
    }
  }

  // Increase stepsize
  if(errmax>errcon)
    hnext = safety*h*pow(errmax,powgrow);
  else
    hnext = 5*h;

  // If a maximum step is set, check that we don't exceed it
  if(hmax && (fabs(hnext)>hmax) )
    hnext=hnext/fabs(hnext)*hmax;

  cury = ytemp;
  hdid=h;
  curx+=h;
}

  
// Takes a RK step of length h, trunc error est in yerr
matrix<double> RKintegrator::RKstep(double x, const matrix<double>& y,
				    const matrix<double>& dydx, double h)
{

  // First step
  matrix<double> ytemp = y + b[2][1]*h*dydx;

  // Second step
  matrix<double> ak2 = dydxfunction(x + a[2]*h, ytemp);
  ytemp              = y + h*( b[3][1]*dydx + b[3][2]*ak2 );

  // Third step
  matrix<double> ak3 = dydxfunction(x + a[3]*h, ytemp);
  ytemp              = y + h*( b[4][1]*dydx + b[4][2]*ak2 + b[4][3]*ak3 );

  // Fourth step
  matrix<double> ak4 = dydxfunction(x + a[4]*h, ytemp);
  ytemp              = y + h*( b[5][1]*dydx + b[5][2]*ak2 + b[5][3]*ak3 +
			      b[5][4]*ak4);

  // Fifth step
  matrix<double> ak5 = dydxfunction(x + a[5]*h, ytemp);
  ytemp              = y + h*( b[6][1]*dydx + b[6][2]*ak2 + b[6][3]*ak3 +
			      b[6][4]*ak4 + b[6][5]*ak5 );
  // Sixth step
  matrix<double> ak6 = dydxfunction(x + a[6]*h, ytemp);

  // Accumulate increments to get final result
  matrix<double> yout = y + h*( c[1]*dydx + c[3]*ak3 + c[4]*ak4 + c[6]*ak6 );

  // Estimate error as difference between 4th and 5th order methods
  yerr = h*( dc[1]*dydx + dc[3]*ak3 + dc[4]*ak4 + dc[5]*ak5 + dc[6]*ak6 );

  return yout;
}

// Advances the solution to x=xend and returns the result. Note that it doesn't
// return on exactly xend, but rather after the step that takes it across
matrix<double> RKintegrator::solve(double xend)
{
  // Check sign of h
  if((xend-curx)*hnext<0) {
    // wrong sign - switch and hope for the best...
    cerr << "RKintegraotr::solve - step guess has wrong sign, moron!\n";
    //hnext*=-1;
    return matrix<double>(2,1,0.);
  }

  while( (xend-curx)*hnext>0 )
    // Still not there, step again
    RKadvance();

  // Passed - stop
  return cury;
}
