This library contains assorted useful stuff written or assimilated by Patrik. A lot are from the pre-21st century and not used any more except for compatibility in old codes.

* Patrik's stuff:

atmosphere.h: A class to get temperature, pressure and density from the
U.S. standard atmosphere model

biniostream.h: A stream that does raw binary I/O with specified byte
order. Implements the native types.

constants.h: Useful physical and mathematical constants, in namespace constants.

counter.h: A counter class that outputs updates to stdout with specified interval and can be turned off.

exceptions.h: A file_not_found exception class.

fits-utilities.h: Function to open a CCfits HDU regardless of whether
it's previously opened.

histogram.h: Pre-21st century class for accumulating data into a histogram.

hpm.h: Functionality for using performance counters. Originally just
wrapped the IBM HPM toolkit, but now implements PAPI instead.
 
integratort.h: Numerical integrator where the integrand is a
functor.

interpolatort.h: N-dimensional linear interpolator using Blitz and
template unrolling.

lists.h: Pre-21st century implementation of a list. Obsolete.

message.h: A class that when inherited from wraps cout/cerr so they can be turned off.

misc.h: Functions to manipulate file names and do globbing of strings.

preferences.h: A class that reads configuration files and then makes
the keyword/value pairs queriable. Unlike e.g. boost::program_options,
there is no need to declare which keywords will be present.

solvert.h: A Newton-Raphson solver taking the function as a functor.

units.h: A units-conversion function that interfaces to the GNU Units program.

vector_out.h: ostream operators for std::vector.


* Stuff imported/converted from others:

rungekutta_template.h: Implementation of a Runge-Kutta ODE integrator
from Numerical Recipes, where the function to integrate is implemented
in a derived class.

fitter.h: A class that does Lewenberg-Marquart chi2 minimization,
ported to C++ from Numerical Recipes.

copy_if.h: A pre C++11 implementation of the copy_if algorithm, from
Scott Meyers' "Effective STL".

Factory.h, Singleton.h, etc: Factory and Singleton classes from the
Loki library by Alexei Alexandrescu.

gnu_units.h, etc: The GNU Units package, exposed as a units conversion
API by my units.h.


* Obsolete stuff:

complex.h: A pre-standard implementation of complex numbers. Obsolete.

integrator.h: Numerical integrator where the integrand is a function pointer. Obsolete.

integrator2.h: Numerical integrator where the integrand is a derived class. Obsolete.

interpolator.h: Pre-21st century class for 1-d linear interpolation. Obsolete.

interpolator_template.h: 1-d linear interpolator that uses a map. Obsolete.

ndim_interpolator.h: N-dimensional interpolator. Obsolete.

lsqfitter.h, lsqfitter2.h, lsqfitter3.h: 

mat3.h: 3x3 matrix. Use Blitz instead.

matrix.h: Dynamically sized matrix class. Use blitz instead.

rungekutta.h: C++ implementation of ODE solver from Numerical Recipes.

solver.h: Newton-Raphson solver.

solver2.h: A Newton-Raphson solver where the function is implemented
in a derived class.

tableutils.h: A pre-STL vector class. Use std::vector or Blitz instead.

vec3.h: 3-vector. Use Blitz instead.

