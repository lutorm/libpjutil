// solver.h
//
// Declaration of numerical equation solver routines
// Uses simple Newton-Raphson method
//
// Patrik Jonsson
//

/*
One solver object solves for a specific function f=0. The function may take
any number of parameters that are supplies as double*. The call to solve
must match the number of parameters and supply a first guess.
The constructor must be supplied with the function as well as its derivative.
*/

#ifndef __solver__
#define __solver__

class solver {
private:
  double (*func)(double,double*);      // Pointer to function to solve
  double (*der)(double,double*);       // Pointer to function derivative
  int npar;                            // Number of function parameters
  double* parameters;                  // Function parameters
  double accuracy;                     // Required accuracy;

public:
  solver( double(*f)(double, double*), double(*fp)(double, double*),
	  int n, double a);

  double Solve(double x0 ...);         // Solve equation supplying parameters

};

#endif

