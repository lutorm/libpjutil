// solvert.h
//
// A template-based numerical equation solver using Newton-Raphson method.
//
// Patrik Jonsson
//
// $Id: solver2.h 237 2007-10-22 18:22:53Z patrik $

/*
A solver object solves for a specific function f=0. The function is
defined through a functor class which is a template parameter. This
functor must define the typedef T_numtype and the methods 
T_numtype func(T_numtype x) and 
T_numtype der(T_numtype x).
*/

#ifndef __solvert__
#define __solvert__

template<typename T_functor>
class solver {
public:
  typedef typename T_functor::T_numtype T_float;

  class returntype {
  public:
    T_float value;
    T_float f;
    int n;
    int status;

    returntype(T_float xx, T_float ff, int nn, int ss) :
      value(xx), f(ff), n(nn), status(ss) {};
  };

private:
  bool verbose_;
  T_float accuracy_,xaccuracy_;                     // Required accuracy;

  // The function object. This reference must not be stale throughout
  // the use of the solver.
  const T_functor& fun_;

public:
  solver(const T_functor& f) :
    accuracy_(1), xaccuracy_(0), verbose_ (false), fun_(f) {};

  solver(const T_functor& f, T_float a, T_float xa=0) :
    accuracy_ (a), xaccuracy_ (xa), verbose_ (false), fun_(f) {}

  // Solve equation from first guess x0
  returntype solve(T_float x0 );         

  void setaccuracy(T_float a, T_float xa=0) {
    accuracy_ = a;
    xaccuracy_ = xa;
  };

  void setverbose(bool v) {verbose_ = v;};
};

// Call to solve returns solution of f(x)=0. For an equation with multiple
// roots, which root is returned depends on the inital guess
template<typename T_functor>
typename solver<T_functor>::returntype solver<T_functor>::solve(T_float x0)
{
  const int maxsteps = 1000;

  T_float f;
  T_float x = x0;
  int Nstep = 0;

  while( (fabs(f=fun_.func(x))>accuracy_) && (Nstep<maxsteps) ) {
    Nstep++;

    const T_float fp = fun_.der(x);
    if(fp==0) {
      if(verbose_)
	std::cerr << "solver::Solve: Warning, extremum encountered\n";

      // return NaN to indicate that solution is crap
      // status 1 is extremum
      return returntype(blitz::quiet_NaN(T_float()), f, Nstep, 1);
    }
     else
      x-= f/fp;

    if(fabs(x-x0) < xaccuracy_)
      break;
    x0=x;
  }

  if(Nstep==maxsteps) {
    if(verbose_)
      std::cerr << "solver::Solve: Warning, failed to converge\n";

    // status 2 is failed to converge
    return returntype(blitz::quiet_NaN(T_float()), f, Nstep, 2);
  }

  //cout << Nstep << " steps\n";
  return returntype(x, f, Nstep, 0);
}


#endif

