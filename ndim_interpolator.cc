// ndim_interpolator.cc
//
// Implementations of the n-dimensional interpolator
//
// $Id$

#include "ndim_interpolator.h"
#include <iostream>
#include <fstream>
#include <cstdarg>

using namespace std;

/*
  int err=0;
  int j=ji[0];

  // Only need overrun checks when debugging, we know it works now...
  
  if((j<0)||(j>npoints[0]-1)) {
    cerr << "ndim_interpolator::index - array index[0] out of range: " << j
	 << '\n';
    err=1;
  }
  

  //int m=1;

  for(int i=1;i<ndim;i++) {
    
    if((ji[i]<0)||(ji[i]>npoints[i]-1)) {
      cerr << "ndim_interpolator::index - array index[" << i 
	   << "] out of range: " << ji[i] << '\n';
      err=1;
    }
    
    //m*= npoints[i-1];
    j+= multi[i] * ji[i];
  }
  //if(err)
  //  j=0;

  return j;
}
*/

// Constructor takes following arguments
// n = number of dimensions
// n # of integers - number of grid points in respective dimension
// n # of double* - grid values in respective dimension
// double* - n1*n2*n3*...*nn values of the data points at respective points
//           where the first dimension is least significant

ndim_interpolator::ndim_interpolator(int n...)
{
  va_list args;
  va_start(args,n);

  ndim=n;
  npoints = new int[ndim];
  multi= new int[ndim];
  points_indep = new table<double>[ndim];

  // Read number of points in each dimension
  int ntot=1;
  multi[0]=1;
  for(int i=0;i<ndim;i++) {
    npoints[i]= va_arg(args,int);
    ntot*=npoints[i];
    if(i)
      multi[i]=multi[i-1]*npoints[i-1];
    points_indep[i] = table<double>(npoints[i]);
  }

  // Read the grid values in each dimension
  for(int i=0;i<ndim;i++) {
    double* p = va_arg(args,double*);

    for(int j=0;j<npoints[i];j++)
      points_indep[i][j]=p[j];
  }

  // Allocate and read dependent values
  points_dep = new double[ntot];
  double* v = va_arg(args,double*);
  for(int i=0;i<ntot;i++)
    points_dep[i] = v[i];
}


// constructor takes data from file
ndim_interpolator::ndim_interpolator(char* filename)
{
 ifstream file(filename);

  if(!file.good()) {
    cerr << "Error opening interpolation data file: " << filename << '\n';
    ndim=0;
    points_dep=0;
    points_indep=0;
  }
  else {
    // File good - let's hope data are ok now
    file >> ndim;

    npoints = new int[ndim];
    multi= new int[ndim];
    points_indep = new table<double>[ndim];


    // Read number of points in each dimension
    int ntot=1;
    multi[0]=1;
    for(int i=0;i<ndim;i++) {
      file >> npoints[i];
      ntot*=npoints[i];
      if(i)
	multi[i]=multi[i-1]*npoints[i-1];
      points_indep[i] = table<double>(npoints[i]);
    }

    // Read the grid values in each dimension
    for(int i=0;i<ndim;i++)
      for(int j=0;j<npoints[i];j++) {
	file >> points_indep[i][j];
      }

    // Allocate and read dependent values
    points_dep = new double[ntot];
    for(int i=0;i<ntot;i++)
      file >> points_dep[i];
  }
}


// Set's the data point j1,j2,...,jn to v
void ndim_interpolator::setPoint(int j1...)
{
  va_list args;
  va_start(args,j1);

  //int j[ndim];
  table<int> j(ndim);//=new int[ndim];

  j[0]=j1;
  for(int i=1;i<ndim;i++)
    j[i] = va_arg(args,int);

  int k = index(j); // Get the index value of the data point

  points_dep[k] = va_arg(args,double);

  //delete[] j;
}


// This function just runs the va_arg stuff and builds a table and then
// calls the real interpolation routine
double ndim_interpolator::interpolate(double x1...)
{
  // Extract independent coordinates into a table
  table<double> x(ndim);

  //double* x=new double[ndim];
  //double x[ndim];
  va_list args;
  va_start(args,x1);

  x[0]=x1;
  for(int i=1;i<ndim;i++)
    x[i]=va_arg(args,double);

  return interpolate(x);
}

// do a linear interpolation
double ndim_interpolator::interpolate(table<double>& x)
{
  table<int> j(ndim);
 
  // find lower index of the 2 grid points involved
  for(int k=0;k<ndim;k++) {
    j[k] = points_indep[k].locate(x[k]);
    //cout << j [k] << '\t';
#ifndef NDIM_INP_NODEBUG
    if((j[k]<0)||(j[k]>=points_indep[k].n())) {
      cerr << "ndim_interpolator::can't extrapolate!\n";
      return 0;
    }
#endif
  }
  //cout << endl;
  // Our point is now between j[i] and j[i]+1 in the ith dimension

  // Calculate fractional distance of x[i] from j[i]
  table<double> f(ndim);

  for(int i=0;i<ndim;i++) {
    const int ji=j[i];
    const double piiji = points_indep[i][ji];
    f[i]=(x[i]-piiji)/
      (points_indep[i][ji+1]-piiji);
    //cout << f [i] << '\t';
  }
  //cout << endl;
  // Set up the value array
  const int npairs = 1<<ndim;
  table<double> values(npairs);
  table<int> m(ndim);

  //cerr << "setting up values array\n";
  for(int i=0;i<npairs;i++) {
    //cerr << "i is " << i << '\n';
    for(int k=0;k<ndim;k++) {
      m[k]=j[k]+( i & (1<<k))/(1<<k);
      //  cerr << "m[" << k << "]=" << m[k]<< '\t';
    }
    //cerr <<  "Index is " << index(m) << '\n';
    values[i]=points_dep[index(m)];
    // cerr << "values[" << i << "]=" << values[i] << '\n';
  }
  //  for (int i = 0; i <npairs; ++i)
  //cout << values [i] << '\t';
  //cout << endl;
  const double returnval=reduce(ndim,values,f);

  return returnval;
}

// Interpolates one dimension of the array of points, which are of dimension n
// f contains fractions, v the value hypercube
double ndim_interpolator::reduce(int n, table<double>& v, table<double>& f)
{
  // End of recursion
  if(n==1)
    return (v[1]-v[0])*f[ndim-1]+v[0];

  // we can reduce the arrays in place
  const int a=1<<(n-1);
  const int b=ndim-n;
  //cout << "collapsing " << b << endl;
  for(int i=0;i<a;i++) {// Collapse first dimension
    //cout <<  '\t' << v[(i<<1)+1] << '\t' << v[i<<1] << '\n';
    const int j=(i<<1);
    v[i] = (v[j+1]-v[j])*f[b] + v[j];
    //cerr << vp[i] << '\n';
  }

  // recurse
  double r=reduce(n-1,v,f);

  return r;
}

void ndim_interpolator::writedata(char* filename)
{
  cout << "Writing\n";
  ofstream of(filename);

  table<int> bin(ndim);// = new int[ndim]; // Counter of our current position

  //int loop=1;
  //for(int i=0;i<ndim;i++)
  //  bin[i]=0;
    bin=0;

  // Loop to exhaust all the dimensions
  while(bin[ndim-1]<npoints[ndim-1]) {

    // Write coordinates
    for(int i=0;i<ndim;i++)
      of << points_indep[i][bin[i]] << '\t';

    // Write value
    of << points_dep[index(bin)] << '\n';

    // Increase bin number
    bin[0]++;
    for(int i=0;i<ndim-1;i++)
      if(bin[i]>=npoints[i]) {
	bin[i]=0;
	bin[i+1]++;
	of << '\n';
      }
  }

  //delete[] bin;
}
